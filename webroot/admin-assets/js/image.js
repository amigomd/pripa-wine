$(function () {
    //define global variable
    var cropper;
    var imageCropper;
    var activeClassName;
    var gallery;
    var btnEl;
    var OriginFilename;
    var startCrop;
    var startCropI;

    //initialize functions
    InitItemEvents();
    InitUploadImage();
    InitCropForm();
    InitGallerySortable();
});
function InitGallerySortable() {
    $('#dropdown1').sortable();
}
function InitUploadImage() {
    //click to Chose file event
    $('.btnAddItem').click(function (e) {
        e.preventDefault();
        btnEl = this;
        if($(this).hasClass('gallery')){
            gallery = true;
        }else{
            gallery = false;
        }
        $(this).parents().eq(3).find(".image-file").click();
    });

    // check and upload images
    $('.image-file').unbind().on('change', function () {
        //define local variable
        item = this;
        activeClassName = !gallery ? $(this).prev().attr('name') : 'gallery';
        formEl = !gallery ? $("form.form-page") : $("form.gallery-form");
        // Validate Picture
        if (!window.FileReader || !this.files) {
            // Old Browsers
            console.log('Old Browser');
        }
        else if (!this.files[0]) {
            // No Pictures
            console.log('No picture');
        }
        else
        {
            startCrop = false;
            startCropI = -1;
            //Send image data in uploadImage
            formEl.ajaxSubmit({
                url: '/admin/'+Controller_Name+'/uploadImage?name='+activeClassName,
                dataType: "json",
                success: function (response) {
                    $.each(response, function( i, v ) {
                        if (v.success) {
                            $('.image-file').val('');
                            OriginFilename = v.filename;
                            if (!gallery) {
                                $(item).prev().val(v.filename);
                            }
                            if (v.crop === true) {
                                var CropTimer = setInterval(function () {
                                    if(!startCrop){
                                        startCropI = startCropI + 1;
                                        if(response[startCropI]) {
                                            OriginFilename = response[startCropI].filename;
                                            startCrop = true;
                                            InitCropper(response[startCropI]);
                                        }else{
                                            clearInterval(CropTimer);
                                        }
                                    }
                                },1000);
                            } else {
                                if (gallery) {
                                    template = _.template($('#galleryTmp').html());
                                    $(btnEl).parent().append(template({
                                        image: '/' + v.path + v.filename,
                                        filename: v.filename,
                                        random: v.random
                                    }));
                                    $('#gallery-random').val(v.uniqid);
                                    InitGallerySortable();
                                } else {
                                    template = _.template($('#imageTmp').html());
                                    $(item).parent().find('.gallery-item').html(template({
                                        image: '/' + v.path + v.filename,
                                        filename: v.filename
                                    }));
                                    $('#'+activeClassName).next().next().val(v.random);
                                }
                            }
                            InitItemEvents();
                        } else {
                            alert(v.error);
                        }
                    });
                }
            });
        }
    });
}
// cropper
function InitCropper(data) {
    if(data.crop === true) {
        //Event hide modal
        $('#cropperModal').on('hidden.bs.modal', function (e) {
            cropper.destroy();
            $('.crop-current-data').val('');
            $("#cropForm input[name*=random]").remove();
            startCrop = false;
        });

        //Clear
        $("#cropForm input[name*=random]").remove();
        $('#cropImage').attr('src',data.originalImage);
        $('.btn-modal-image-save').removeAttr('disabled');
        $('#cropperModal').modal('show');
        imageCropper = document.getElementById('cropImage');

        // Event start crop
        imageCropper.addEventListener('ready', function () {
            if(this.cropper === cropper){
                $('.crop-current-data').val('');
                $("input[name='crop[filename]']").val(data.filename);
                if(gallery){
                    $("#cropForm input[name*=random]").remove();
                    $('#cropForm input').eq(0).before($('#gallery-random').clone());
                    $('#cropForm input').eq(0).val(data.random);
                    $('#cropForm #gallery-random').attr('id','');
                }else{
                    if($('#'+activeClassName).next().next().attr('type')) {
                        $('#cropForm input').eq(0).before($('#' + activeClassName).next().next().clone());
                    }
                }
            }
        });

        //Init crop
        cropper = new Cropper(imageCropper, {
            aspectRatio: data.aspectRatio,
            minContainerWidth : 568,
            minContainerHeight: 400,
            viewMode: 3,
            crop: function(e) {
                $("input[name='crop[x]']").val(e.detail.x);
                $("input[name='crop[y]']").val(e.detail.y);
                $("input[name='crop[width]']").val(e.detail.width);
                $("input[name='crop[height]']").val(e.detail.height);
                $("input[name='crop[rotate]']").val(e.detail.rotate);
                $("input[name='crop[scaleX]']").val(e.detail.scaleX);
                $("input[name='crop[scaleY]']").val(e.detail.scaleY);
            }
        });

    }
}

function InitCropForm() {
    $('#cropForm').submit(function (e) {
        e.preventDefault();
        cropper.getCroppedCanvas();

        cropper.getCroppedCanvas({
            width: 160,
            height: 90
        });

        cropper.getCroppedCanvas().toBlob(function (blob) {
            var formData = new FormData(document.getElementById('cropForm'));

            formData.append('croppedImage', blob);

            // Use `jQuery.ajax` method
            $.ajax('/admin/'+Controller_Name+'/cropBlobImage?name='+activeClassName, {
                method: "POST",
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (response) {
                    response = response[0];
                    $('.btn-modal-image-save').attr('disabled');
                    if(gallery){
                        template = _.template($('#galleryTmp').html());
                        $(btnEl).parent().append(template({
                            image:'/'+response.path+response.filename,
                            filename:OriginFilename,
                            random:response.random
                        }));
                        $('#gallery-random').val(response.uniqid);
                        InitGallerySortable();
                    }else{
                        $('#'+activeClassName).parent().find('.gallery-item').val(response.filename);
                        template = _.template($('#imageTmp').html());
                        $(item).parent().find('.gallery-item').html(template({
                            image:'/'+response.path+response.filename,
                            filename:response.filename
                        }));
                    }
                    $('#cropperModal').modal('hide');
                    InitItemEvents();
                },
                error: function () {
                    console.log('Upload error');
                }
            });
        });
    });
}
function InitItemEvents() {
    // Delete the Picture
    $(".gallery-item .btnDeleteItem").unbind().click(function (e) {
        e.preventDefault();
        if(Number.isInteger(parseInt($('#cropForm input[name=id]').val()))){
            deleteGalleryImage(this,'isset',$('#cropForm input[name=id]').val(),$(this).parents().eq(3).find("input[type=hidden]").attr('name'));
        }else{
            deleteGalleryImage(this,'new',$(this).parent().parent().parent().find("input[name*=random]").val(),$(this).parents().eq(3).find("input[type=hidden]").attr('name'));
        }
//				$(this).parents().eq(3).find('.image-file').prev().val('');
        InitUploadImage();
    });
}

function deleteGalleryImage(item,type,id,name='gallery') {
    if (!confirm('Are you sure?')) {
        return false;
    }
    if(name!='gallery'){
        $(item).parent().html('<button class="btnAddItem"></button>');
    }
    $.ajax('/admin/'+Controller_Name+'/deleteImage', {
        method: "POST",
        data: {
            type:type,
            id:id,
            name:name
        },
        dataType: "json",
        success: function (response) {
            $(item).parent().parent().remove();
        },
        error: function () {
        }
    });
}