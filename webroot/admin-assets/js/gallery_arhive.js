Select2SearchAjax();

$('.btn-add').click(function (e) {
    e.preventDefault();
    btnEl = this;
    $('.file-upload').click();
});

$('.file-upload').unbind().on('change', function () {
    $('.file-form').ajaxSubmit({
        url: '/admin/galleries/uploadImages',
        dataType: "json",
        success: function (response) {
            $.each(response, function( i, v ) {
                template = _.template($('#galleryImage').html());
                $('.search-results').prepend().prepend(template(v));
            });
        }
    });
});

$('#GalleryModal').on('show.bs.modal', function (e) {
    $('#GalleryModal img').attr('src',$(e.relatedTarget).attr('data-href'));
});

$('.right-form #btn-save-tags').click(function () {
    if(confirm('You are sure?')){
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/galleries/saveTags',
            data: $('.right-form').serialize()
        }).error(function (e) {
        }).done(function (data) {
        });
    }
    return false;
});

$('.right-form #btn-delete').click(function () {
    if(confirm('You are sure?')){
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/galleries/deleteImages',
            data: $('.right-form').serialize()
        }).error(function (e) {
        }).done(function (data) {
            $('.right-gallery-elements .right-element').remove();
            $(".search-element[style*='none']").remove()
        });
    }
    return false;
});

$(function () {
    $('.select2_tags-custom-multiple').select2({
        tags: true,
        width: '100%',
        ajax: {
            url: '/admin/galleries/loadSimilarTags',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term, // search term
                    page: params.page,
                };
            },
            processResults: function (data, params) {
                params.page = 1;

                return {
                    results: data.items
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });
});

function GalleryChecker(item,id) {
    if($(item).prop('checked')){
        template = _.template($('#galleryRightImage').html());
        $('.right-gallery-elements').prepend().prepend(template({
            image: $(item).parents('.search-element').find('img').attr('src'),
            id: id,
            comment: $(item).attr('data-comment')
        }));
        $(item).parents('.search-element').hide();
       TagsUpdate();
       UpdateComment();
    }
}

function TagsUpdate() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/admin/galleries/groupTags',
        data: $('.right-form').serialize()
    }).error(function (e) {
    }).done(function (data) {
        html = '';
        $.each(data, function( i, v ) {
            html += '<option value="'+v+'" selected>'+v+'</option>';
        });
        $('.select2_tags-custom-multiple').html(html);
    });
}

function GalleryUnChecker(item,id) {
    $('.element-'+id).find('input[type=checkbox]').prop('checked',false);
    $('.element-'+id).show();
    $(item).parents('.right-element').remove();
    TagsUpdate();
}

function LoadPage(item,event) {
    if(event == 'submit'){
        $('input[name=page]').val(1);
        $('.search-results .search-element').remove();
    }else{
        $('input[name=page]').val(parseInt($('input[name=page]').val())+1);
    }
    $.ajax({
        type: "POST",
        dataType: "json",
        url: '/admin/galleries?page='+$('input[name=page]').val(),
        data: $('form').serialize()
    }).error(function (e) {
        $('.gallery-load-page').parent().hide();
    }).done(function (data) {
        $('.gallery-load-page').parent().show();
        $.each(data, function( i, v ) {
            template = _.template($('#galleryImage').html());
            if(event == 'submit'){
                $('.search-results').prepend().prepend(template(v));
            }else{
                $(item).parent().before(template(v));
            }
        });
    });
}
function UpdateComment() {
    $('.right-gallery-elements .right-element').eq(0).find('input[name=comment]').keyup(function () {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: '/admin/galleries/saveComment',
            data: {
                id:$(this).parents('.right-element').find('input[type=hidden]').eq(0).val(),
                comment:$(this).val()
            }
        }).error(function (e) {
        }).done(function (data) {
        });
    });
}