$(function(){

	//------------------------------------------------------------------------------------------------------------------
	// --- Show/Hide passwordd

	$('.password-block .show-pass').click(function(e){
		if (!$(this).hasClass('shown')) {
			$(this).addClass('shown');
			$(this).next('.form-control').attr('type', 'text')
		} else {
			$(this).removeClass('shown');
			$(this).next('.form-control').attr('type', 'password')
		}
		$(this).next('.form-control').focus();
		e.preventDefault();
	});

	// --- End Show/Hide password
	//------------------------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------------------------
	// --- Login form

	$("form#admin-login-form").submit(function(e) {
		e.preventDefault();
		$('.login-block').removeClass('shake');
		$.ajax({
			type: "POST",
			data: $(this).serialize(),
			dataType: "json",
			success: function(response) {
				if (response.redirectUrl) {
					window.location.href = window.location.origin + response.redirectUrl;
				} else {
					$('.login-block').addClass('shake');
				}
			}
		});
	});

	// --- End Login form
	//------------------------------------------------------------------------------------------------------------------


	//------------------------------------------------------------------------------------------------------------------
	// --- Navigation

	$('.navigation-menu > li > a .nav-caret').click(function(e){
		var element = $(this).parents('li');
		if (element.hasClass('open-menu-item')) {
			element.removeClass('open-menu-item');
		} else {
			$('.navigation-menu > li').removeClass('open-menu-item');
			element.addClass('open-menu-item');
		}
		e.preventDefault();
	});

	//------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	// --- Lang switcher

	$('.lang-switcher a').click(function(e){
		if (!$(this).hasClass('current')) {
			$('.lang-group').hide();
			$('.lang-switcher a').removeClass('current');
			$(this).addClass('current');
			$('.lang-'+ $(this).data('lang')).show();
			var width = $(this).width();
			var offset = $(this).position();
			$('.lang-switcher .move-block').css('left', offset.left).width(width);
		}
		e.preventDefault();
	});

	// --- End Lang switcher


	//-----------------------------------------------------------------------------------------------------------------------
	// --- Sortable menu

	function sortable_ini() {
		$('ol.sortable').nestedSortable({
			forcePlaceholderSize: true,
			handle: 'div',
			helper: 'clone',
			items: 'li',
			opacity: .6,
			placeholder: 'placeholder',
			revert: 250,
			tabSize: 25,
			tolerance: 'pointer',
			toleranceElement: '> div',
			maxLevels: 1,
			isTree: false,
			expandOnHover: 700,
			startCollapsed: false,
      cancel:'.ui-state-disabled',
			stop: function( event, ui ) {
			},
			change: function( event, ui ) {
			},
			update: function( event, ui ) {
				console.log(ui);
					updateNested();
			},
			start: function( event, ui ) {
        console.log(event, ui );
			}
		});
	}
	function updateNested() {
		$.ajax({
			type: "POST",
			url: '/admin/'+Controller_Name+'/updateNested',
			data: $('ol.sortable').nestedSortable('serialize'),
			dataType: "json",
			success: function () {

			}
		});
	}

	// --- End Sortable menu
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	// --- Translations

	$('.translate-item').click(function(e){
		if (!$(this).hasClass('edit')) {
			var val = $(this).text();
			$(this).addClass('edit').hide();
			// $(this).parent().append('<span class="translate-item-edit"><input value="'+ val +'" class="form-control" type="text"></span>');
			$(this).parent().append('<span class="translate-item-edit"><textarea class="form-control">'+ val +'</textarea></span>');

			autosize(document.querySelectorAll('.translate-item-edit .form-control'));
			$('.translate-item-edit .form-control').focus();
		}
		e.preventDefault();
	});

	$(document).on('keydown', '.translate-item-edit .form-control' ,function(evt) {
		if (evt.keyCode == 13) {
			var val = $(this).val();
			id = $(this).parent().prev().attr('data-id');
			language_id = $(this).parent().prev().attr('data-language-id');
			msgid = $(this).parent().prev().attr('data-msgid');
			domain = $(this).parent().prev().attr('data-domain');
			autosize.destroy(document.querySelectorAll('.translate-item-edit .form-control'));
			$(this).parents('td').find('.translate-item').show().removeClass('edit').html(val + '<i class="icon-pencil"></i>');
			$(this).parent().remove();
			$.ajax({
				method:'POST',
				url: "/admin/translations/edit",
				dataType:'json',
				data:{
					id:id,
					text:val,
					language_id: language_id,
					msgid: msgid,
					domain:domain
				}
			}).done(function(data) {
			});
		}
	});

	// --- End Translations
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	// --- File name edit

	$(document).on('keydown', '.file-title-edit .form-control' ,function(evt) {
		if (evt.keyCode == 13) {
			evt.preventDefault();
			var val = $(this).val();
			id = $(this).parent().prev().attr('data-id');
			// autosize.destroy(document.querySelectorAll('.translate-item-edit .form-control'));
			$(this).parents('.right-file-element').removeClass('edit');
			$(this).parents('.right-file-element').find('.file-title').html(val);
			$(this).parents('.right-file-element').find('.file-title-edit input').val(val);
			// $(this).parent().remove();
			// $.ajax({
			// 	method:'POST',
			// 	url: "file/edit",
			// 	dataType:'json',
			// 	data:{
			// 		id:id,
			// 		text:val
			// 	}
			// }).done(function(data) {
			// });
		}
	});

	// $('.right-file-element .remove-file').click(function(e){
	// 	$(this).parent().remove();
	// 	e.preventDefault();
	// });

	// --- End File name edit
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	// ---

	$( '#menu-type' ).change(function() {
		var val = $(this).find('option:selected').attr('value');
		$('.menu-type').hide();
		$('#menu-type-' + val).show();
	});

	$( '#menu-cat-index' ).change(function() {
		var val = $(this).find('option:selected').attr('value');
		var url = '//' + window.location.host+window.location.pathname+'?type='+val;
		window.history.pushState(null, null, url);
				window.location.reload();
		// $.ajax({
		// 	type: 'POST',
		// 	url: url,
		// 	data:  {'menu_cat':val, 'ajax':1},
		// 	async: false,
		// 	success:function(data){
		// 		window.history.pushState(null, null, url + '?menu_cat=' + val);
		// 		$('#content').html(data);
		// 		sortable_ini();
		// 	}
		// });
	});

	$( '#menu-cat-index' ).change(function() {
		var val = $(this).find('option:selected').attr('value');
		var url = '//' + window.location.host+window.location.pathname+'?type='+val;
		window.history.pushState(null, null, url);
		window.location.reload();
		// $.ajax({
		// 	type: 'POST',
		// 	url: url,
		// 	data:  {'menu_cat':val, 'ajax':1},
		// 	async: false,
		// 	success:function(data){
		// 		window.history.pushState(null, null, url + '?menu_cat=' + val);
		// 		$('#content').html(data);
		// 		sortable_ini();
		// 	}
		// });
	});

	$( '#tran-cat-domain' ).change(function() {
		var val = $(this).find('option:selected').attr('value');
		var url = '//' + window.location.host+window.location.pathname+'?domain='+val;
		window.history.pushState(null, null, url);
		window.location.reload();

	});

	sortable_ini();

	// --- End
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	// --- Datetimepicker
	$('.datetimepicker').datetimepicker({
		viewMode: 'days',
		locale: 'ro',
		format: 'DD MMMM YYYY',
	});

	$(".datetimepicker").on("dp.change", function (e) {
		date = new Date(e.date);
		sel_date = date.getDate();
		sel_month = date.getMonth() + 1;
		sel_year = date.getFullYear();

		$(this).parent().find('input[type=hidden]').val(sel_year+'-'+sel_month+'-'+sel_date);
	});

	// --- End Datetimepicker
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	// --- General form validation

	$('.save-continue, .save-redirect').click(function (e) {
		if($(this).attr('ajax') === '1') {
			item = this;
			e.preventDefault();

			$('.tinymce').each(function () {
				id = $(this).find('textarea').attr('id');
				if(tinymce.get(id)){
					$('textarea#' + id).html( tinymce.get(id).getContent() );
				}
			});

			$.ajax({
				type: "POST",
				dataType: "json",
				url: '/admin/' + Controller_Name + '/formValidation',
				data: $('form').serialize()
			}).done(function (data) {
				//animation start

				$('input,select').parent().removeClass('error');
				if (!data.success) {
					error_message = '<div>';
					$.each(data.errors, function (name, message) {
						$('input[name="' + name + '"]').parent().addClass('error');
						// $('select[name='+name+'\\[_ids\\]\\[\\]]').parent().addClass('error');
						$('select[name="' + name + '"]').parent().addClass('error');
						$('textarea[name="' + name + '"]').parent().addClass('error');
						if(message != '' && message != '') {
							error_message += message + '<br>';
						}
					});
					error_message +='</div>';
					if($('.message.message-error').length == 0){
						$('.wrapper').prepend('<header class="header"><div class="message message-error animated slideInDown"><span></span></div></header>');
					}
					$('.message.message-error').slideDown("slow");
					$('.message.message-error').parent().removeClass('hidden');
					$('.message.message-error span').html('Please fix error(s).'+error_message);
				} else {
					window.location.href = data.redirect_url;
					if($(item).hasClass('save-redirect')){
						window.location.href = data.redirect_url;
					}else{
						window.location.href = data.url;
						// history.pushState({'id': data.id}, "", data.url);
						// $('#id').val(data.id);
						// $('.message.message-success').slideDown("slow");
						// $('.message.message-success').parent().removeClass('hidden');
						// $('.message.message-success span').html('The pages has been updated.');
					}
				}

				//stop animation
				setTimeout(function () {
					$('.message').slideUp("slow");
				}, 5000)
			});
			$(window).on("popstate", function (e) {
				if (!e.originalEvent.state) {
					location.reload();
				}
			});
		}
	});
	// --- End General form validation


	//-----------------------------------------------------------------------------------------------------------------------
	// Select2 init
	// $.fn.select2.defaults.set("theme", "bootstrap");
	$('.select2-search').select2({ width: '100%' });
	$('.select2-tags').select2({ width: '100%' });
	$('.select2-tags-multiple').select2({
		tags: true,
		width: '100%'
	});
	$('.select2-multiselect').select2({ width: '100%' });


	// Select2 search ajax
	// 'class' => 'select2-search-ajax',
	//'data-model' => 'Members',
	//'data-name' => 'first_name',
	Select2SearchAjax();

	// Search in list admin
	ListSearch2();
});
String.prototype.replaceAll = function(search, replacement) {
  var target = this;
  return target.replace(new RegExp(search, 'g'), replacement);
};
function Select2SearchAjax() {
	$(".select2-search-ajax").select2({
		width: '100%',
		ajax: {
			url: "/admin/"+Controller_Name+"/search2ajax",
			dataType: 'json',
			delay: 250,
			data: function (params) {
				return {
					q: params.term, // search term
					page: params.page,
					model:$(this).attr('data-model'),
					name:$(this).attr('data-name'),
					find:$(this).attr('data-find')
				};
			},
			processResults: function (data, params) {
				params.page = params.page || 1;

				return {
					results: data.items,
				};
			},
			cache: true
		},
		escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
		minimumInputLength: 1,
		templateResult: formatRepo, // omitted for brevity, see the source of this page
		templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
	});
}

function formatRepoSelection (repo) {
	return repo.name || repo.text;
}

function formatRepo (repo) {
	// if (repo.loading) return repo.text;
	if(repo.name) {
		var markup = "<div class='select2-result-repository clearfix'>" +
			"<div class='select2-result-repository__meta'>" +
			"<div class='select2-result-repository__title'>" + repo.name + "</div>";
		return markup;
	}

}

function ListSearch2() {
	$('.menu-list-search').select2({ width: '100%' });
	$('.list-search').change(function () {
		u = new Url();
		if($(this).val()){
			u.query[$(this).attr('name')] = $(this).val();
		}else{
			delete u.query[$(this).attr('name')];
		}
		window.location.href = u;
	});
}

//------- Search in admin list -------

$(function(){
	$('#translateSearch').on('blur keyup', function () {
		var search = $(this).val().toLowerCase();
		$(".keywords").each(function () {
			if ($(this).attr('data-keywords').indexOf(search) == -1) {
				$(this).hide();
			} else {
				$(this).show();
			}
		});
	});

		$("#search").keyup(function(e) {
			if (e.keyCode == 13) {
				url = GetUrl('search', $(this).val());
				history.pushState({search: $(this).val()}, "", url);
				indexContent(url);
			}
		});
		$(window).on("popstate", function (e) {
			$(".content-search input[type=search]").val(!!e.originalEvent.state.search ? e.originalEvent.state.search : '');
			$('.per-page').val(!!e.originalEvent.state.per ? e.originalEvent.state.per : 10);
			indexContent(window.location.href);
		});
		$('.per-page').change(function(){
			url = GetUrl('per-page',$(this).val());
			history.pushState({per:$(this).val()},"", url);
			indexContent(url);
		});

		// $(".select2-ajax-list").change(function() {
		// 	u = new Url();
		// 	if($(this).val()){
		// 		u.query[$(this).attr('data-search')] = $(this).val();
		// 	}else{
		// 		delete u.query[$(this).attr('data-search')];
		// 	}
		// 	window.location.href = u;
        //
		// 	// url = GetUrl($(this).attr('data-search'),$(this).val());
		// 	// history.pushState({query: $(this).val()}, "", url);
		// 	// indexContent(url);
		// });
});

function indexContent(url) {
		$.ajax({
			type: "GET",
			dataType: "html",
			url: url,
		}).done(function(data) {
			content = $.parseHTML(data);
			len = content.length;
			$.each(content, function (i, el) {
				if (el.className === 'content-block') {
					// $('.admin-list-page').replaceWith($(el).find('.admin-list-page'));
					$('.content-block').replaceWith(el);
					ListSearch2();
					Select2SearchAjax();
					IninAdminListSort();
				}
			});
		});
}
function GetUrl(name,atribute) {
	var u = new Url();
	delete u.query['page'];
	if(atribute == ''){
		delete u.query[name];
	}else{
		u.query[name] = atribute;
	}
	return u;
}
//------- Sortable admin list
$(function () {
	IninAdminListSort();
});
function IninAdminListSort() {
	$('#admin-list').sortable({
		update: function () {
			ajaxSavePositions();
		}
	});
}
function ajaxSavePositions() {
	$.ajax({
		type: "POST",
		url: '/admin/'+Controller_Name+'/upldateListPosition',
		data: $('#admin-list').sortable("serialize"),
		dataType: "json"
	});
}

//-----------------------------------------------------------------------------------------------------------------------
// --- tinymce
if(Action_Name == 'add' || Action_Name == 'edit' || Controller_Name == 'Comparisons') {
	tinymce.init({
		selector: '.tinymce textarea',
		height: 400,
		menubar: false,
		theme: 'modern',
		plugins: [
				"advlist autolink lists link image charmap print preview anchor",
				"searchreplace visualblocks code fullscreen",
				"insertdatetime media table contextmenu paste responsivefilemanager tag_pictures amigo_gallery"
		],
		toolbar: "insertfile undo redo | cut copy paste pastetext | removeformat styleselect | bold italic | alignleft aligncenter alignright | bullist numlist outdent indent table charmap | link image media | code | tag_pictures",
		extended_valid_elements: "a[class|href|title|target|rel]",
		external_filemanager_path: "/admin-assets/plugin/filemanager/filemanager/",
		filemanager_title: "File Manager",
		external_plugins: {"filemanager": "/admin-assets/plugin/filemanager/filemanager/plugin.min.js"}
	});

  tinymce.init({
    selector: '.tinymce-simple textarea',
    height: 200,
    menubar: false,
    plugins: [
      "autolink link lists image charmap",
      "contextmenu"
    ],
    rel_list: [
      {title: 'follow', value: ''},
      {title: 'nofollow', value: 'nofollow'}
    ],
    toolbar: "bold italic bullist link",
  });
}

// --- End tinymce
//-----------------------------------------------------------------------------------------------------------------------

