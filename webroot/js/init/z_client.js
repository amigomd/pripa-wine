var window_w = 0;
var height_w = 0;
var window_w_2 = 0;
var height_w_2 = 0;
$(function(){

	window_w = $(window).width();
	height_w = $(window).height();
	window_w_2 = $(window).width();
	height_w_2 = $(window).height();
	var locale = $('html').attr('lang');


	function checkOperatingSystem() {
    var  userAgent = navigator.userAgent || navigator.vendor || window.opera;

    //Check mobile device is Android
    if (/android/i.test(userAgent)) {
      // alert("android");
    }

    //Check mobile device is IOS
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      // alert("iPad|iPhone|iPod");
    }

    //Check device os is Windows (For Laptop and PC)
    if (navigator.appVersion.indexOf("Win")!=-1)
    {
      // alert("Win");
      $('body').addClass('win_os');
    }

    //Check device os is MAC (For Laptop and PC)
    if (navigator.appVersion.indexOf("Mac")!=-1)
    {
      // alert("Mac");
      $('body').addClass('mac_os');
    }
	}

	checkOperatingSystem();




	// var menuEl = document.getElementById('ml-menu'),
	// mlmenu = new MLMenu(menuEl, {
	// 	// breadcrumbsCtrl : true, // show breadcrumbs
	// 	initialBreadcrumb : '&nbsp;', // initial breadcrumb text
	// 	backCtrl : false, // show back button
	// 	// itemsDelayInterval : 60, // delay between each menu item sliding animation
	// 	// onItemClick: loadDummyData // callback: item that doesn´t have a submenu gets clicked - onItemClick([event], [inner HTML of the clicked item])
	// });




	//-----------------------------------------------------------------------------------------------------------------------
	// --- Wines hover/click

	$('.wines-swiper .swiper-slide').click(function (e) {
		window_w = $(window).width();
		if (window_w < 1025 ) {
			if ($(this).find('.slide-desc').hasClass('show')) {
				// $(this).find('.slide-desc').removeClass('show');
				// $(this).find('.slide-desc').stop().fadeOut(300);

				// $('.header__logo, .trigger-block, .footer, .swiper-pagination').stop().fadeIn(300);

			} else {
				$(this).find('.slide-desc').addClass('show');
				$(this).find('.slide-desc').stop().fadeIn(300);

				$('.swiper-pagination').stop().fadeOut(300);
			}
		}
		e.preventDefault();
	});

	$('.close-slide-desc').click(function (e) {
		window_w = $(window).width();
		if (window_w < 1025 ) {
			$(this).parents('.slide-desc').stop().fadeOut(300);
			$(this).parents('.slide-desc').removeClass('show');
			$('.swiper-pagination').fadeIn(300);
		}
		e.stopPropagation();
		e.preventDefault();
	});





	$('.wines-swiper .swiper-slide').hover(function (e) {
		window_w = $(window).width();
		if (window_w > 1024 ) {
			$(this).find('.slide-desc').addClass('open');
			$(this).find('.slide-desc').stop().fadeIn(300);
		}
	}, function (e) {
		window_w = $(window).width();
		if (window_w > 1024 ) {
			$(this).find('.slide-desc').removeClass('open');
			$(this).find('.slide-desc').stop().fadeOut(300);
		}
	});

	// --- End Wines hover/click
	//-----------------------------------------------------------------------------------------------------------------------







	//-----------------------------------------------------------------------------------------------------------------------
	// --- Stiky sidebar

	// if (window_w > 1024) {
	// 	$('.scroll-menu').height($('.scroll-block').height());
	// }

	// if ($('.scroll-menu').length) {


	// 	$('body').scrollspy({ target: '#navbar-example', offset: 184 });

	// 	$(window).on('activate.bs.scrollspy', function (e) {
	// 	  history.replaceState({}, "", $("a[href^='#']", e.target).attr("href"));
	// 	});

	// 	$('.about-nav a[href^="#"]').on('click',function (e) {
	// 		e.preventDefault();

	// 		var target = this.hash;
	// 		var $target = $(target);

	// 		$('html, body').stop().animate({
	// 			'scrollTop': $target.offset().top - 164
	// 		}, 500);
	// 	});


	// 	if (window_w > 1024) {
	// 		var menu_height = $('.scroll-menu').height();
	// 		var menu_offset = $('.scroll-menu').offset();
	// 		var footer = $('.discover-section').offset();


	// 		$('.scroll-menu').width($('.scroll-menu').parent().width() - 30);


	// 		var pos = footer.top - menu_height - (149 + 160);

	// 		if (menu_offset && $(this).scrollTop() >= menu_offset.top-149) {
	// 			 $('.scroll-menu').addClass('stiky');
	// 		} else {
	// 			$('.scroll-menu').removeClass('stiky');
	// 		}

	// 		if ( $(this).scrollTop() > pos ) {
	// 			$('.scroll-menu').addClass('stiky_bottom');
	// 		} else {
	// 			$('.scroll-menu').removeClass('stiky_bottom');
	// 		}
	// 	}

	// 	$(window).scroll(function () {
	// 		window_w = $(window).width();

	// 		if (window_w > 1024 ) {

	// 			footer = $('.discover-section').offset();

	// 			var pos = footer.top - menu_height - (149 + 160);

	// 			if (menu_offset && $(this).scrollTop() >= menu_offset.top-149) {
	// 				 $('.scroll-menu').addClass('stiky');
	// 			} else {
	// 				$('.scroll-menu').removeClass('stiky');
	// 			}

	// 			if ( $(this).scrollTop() > pos ) {
	// 				$('.scroll-menu').addClass('stiky_bottom');
	// 			} else {
	// 				$('.scroll-menu').removeClass('stiky_bottom');
	// 			}
	// 		}

	// 	});
	// }

	// --- End Stiky sidebar
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	//----

	// if (window.navigator.userAgent.indexOf("Mac") != -1) {
	// 	$('html').addClass('mac_os');
	// }

	//   var userAgent = navigator.userAgent || navigator.vendor || window.opera;

		// Windows Phone must come first because its UA also contains "Android"
		// if (/windows phone/i.test(userAgent)) {
		//     return "Windows Phone";
		// }

		// if (/android/i.test(userAgent)) {
		//   $('html').addClass('mac_os');
		// }

	// iOS detection from: http://stackoverflow.com/a/9039885/177710
	// if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
	//   $('html').addClass('mac_os');
	// }


	// if (window.navigator.userAgent.indexOf("X11") != -1) OSName="UNIX";
	// if (window.navigator.userAgent.indexOf("Linux") != -1) OSName="Linux";

	//---- End
	//-----------------------------------------------------------------------------------------------------------------------



	//----- Input materialize
	//-----------------------------------------------------------------------------------------------------------------------

  // $(document).on('blur', '.contact-form .form-input', function(e){
  // 	console.log('111');
  //   $(this).val() ? $(this).parent().addClass('has-value') : $(this).parent().removeClass('has-value');
  // });

  // $('.contact-form label').click(function(){
  //   $(this).parent('.form-col').find('.form-input').addClass('has-value').focus();
  // });

  // $('.contact-form .form-input').each(function () {
  //   if ($(this).val()) {
  //     $(this).parent().addClass('has-value');
  //   }
  // });

  //----- End Input materialize
  //-----------------------------------------------------------------------------------------------------------------------



	//-----------------------------------------------------------------------------------------------------------------------
	//---- Scroll header

	// $(window).scroll(function() {
	// 	if ($(window).scrollTop() > 150) {
	// 		$('.header').addClass('fixed');
	// 	} else {
	// 		$('.header').removeClass('fixed');
	// 	}

	// 	if ($(window).scrollTop() > 200) {
	// 		$('.header').addClass('fixed-amimate');
	// 	} else {
	// 		$('.header').removeClass('fixed-amimate');
	// 	}

	// });

	//---- End Scroll header
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	//---- Main title animation

	// Fake loading.
	// if ($('.video-section').length) {
	// 	if ($(window).scrollTop() > 200 ) {
	// 		// document.addEventListener("DOMContentLoaded", function(event) {
	// 			// setTimeout(init, 100);
	// 			$('body').addClass('loaded-fast');
	// 		// });
	// 	} else {
			// setTimeout(init, 1000);
			// document.addEventListener("DOMContentLoaded", function(event) {
				setTimeout(function() {
					$('body').addClass('loaded');
				}, 500);
			// });
		// }
	// }

	// function findAncestor (el, cls) {
	// 	while ((el = el.parentElement) && !el.classList.contains(cls));
	// 	return el;
	// }

	// function init() {
	// 	var rev1 = new RevealFx(document.querySelector('#rev-1'), {
	// 		revealSettings : {
	// 			bgcolor: '#ffffff',
	// 			delay: 200,
	// 			duration: 900,
	// 			onStart: function(contentEl, revealerEl) {
	// 				var box = findAncestor(contentEl, 'reveal-box');
	// 				box.style.opacity = 1;
	// 					contentEl.style.opacity = 0;
	// 			},

	// 			onCover: function(contentEl, revealerEl) { contentEl.style.opacity = 1; }
	// 		}
	// 	});
	// 	rev1.reveal();

	// 	var rev2 = new RevealFx(document.querySelector('#rev-2'), {
	// 		revealSettings : {
	// 			bgcolor: '#ffffff',
	// 			duration: 700,
	// 			delay: 400,
	// 			onStart: function(contentEl, revealerEl) {
	// 				var box = findAncestor(contentEl, 'reveal-box');
	// 				box.style.opacity = 1;
	// 					contentEl.style.opacity = 0;
	// 			},

	// 			onCover: function(contentEl, revealerEl) { contentEl.style.opacity = 1; }
	// 		}
	// 	});
	// 	rev2.reveal();
	// }


	//---- End Main title animation
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	//---- Video block height

	// if( $('.video-section').length ) {
	// 	if (window_w > 767) {
	// 		$('.video-section').height($(window).height());
	// 	} else {
	// 		$('.video-section').height($(window).height() - 82);
	// 	}
	// }

	// $( window ).resize(function() {
	// 	if( $('.video-section').length ) {
	// 		window_w = $(window).width();
	// 		if (window_w != window_w_2) {
	// 			if (window_w > 767) {
	// 				$('.video-section').height($(window).height());
	// 			} else {
	// 				$('.video-section').height($(window).height() - 82);
	// 			}
	// 			window_w_2 = $(window).width();
	// 		}

	// 		height_w = $(window).height();
	// 		if (Math.abs(height_w - height_w_2) > 100) {
	// 			if (window_w > 767) {
	// 				$('.video-section').height($(window).height());
	// 			} else {
	// 				$('.video-section').height($(window).height() - 82);
	// 			}
	// 			height_w_2 = $(window).height();
	// 		}
	// 	}
	// });

	//---- End Video block height
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	//---- Lang switcher

	// $('.lang-switcher .dropdown-trigger').click(function (e) {
	// 	if ($(this).hasClass('dropdown-open')) {
	// 		$(this).removeClass('dropdown-open');
	// 		$('.lang-switcher .dropdown-menu').slideUp(250);
	// 	} else {
	// 		$('.header__main-menu .with-submenu').removeClass('open-submenu');
	// 		$('.header__main-menu .with-submenu .sub-menu').stop().slideUp(150);

	// 		$('.search-trigger').removeClass('open');
	// 		setTimeout(function() {
	// 			$('.header').removeClass('search-open');
	// 		}, 150);
	// 		$('.search-block').removeClass('open');
	// 		$('.search-block .search-form input').blur();

	// 		$(this).addClass('dropdown-open');
	// 		$('.lang-switcher .dropdown-menu').slideDown(250);
	// 	}
	// 	e.preventDefault();
	// });

	// $('.lang-switcher .dropdown-menu a').click(function (e) {
	// 	var a_href = $(this).attr('href');
	// 	var a_text = $(this).html();

	// 	$('.lang-switcher .dropdown-menu li').removeClass('current');
	// 	$(this).parent().addClass('current');
	// 	$('.lang-switcher .dropdown-menu').slideUp(250);

	// 	$('.lang-switcher .dropdown-trigger').removeClass('dropdown-open');
	// 	$('.lang-switcher .dropdown-trigger').html(a_text);

	// 	e.preventDefault();
	// });


	//---- End Lang switcher
	//-----------------------------------------------------------------------------------------------------------------------


	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- Popup

	// $('.schedule-table .tr').click(function (e) {
	// 	$('.popup-block').addClass('show');
	// 	$('html').addClass('modal-open');

	// 	if ($(window).width() > 1024 ) {
	// 		$('html').css('padding-right', 15);
	// 	}
	// 	e.preventDefault();
	// });

	// $('.schedule-table .flights-avatar').click(function (e) {
	// 	e.stopPropagation();
	// });

	// $('.popup-wrap-block').click(function (e) {
	// 	$('.popup-block').removeClass('show');
	// 	$('html').removeClass('modal-open');
	// 	if ($(window).width() > 1024 ) {
	// 		$('html').removeAttr('style');
	// 	}
	// 	e.preventDefault();
	// });

	// $('.popup-inner').click(function (e) {
	// 	e.stopPropagation();
	// });

	// // --- End popup
	// //-----------------------------------------------------------------------------------------------------------------------

	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- Tabs

	// $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	// 	// console.log(e.target);
	// 	// console.log(e.relatedTarget);
	// 	var pos = 0;

	// 	if ($('.header__flights-nav').length) {
	// 		if (window_w > 767) {
	// 			pos = $('.header__flights-nav').offset();
	// 			$('html,body').animate({scrollTop: pos.top - 120 }, 500);
	// 		} else {
	// 			pos = $('.home-section').offset();
	// 			$('html,body').animate({scrollTop: pos.top - 80 }, 500);
	// 		}
	// 	}

	// 	if ($('.page__flights-nav').length) {
	// 		pos = $('.page__flights-nav').offset();
	// 		$('html,body').animate({scrollTop: pos.top - 50 }, 500);
	// 	}

	// })

	//---- End Tabs
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	//----

	// $('.header__main-menu > .with-submenu > a').click(function(e) {

	// 	if ($(this).parent().hasClass('open-submenu')) {
	// 		if (window_w > 767) {
	// 			// console.log('222');
	// 			$(this).parent().removeClass('open-submenu');
	// 			$(this).parent().find('.sub-menu').stop().slideUp(250);
	// 		}
	// 	} else {
	// 		// console.log('333');

	// 		// $('.lang-switcher .dropdown-trigger').removeClass('dropdown-open');
	// 		// $('.lang-switcher .dropdown-menu').slideUp(150);

	// 		$('.search-trigger').removeClass('open');
	// 		setTimeout(function() {
	// 			$('.header').removeClass('search-open');
	// 		}, 150);
	// 		$('.search-block').removeClass('open');
	// 		$('.search-block .search-form input').blur();

	// 		if (window_w > 767) {
	// 			// console.log('111');

	// 			// $('.header__main-menu .with-submenu').removeClass('open-submenu');
	// 			// $('.header__main-menu .with-submenu .sub-menu').stop().slideUp(150);

	// 			$(this).parent().addClass('open-submenu');
	// 			$(this).parent().find('.sub-menu').stop().slideDown(250);

	// 		}
	// 	}
	// 	console.log('click');
	// 	e.preventDefault();
	// });

	//---- End
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	//---- Open/Close search

	// $('.search-trigger').click(function (e) {
	// 	if ($(this).hasClass('open')) {
	// 		$(this).removeClass('open');
	// 		setTimeout(function() {
	// 			$('.header').removeClass('search-open');
	// 		}, 150);
	// 		$('.search-block').removeClass('open');
	// 		$('.search-block .search-form input').blur();
	// 	} else {

	// 		if (window_w > 767) {
	// 			console.log('444');
	// 			$('.lang-switcher .dropdown-trigger').removeClass('dropdown-open');
	// 			$('.lang-switcher .dropdown-menu').slideUp(150);

	// 			$('.header__main-menu .with-submenu').removeClass('open-submenu');
	// 			$('.header__main-menu .with-submenu .sub-menu').stop().slideUp(150);

	// 		}

	// 		// $('.menu-trigger').removeClass('open');
	// 		$('.main-menu-block, .search-trigger').removeClass('open-menu');
	// 		$('.menu-trigger').removeClass('open');
	// 		$('html').removeClass('modal-open');


	// 		$(this).addClass('open');
	// 		setTimeout(function() {
	// 			$('.header').addClass('search-open');
	// 		}, 150);
	// 		$('.search-block').addClass('open');
	// 		$('.search-block').find('.search-form input').focus();
	// 	}
	// 	e.preventDefault();
	// });



	//---- End Open/Close search
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	//----

	$('.menu-trigger').click(function (e) {
		if ($(this).hasClass('open')) {
			$(this).removeClass('open');
			$('.main-menu-block').removeClass('open-menu');
			window_w = $(window).width();
			if (window_w > 1025 ) {
				$('.main-menu-overlay').fadeOut(300);
			} else {
				$('html').removeClass('modal-open');
			}
		} else {
			$(this).addClass('open');
			$('.main-menu-block').addClass('open-menu');
			window_w = $(window).width();
			if (window_w > 1025 ) {
				$('.main-menu-overlay').fadeIn(300);
			} else {
				$('html').addClass('modal-open');
			}
		}
		e.preventDefault();
	});

	//---- End
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	//----

	// var owl = $('.owl-carousel');

	// owl.on('initialized.owl.carousel ', function(event) {
	// 	var current = event.item.index;
	// 	$(event.target).find(".owl-item").eq(event.item.index).addClass('show-animation');
	// 	// console.log($(event.target).find(".owl-item").eq(event.item.index));
	// });


	// owl.owlCarousel({
	// 	loop:true,
	// 	margin:0,
	// 	// nav:true,
	// 	items:1,
	// 	animateOut: 'fadeOut',
	// 	animateIn: 'fadeIn',
	// 	autoplay: true,
	// 	autoplayHoverPause: true,
	// 	autoplayTimeout: 4000
	// });


	// owl.on('translated.owl.carousel', function(event) {
	// 	// console.log('222');
	// 	var current = event.item.index;
	// 	$(event.target).find(".owl-item").eq(event.item.index).addClass('show-animation');
	// 	// console.log($(event.target).find(".owl-item").eq(event.item.index));
	// });

	// owl.on('translate.owl.carousel', function(event) {
	// 	// console.log('111');
	// 	$(event.target).find(".owl-item").removeClass('show-animation')
	// });


	//---- End
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	// // --- Socials Shares

	// if( $('.socials-block').length ) {
	// 	var url = window.location.href;
	// 	var encoded = encodeURIComponent(url);
	// 	var requestFB = "https://graph.facebook.com/?id=" + encoded;
	// 	var requestGoogle = "https://plusone.google.com/_/+1/fastbutton?url=" + url;

	// 	// Facebook
	// 	$.ajax({
	// 		url: requestFB,
	// 		dataType: "jsonp",
	// 		success: function(data) {
	// 			if (data.share) {
	// 				$('.number-facebook').text(data.share.share_count);
	// 			}
	// 		}
	// 	});

	// 	// Google Plus
	// 	var data = {
	// 		"method": "pos.plusones.get",
	// 		"id": url,
	// 		"params": {
	// 			"nolog": true,
	// 			"id": url,
	// 			"source": "widget",
	// 			"userId": "@viewer",
	// 			"groupId": "@self"
	// 		},
	// 		"jsonrpc": "2.0",
	// 		"key": "p",
	// 		"apiVersion": "v1"
	// 	};

	// 	$.ajax({
	// 		type: "POST",
	// 		url: "https://clients6.google.com/rpc",
	// 		processData: true,
	// 		contentType: 'application/json',
	// 		data: JSON.stringify(data),
	// 		success: function(r){
	// 			if (r.result.metadata.globalCounts.count) {
	// 				$('.number-google-plus').text(r.result.metadata.globalCounts.count);
	// 			}
	// 		}
	// 	});

	// }

	// --- End Socials Shares
	//-----------------------------------------------------------------------------------------------------------------------
	//-----------------------------------------------------------------------------------------------------------------------
	// --- Order history table

	// if( $('.article-image-section').length ) {
	// 	$('.article-image-section').height($(window).height());


	// 	$(window).scroll(function() {
	// 		if ($(window).scrollTop() > 0 ) {
	// 			console.log('111');
	// 			$('.intro-effect-fadeout').addClass('modify');
	// 		} else {
	// 			console.log('22');
	// 			$('.intro-effect-fadeout').removeClass('modify');
	// 		}
	// 	});

	// }

	// $( window ).resize(function() {
	// 	if( $('.article-image-section').length ) {
	// 		// $('.article-image-section').height($(window).height());
	// 		window_w = $(window).width();
	// 		if (window_w != window_w_2) {
	// 			$('.article-image-section').height($(window).height());
	// 			window_w_2 = $(window).width();
	// 		}
	// 		height_w = $(window).height();
	// 		if (Math.abs(height_w - height_w_2) > 100) {
	// 			$('.article-image-section').height($(window).height());
	// 			height_w_2 = $(window).height();
	// 		}
	// 	}
	// });

	// if ($('.video-section').length) {

	// }



	// --- End Order history table
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	// --- Accordion

	$('.simple-section').on('click', '.toggle-block .toggle-button', function (e) {
		if ($(this).hasClass('open-toggle')) {
			$(this).removeClass('open-toggle');
			$(this).parent().find('.toggle-panel').stop().slideUp(300);
		} else {

			$(this).addClass('open-toggle');
			$(this).parent().find('.toggle-panel').stop().slideDown(300);
		}
		e.preventDefault();
	});

	// --- End Accordion
	//-----------------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------------
	// --- Quantity popup

	// $('.lightBoxVideoLink').simpleLightbox();

	// --- End Quantity popup
	//-----------------------------------------------------------------------------------------------------------------------


	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- breadrumbs width

	// var sum = 0;

	// if($('.breadcrumbs-block').length) {
	// 	$('.breadcrumbs-block a').each(function () {
	// 		var width = $(this).innerWidth();
	// 		var count = $(this).parent().find('a').size();

	// 		sum +=width;

	// 		$('.breadcrumbs-wrap').css('min-width', sum+count);
	// 	});
	// }


	// // --- End breadrumbs width
	// //-----------------------------------------------------------------------------------------------------------------------


	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- Get/Set rating

	// $('.put-rating').barrating({
	// 	theme: 'css-stars',
	// 	showSelectedRating: false,
	// 	// readonly: true,
	// 	onSelect: function(value, text, event) {
	// 		ratingStars = value;
	// 	}
	// });

	// $('.multiple-floating-star').each(function (index, item) {
	// 	var rating = $(item).data('current-rating');
	// 	$(item).barrating({
	// 		theme: 'css-stars',
	// 		showSelectedRating: false,
	// 		initialRating: rating,
	// 		readonly: true
	// 	});
	// });

	// // --- End Get/Set rating
	// //-----------------------------------------------------------------------------------------------------------------------


	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- Open/Close Review form

	// $('.btn-leave-review').click(function (e) {
	// 	if ($('.one-product-review-form').hasClass('review-open')) {
	// 		$('.one-product-review-form').removeClass('review-open');
	// 		$('.one-product-review-form').slideUp(300);

	// 	} else {
	// 		$('.one-product-review-form').addClass('review-open');
	// 		$('.one-product-review-form').slideDown(300);
	// 	}
	// 	e.preventDefault();
	// });

	// // --- End Open/Close Review form
	// //-----------------------------------------------------------------------------------------------------------------------


	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- Move To Review block

	// $('.one-product-rating .votes').click(function (e) {
	// 	$('html,body').animate({scrollTop: $(".reviews-block").offset().top - 15 }, 500);
	// 	e.preventDefault();
	// });

	// // --- End Move To Review block
	// //-----------------------------------------------------------------------------------------------------------------------



	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- Sliders Init

	// // var mySwiper_slider = new Swiper ('.home-slider', {
	// // 	direction: 'vertical',
	// // 	loop: true,
	// // 	autoHeight: true,
	// // 	pagination: {
	// // 		el: '.swiper-pagination',
	// // 		clickable: true,
	// // 	},
	// // })


	// $('.home-slider').owlCarousel({
 //    loop:true,
 //    margin:10,
 //    items:1,
 //    mouseDrag: false,
 //    animateOut: 'fadeOut',
 //    animateIn: 'fadeIn',
 //    autoplay: true,
	// })


	// var mySwiper_prod = new Swiper ('.top-offers', {
	// 	slidesPerView: 5,
	// 	spaceBetween: 25,
	// 	simulateTouch: false,
	// 	breakpoints: {
	// 		1024: {
	// 			slidesPerView: 3,
	// 			spaceBetween: 20
	// 		},
	// 		767: {
	// 			slidesPerView: 2,
	// 			spaceBetween: 20
	// 		}
	// 	},
	// 	pagination: {
	// 		el: '.swiper-pagination',
	// 		clickable: true,
	// 	},
	// })

	// var mySwiper_top = new Swiper ('.top-sellers', {
	// 	slidesPerView: 5,
	// 	slidesPerColumn: 2,

	// 	spaceBetween: 25,
	// 	simulateTouch: false,
	// 	breakpoints: {
	// 		1024: {
	// 			slidesPerView: 3,
	// 			spaceBetween: 20
	// 		},
	// 		767: {
	// 			slidesPerView: 2,
	// 			spaceBetween: 20
	// 		}
	// 	},
	// 	pagination: {
	// 		el: '.swiper-pagination',
	// 		clickable: true,
	// 	},
	// })

	// var mySwiper_article = new Swiper ('.articles-list-block', {
	// 	slidesPerView: 4,
	// 	spaceBetween: 20,
	// 	simulateTouch: false,
	// 	breakpoints: {
	// 		1024: {
	// 			slidesPerView: 3,
	// 		},
	// 		767: {
	// 			slidesPerView: 2,
	// 		},
	// 		480 : {
	// 			slidesPerView: 1,
	// 		}
	// 	},
	// 	pagination: {
	// 		el: '.swiper-pagination',
	// 		clickable: true,
	// 	},
	// })

	// // --- End Sliders Init
	// //-----------------------------------------------------------------------------------------------------------------------


	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- Lighbox init

	// $("[data-fancybox]").fancybox({
	// 	infobar: false,
	// 	transitionEffect: "slide",
	// 	animationEffect: "fade",
	// 	mobile: {
 //      idleTime: false,
 //      clickContent: function(current, event) {
 //      	console.log('111');
 //          return current.type === "image" ? "close" : true;
 //      },
 //      clickSlide: function(current, event) {
 //      	console.log('222');
 //          return current.type === "image" ? "close" : true;
 //      },
 //    },
	// });

	// // --- End Lighbox init
	// //-----------------------------------------------------------------------------------------------------------------------


	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- Desc more

	// $('.desc-more').click(function (e) {
	// 	$(this).remove();
	// 	$('.desc-block-more').slideDown(300);
	// 	e.preventDefault();
	// });

	// // --- End Desc more
	// //-----------------------------------------------------------------------------------------------------------------------


	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- Button icon click

	// $('.btn-icon').click(function (e) {

	// 	if ($(this).hasClass('select')) {
	// 		$(this).removeClass('select');
	// 	} else {
	// 		$(this).addClass('select');
	// 	}

	// 	e.preventDefault();
	// });

	// // --- End Button icon click
	// //-----------------------------------------------------------------------------------------------------------------------


	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- Main menu

	// $('.main-menu > ul > .with-submenu > a').click(function (e) {

	// 	window_w = $(window).width();
	// 	if (window_w < 768 ) {

	// 		if ($('.main-menu-block').hasClass('sub-menu-open')) {
	// 			$('.main-menu-block').removeClass('sub-menu-open');
	// 			$('.main-menu > ul > li').stop().show();

	// 			$(this).parent().removeClass('open');
	// 			e.preventDefault();

	// 		} else {
	// 			$('.main-menu-block').addClass('sub-menu-open');
	// 			$('.main-menu > ul > li').stop().hide();

	// 			$(this).parent().stop().show().addClass('open');
	// 			e.preventDefault();
	// 		}
	// 	} if (window_w >= 768 && window_w < 1025  ) {

	// 		if ($(this).parent().hasClass('hover')) {
	// 			$(this).parent().removeClass('hover');
	// 		} else {
	// 			$('.main-menu-block .main-menu > ul > li').removeClass('hover');
	// 			$(this).parent().addClass('hover');
	// 		}
	// 	}
	// });

	// // --- End Main menu
	// //-----------------------------------------------------------------------------------------------------------------------


	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- Autosize Textarea

	// // autosize(document.querySelectorAll('textarea'));

	// // --- End Autosize Textarea
	// //-----------------------------------------------------------------------------------------------------------------------


	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- Tabs

	// // $('.nav-tabs a').click(function(){
	// // 	$(this).tab('show');
	// // })

	// // --- End Tabs
	// //-----------------------------------------------------------------------------------------------------------------------





	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- custom select

	// $('.custom-select select').select2({
	// 	width: '100%',
	// 	placeholder: function(){
	// 			$(this).data('placeholder');
	// 	}
	// 	// minimumResultsForSearch: Infinity,
	// });

	// // --- custom select
	// //-----------------------------------------------------------------------------------------------------------------------


	// //-----------------------------------------------------------------------------------------------------------------------
	// // --- Open menu

	// $('.menu-trigger').click(function (e) {
	// 	if ($(this).hasClass('open')) {
	// 		$(this).removeClass('open');
	// 		$('.main-menu').removeClass('menu-open');
	// 		$('body').removeClass('body-menu-open');
	// 		$('html').removeClass('modal-open');
	// 	} else {

	// 		var pos = $('main').offset();
	// 		var scroll_to = $(window).scrollTop();
	// 		console.log(pos.top - scroll_to);
	// 		$('.main-menu').css('top', pos.top - scroll_to);

	// 		$(this).addClass('open');
	// 		$('.main-menu').addClass('menu-open');
	// 		$('body').addClass('body-menu-open');
	// 		$('html').addClass('modal-open');

	// 	}
	// 	e.preventDefault();
	// });

	// // --- End Open menu
	// //-----------------------------------------------------------------------------------------------------------------------


	// //---- Open/Close Search block
	// $('.search-trigger').click(function (e) {
	// 	if ($(this).hasClass('search-open')) {
	// 		$('.search-block').fadeOut(200).find('input').blur();
	// 		$(this).removeClass('search-open');
	// 		$('main, .footer').removeClass('show');
	// 	} else {
	// 		$('.search-block').fadeIn(200).find('input').focus();
	// 		$(this).addClass('search-open');
	// 		$('main, .footer').addClass('show');
	// 	}
	// 	e.preventDefault();
	// });
	//---- End Open Search block


	//-----------------------------------------------------------------------------------------------------------------------
	// --- Accordion

	// $('.toggle-block .toggle-button').click(function (e) {
	// 	if ($(this).hasClass('open-toggle')) {
	// 		$(this).removeClass('open-toggle');
	// 		$(this).parent().find('.toggle-panel').stop().slideUp(300);
	// 		$(this).parent().parent().removeClass('open');
	// 	} else {

	// 		if (owl_article.length) {
	// 			owl_article.trigger("refresh.owl.carousel");
	// 		}
	// 		// if (!$(this).parent().hasClass('toggle-block-item')) {
	// 		//  $('.toggle-block .toggle-button').removeClass('open-toggle');
	// 		//  $('.toggle-block .toggle-panel').stop().slideUp(300);
	// 		// }
	// 		$(this).addClass('open-toggle');
	// 		$(this).parent().find('.toggle-panel').stop().slideDown(300);
	// 		$(this).parent().parent().addClass('open');

	// 	}
	// 	e.preventDefault();
	// });

	// --- End Accordion
	//-----------------------------------------------------------------------------------------------------------------------






	// lazyLoad();



});



// var lazyLoad = function() {

// 	var images_bck = $('.lazy-load-wrapper:not(.loaded) .data-lazy-bck');
// 	var images_bck_small = $('.lazy-load-wrapper:not(.loaded) .data-lazy-bck-small');

// 	$('.lazy-load-img:not(.loaded) .lazy-img').each(function() {
// 		if ($(this).prop('complete')) {
// 			$(this).parent().addClass('loaded loaded-from-cache');
// 		} else {
// 			$(this).load(function() {
// 				$(this).parent().addClass('loaded');
// 			});
// 		}
// 	});

// 	$('.lazy-load-wrapper:not(.loaded) .lazy-img').each(function() {
// 		if ($(this).prop('complete')) {
// 			$(this).parent().addClass('loaded loaded-from-cache').css('background-image', 'url(' + $(this).attr("src") + ')');
// 			// $(this).parents('.header-wrap').addClass('loaded-wrap');
// 		} else {
// 			$(this).load(function() {
// 				$(this).parent().addClass('loaded').css('background-image', 'url(' + $(this).attr("src") + ')');
// 				// $(this).parents('.header-wrap').addClass('loaded-wrap');
// 			});
// 		}
// 	});
// }



//-----------------------------------------------------------------------------------------------------------------------
// --- Close Search block

// $(document).click( function(e){
// 	if( $(e.target).closest('.search-block, .search-trigger, .search-open').length )
// 		return;
// 	$('.search-trigger').removeClass('open');
// 	$('.search-block').removeClass('open');
// 	$('.search-block .search-form input').blur();
// 	setTimeout(function() {
// 		$('.header').removeClass('search-open');
// 	}, 150);



// 	if (window_w > 767) {

// 		if( $(e.target).closest('.lang-switcher').length )
// 			return;

// 		$('.lang-switcher .dropdown-trigger').removeClass('dropdown-open');
// 		$('.lang-switcher .dropdown-menu').slideUp(150);

// 		if( $(e.target).closest('.main-menu-block').length )
// 			return;

// 		$('.header__main-menu .with-submenu').removeClass('open-submenu');
// 		$('.header__main-menu .with-submenu .sub-menu').stop().slideUp(150);

// 		// console.log('777');

// 	}
// 	e.stopPropagation();
// });



// $('.search-trigger').click(function (e) {
// 	if ($(this).hasClass('open')) {
// 		$(this).removeClass('open');
		// setTimeout(function() {
		// 	$('.header').removeClass('search-open');
		// }, 150);
// 		$('.search-block').removeClass('open');
// 	} else {
// 		$(this).addClass('open');
// 		setTimeout(function() {
// 			$('.header').addClass('search-open');
// 		}, 150);
// 		$('.search-block').addClass('open');
// 		// $('.lang-switcher .dropdown-menu').slideDown(250);
// 	}
// 	e.preventDefault();
// });






// $(document).on('keyup',function(evt) {
// 	if (evt.keyCode == 27) {
// 		if ($('.search-trigger').hasClass('open')) {
// 			$('.search-trigger').removeClass('open');
// 			$('.search-block').removeClass('open');
// 			$('.search-block .search-form input').blur();
// 			setTimeout(function() {
// 				$('.header').removeClass('search-open');
// 			}, 150);
// 		}

// 		if ($('.lang-switcher .dropdown-trigger').hasClass('dropdown-open')) {
// 			$('.lang-switcher .dropdown-trigger').removeClass('dropdown-open');
// 			$('.lang-switcher .dropdown-menu').slideUp(150);
// 		}

// 		if ($('.header__main-menu .with-submenu').hasClass('open-submenu')) {
// 			$('.header__main-menu .with-submenu').removeClass('open-submenu');
// 			$('.header__main-menu .with-submenu .sub-menu').stop().slideUp(150);
// 		}

// 	}
// });

// --- End Close Search block
