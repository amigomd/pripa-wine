<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\SpokenLanguagesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\SpokenLanguagesController Test Case
 */
class SpokenLanguagesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.spoken_languages',
        'app.members',
        'app.languages',
        'app.translations',
        'app.specializations',
        'app.specializations_title_translation',
        'app.i18n',
        'app.companies',
        'app.slugs',
        'app.companies_meta_description_translation',
        'app.companies_meta_title_translation',
        'app.companies_meta_keywords_translation',
        'app.companies_specializations',
        'app.members_specializations',
        'app.companies_members',
        'app.members_spoken_languages'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
