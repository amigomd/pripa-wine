<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\TagsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\TagsController Test Case
 */
class TagsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.tags',
        'app.news',
        'app.news_title_translation',
        'app.news_content_translation',
        'app.news_meta_description_translation',
        'app.news_meta_title_translation',
        'app.news_meta_keywords_translation',
        'app.news_description_translation',
        'app.i18n',
        'app.slugs',
        'app.languages',
        'app.translations',
        'app.news_categories_table',
        'app.tags_news'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
