<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MoviesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\MoviesController Test Case
 */
class MoviesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.movies',
        'app.slugs',
        'app.languages',
        'app.translations',
        'app.directors',
        'app.producers',
        'app.companies',
        'app.companies_meta_description_translation',
        'app.companies_meta_title_translation',
        'app.companies_meta_keywords_translation',
        'app.i18n',
        'app.specializations',
        'app.specializations_title_translation',
        'app.companies_specializations',
        'app.members',
        'app.members_specializations',
        'app.companies_members',
        'app.spoken_languages',
        'app.spoken_languages_title_translation',
        'app.members_spoken_languages',
        'app.members_movies',
        'app.companies_movies',
        'app.genres',
        'app.genres_title_translation',
        'app.genres_movies'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
