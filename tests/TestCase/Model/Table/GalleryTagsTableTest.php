<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GalleryTagsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GalleryTagsTable Test Case
 */
class GalleryTagsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GalleryTagsTable
     */
    public $GalleryTags;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.gallery_tags',
        'app.galleries',
        'app.images',
        'app.modules',
        'app.permissions',
        'app.roles',
        'app.users',
        'app.user_settings',
        'app.actions_modules',
        'app.actions',
        'app.first_image',
        'app.gallery_tags_galleries'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('GalleryTags') ? [] : ['className' => 'App\Model\Table\GalleryTagsTable'];
        $this->GalleryTags = TableRegistry::get('GalleryTags', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->GalleryTags);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
