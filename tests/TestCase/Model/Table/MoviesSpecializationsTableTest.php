<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MoviesSpecializationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MoviesSpecializationsTable Test Case
 */
class MoviesSpecializationsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MoviesSpecializationsTable
     */
    public $MoviesSpecializations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.movies_specializations',
        'app.movies',
        'app.slugs',
        'app.languages',
        'app.translations',
        'app.directors',
        'app.producers',
        'app.companies',
        'app.companies_meta_description_translation',
        'app.companies_meta_title_translation',
        'app.companies_meta_keywords_translation',
        'app.i18n',
        'app.services',
        'app.companies_services',
        'app.companies_movies',
        'app.genres',
        'app.genres_title_translation',
        'app.genres_movies',
        'app.specializations',
        'app.specializations_title_translation',
        'app.companies_specializations',
        'app.members',
        'app.members_specializations',
        'app.companies_members',
        'app.spoken_languages',
        'app.spoken_languages_title_translation',
        'app.members_spoken_languages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MoviesSpecializations') ? [] : ['className' => 'App\Model\Table\MoviesSpecializationsTable'];
        $this->MoviesSpecializations = TableRegistry::get('MoviesSpecializations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MoviesSpecializations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
