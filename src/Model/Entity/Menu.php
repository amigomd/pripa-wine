<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;

/**
 * Menu Entity
 *
 * @property int $id
 * @property bool $published
 * @property int $position
 */
class Menu extends Entity
{
    use TranslateTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    public function getTitle(){
        return $this->page && $this->page->title ? $this->page->title : $this->title;
    }

    public function getHref(){
        $href = '';
        if($this->type == 1 && $this->page){
            $ex = explode('/',$this->page->module);
            if(!empty($ex)) {
                $url = [];
                $url['controller'] = $ex[0];
                $url['language'] = LANG;
                $url['action'] = 'index';
                if (count($ex) == 2) {
                    $url['action'] = $ex[1];
                    if(isset($this->page->slug[LANG])){
                        $url['slug'] = $this->page->slug[LANG];
                    }
                }
                $href = \Cake\Routing\Router::url($url);
            }
        }elseif($this->type == 1){
            $href = $this->external_link;
        }
        return $href;
    }
}
