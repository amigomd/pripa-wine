<?php
namespace App\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;

/**
 * Category Entity
 *
 * @property int $id
 * @property string $icon
 * @property bool $published
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\Menu[] $menus
 */
class Category extends AbstractEntity
{
    use TranslateTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];

    public function svgColor(){
        $file = 'common/svg_categories/'.$this->icon_svg;
        if(is_file($file)){
            $file = file_get_contents($file);
            $pos = strpos($file, 'fill="#');
            if($pos !== false){
                return substr($file,$pos+6,7);
            }
        }
        return '#E67E22';
    }
}
