<?php
namespace App\Model\Table;

use Cake\Validation\Validator;
use Cake\ORM\Behavior\Translate\TranslateTrait;

/**
 * Menus Model
 *
 * @method \App\Model\Entity\Menu get($primaryKey, $options = [])
 * @method \App\Model\Entity\Menu newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Menu[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Menu|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Menu patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Menu[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Menu findOrCreate($search, callable $callback = null, $options = [])
 */
class AwardsTypeTable extends AbstractTable
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('awards_type');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');



    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {

        /*
                $validator
                    ->integer('id')
                    ->allowEmpty('id', 'create');

                $validator
                    ->boolean('published')
                    ->allowEmpty('published');

                $validator
                    ->integer('position')
                    ->allowEmpty('position');*/

        return $validator;
    }
}
