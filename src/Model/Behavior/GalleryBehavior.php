<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Inflector;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;
use App\Controller\Component\AmigoFileComponent;
use Cake\Filesystem\Folder;

/**
 * Image behavior
 */
class GalleryBehavior extends Behavior
{

	/**
	 * Default configuration.
	 *
	 * @var array
	 */
	protected $_defaultConfig = [
		'fields' => [],
	];


	public function afterSave(Event $event, EntityInterface $entity)
	{
		//single image copy from tmp to new folder
		$module_id = dev_conf('Modules')[$event->subject()->getAlias()]->id;
		if($module_id && $entity->gallery) {
			$galleryTable = \Cake\ORM\TableRegistry::get('Images');
			$i = 0;
			foreach ($entity->gallery as $random_name_folder => $image_name){
				if(!is_int($random_name_folder)) {
					$gallery = $galleryTable->newEntity();
					$gallery->name = $image_name;
					$gallery->module_id = $module_id;
					$gallery->foreign_key = $entity->id;
					$gallery->position = $i;
					$save = $galleryTable->save($gallery);
					if($save){
						$source = IMAGE_PATH.strtolower($event->subject()->getAlias()).DS.'gallery'.DS.IMAGE_PATH_TMP.DS.$random_name_folder;
						$destination = IMAGE_PATH.strtolower($event->subject()->getAlias()).DS.'gallery'.DS.$gallery->id;
						$dir = new Folder();
						$copy = $dir->copy([
							'to' => $destination,
							'from' => $source,
							'mode' => 0777,
						]);
						if($copy){
							$deleteTmpFolder = new Folder($source);
							$deleteTmpFolder->delete();
						}
					}
				}else{
					$gallery = $galleryTable->get($random_name_folder);
					$gallery->position = $i;
					$save = $galleryTable->save($gallery);
				}
				$i++;
			}
		}
	}

}
