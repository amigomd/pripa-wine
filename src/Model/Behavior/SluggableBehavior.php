<?php
namespace App\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use Cake\Utility\Inflector;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;

/**
 * Sluggable behavior
 */
class SluggableBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'field' => 'title',
        'slug' => 'slug',
        'replacement' => '-',
    ];

    /**
     * @param Entity $entity
     */
    public function slug(Entity $entity,$alias)
    {
        $config = $this->config();
        $slugTable = TableRegistry::get('Slugs');
        $module_id = dev_conf('Modules')[$alias]->id;
        $dev_conf = dev_conf('Config.'.$alias.'.slug_fields');
        $slugFields = $dev_conf ? $dev_conf : ['title'];

        foreach (lang() as $language) {
			if(!$language->admin) continue;
			$str = '';
        	foreach ($slugFields as $slugField){
        		if($entity->get('_translations')){
					$str .= $entity->get('_translations')[$language->code]->{$slugField}.'-';
				}else{
					$str .= $entity->{$slugField}.'-';
				}
			}

			//if user custom enter slug
			if(isset($entity->custom_slug[$language->id]) && $entity->custom_slug[$language->id] == 'on'){
				$slugConverter = Inflector::slug($entity->_translations[$language->code]['slug'], $config['replacement']);
			}else{
				$slugConverter = Inflector::slug($str, $config['replacement']);
			}

			$slugConverter = $this->getTranslit($slugConverter);
			if (trim($slugConverter) != '' && (!isset($entity['slug'][$language->code]) || (isset($entity['slug'][$language->code]) && $slugConverter != $entity['slug'][$language->code]))) {
				$checkSlug = $slugTable->find()->where(['language_id' => $language->id,'module_id' => $module_id,'slug' => $slugConverter,'foreign_key !=' => $entity->id])->first();
				if($checkSlug){
					$slugConverter = $slugConverter .'-'.$entity->id;
				}

				$existSlug = $slugTable->find()->where(['language_id' => $language->id,'module_id' => $module_id,'slug' => $slugConverter])->first();
				$update = $slugTable->query()->update()->set(['status' => 0])->where(['language_id' => $language->id,'module_id' => $module_id,'foreign_key' => $entity->id])->execute();
				if ($existSlug) {
					$slug = $existSlug;
				} else {
					$slug = $slugTable->newEntity();
					$slug->created = date('Y-m-d H:i:s');
				}
				$slug->language_id = $language->id;
				$slug->module_id = $module_id;
				$slug->status = 1;
				$slug->foreign_key = $entity->id;
				$slug->slug = $slugConverter;
				$slugTable->save($slug);
			}
		}
    }

    public function afterSave(Event $event, EntityInterface $entity)
    {
		//exception for pages module
		if($event->subject()->getAlias() == 'Pages'){
//			if(empty($entity->slug) || !$entity->module){
				$this->slug($entity,$event->subject()->getAlias());
//			}
		}else{
			$this->slug($entity,$event->subject()->getAlias());
		}

    }

    public function beforeFind(Event $event, Query $query, $options)
    {
        $alias = $event->subject()->getAlias();
        $module_id = dev_conf('Modules')[$alias]->id;

        $query->contain([
            'Slugs' => [
                'queryBuilder' => function ($q) use ($module_id) {
                    return $q->where(["module_id"=>$module_id,'status'=>1])->order("id DESC");
                }
            ]
        ]);

        $query->formatResults(function ($results) {
            return $this->_rowMapper($results);
        }, $query::PREPEND);

        return $query;
    }

    protected function _rowMapper($results)
    {
        return $results->map(function ($row) {
            if ($row === null) {
                return $row;
            }
            $hydrated = !is_array($row);
            $langs = [];
            foreach (lang() as $language){
                $langs[$language->id] = $language;
            }
            $slugs = [];
			if(isset($row['slugs'])) {
				foreach ($row['slugs'] as $slug) {
					$slugs[$langs[$slug->language_id]->code] = $slug->slug;
				}
			}
//            var_dump($slugs);

            $row['slug'] = $slugs;
            if ($hydrated) {
                $row->clean();
            }

            return $row;
        });
    }


    /**
     * Transliterates a string.
     */
    public function getTranslit($str) {
        // Replace Pattern
        $symbols = array(
            // Slavic
            'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g',
            'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh',
            'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
            'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
            'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'ts',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ъ' => 'j',
            'ы' => 'y', 'ь' => 'i', 'э' => 'e', 'ю' => 'yu',
            'я' => 'ya',
        );

        // Return Transliterated String
        return str_replace(array_keys($symbols), $symbols, mb_strtolower($str));
    }
}
