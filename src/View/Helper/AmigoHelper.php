<?php

namespace App\View\Helper;

use Cake\View\Helper;

class AmigoHelper extends Helper
{
	function datetime2MonthName($datetime, $language = 'rom') {
		// Eng
		if ($language == 'eng') {
			return strftime('%B', strtotime($datetime));
		}

		// Other languages
		$datetime = explode(' ', $datetime);
		$date = explode('-', $datetime[0]);
		$month = '';
		switch ($date[1]) {
			case '01': $month = $language == 'rom' ? 'Ianuarie' : 'Января';
				break;
			case '02': $month = $language == 'rom' ? 'Februarie' : 'Февраля';
				break;
			case '03': $month = $language == 'rom' ? 'Martie' : 'Марта';
				break;
			case '04': $month = $language == 'rom' ? 'Aprilie' : 'Апреля';
				break;
			case '05': $month = $language == 'rom' ? 'Mai' : 'Мая';
				break;
			case '06': $month = $language == 'rom' ? 'Iunie' : 'Июня';
				break;
			case '07': $month = $language == 'rom' ? 'Iulie' : 'Июля';
				break;
			case '08': $month = $language == 'rom' ? 'August' : 'Августа';
				break;
			case '09': $month = $language == 'rom' ? 'Septembrie' : 'Сентября';
				break;
			case '10': $month = $language == 'rom' ? 'Octombrie' : 'Октября';
				break;
			case '11': $month = $language == 'rom' ? 'Noiembrie' : 'Ноября';
				break;
			case '12': $month = $language == 'rom' ? 'Decembrie' : 'Декабря';
				break;
		}

		return $month;
	}

	function toNormalDate($datetime, $language = 'rom',$short=true) {
		if (empty($datetime)) {
			return 'empty';
		}
		if ($language == 'eng' || empty($language)) {
			return mb_strtolower(strftime('%d %B %Y', strtotime($datetime)));
		}

		$month = $this->datetime2MonthName($datetime, $language);

		$datetime = explode(' ', $datetime);
		$date = explode('-', $datetime[0]);
		$fullMonth = mb_strtolower($month, 'UTF-8');
		if($short){
			$_month = mb_substr($fullMonth,0,3);
		}else{
			$_month = $fullMonth;
		}
		return (int) $date[2] . ' ' . $_month . ' ' . $date[0];
	}

	public function toNormalTime($duration){
		if ($duration >= 60)
		{
			$min = (int)($duration / 60);
			$sec = $duration % 60;
		}
		return $min.'min'.' '.$sec.'sec';
	}
}