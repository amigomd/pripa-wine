<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         3.0.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\View\Widget;

use Cake\View\Form\ContextInterface;

/**
 * Input widget class for generating a textarea control.
 *
 * This class is intended as an internal implementation detail
 * of Cake\View\Helper\FormHelper and is not intended for direct use.
 */
class TextareaWidget extends \Cake\View\Widget\BasicWidget
{
    /**
     * Render a text area form widget.
     *
     * Data supports the following keys:
     *
     * - `name` - Set the input name.
     * - `val` - A string of the option to mark as selected.
     * - `escape` - Set to false to disable HTML escaping.
     *
     * All other keys will be converted into HTML attributes.
     *
     * @param array $data The data to build a textarea with.
     * @param \Cake\View\Form\ContextInterface $context The current form context.
     * @return string HTML elements.
     */
    public function render(array $data, ContextInterface $context)
    {
        $data += [
            'val' => '',
            'name' => '',
            'escape' => true,
            'rows' => 5,
            'templateVars' => []
        ];

        //set initial variable
        $value = $data['val'];
        $templateVars = $data['templateVars'];
        $name = $data['name'];
		if(get_class($context) !=  'Cake\View\Form\NullContext'){
			$entity = $context->entity();
		}

        // template for language
        if(isset($data['lang']) && $data['lang'] === true){
            $languages = [];
            $data['templateVars']['text'] = isset($templateVars['label']) ? $templateVars['label'] : '';
            foreach (lang() as $language) {
				if(!$language->admin) continue;
                if(!isset($data['prefix'])){
                    $data['val'] = $entity->translation($language->code)->{$name};
                    $data['id'] = 'translations-'.$language->code.'-'.$name;
                    $data['name'] = '_translations['.$language->code.']['.$name.']';
                }else{
                    $data['val'] = $value[$data['prefix'].$language->code];
                }
                $data['templateVars']['lang'] = $language->title;
                $data['templateVars']['class'] = $templateVars['class'].' lang-group lang-'.$language->code;
                $languages[] = $this->_templates->format('languageTextarea', [
                    'name' => $data['name'],
                    'value' => $data['escape'] ? h($data['val']) : $data['val'],
                    'templateVars' => $data['templateVars'],
                    'attrs' => $this->_templates->formatAttributes(
                        $data,
                        ['name', 'type']
                    ),
                ]);
            }
            return $languages;
        }

        return $this->_templates->format('textarea', [
            'name' => $data['name'],
            'value' => $data['escape'] ? h($data['val']) : $data['val'],
            'templateVars' => $data['templateVars'],
            'attrs' => $this->_templates->formatAttributes(
                $data,
                ['name', 'val']
            )
        ]);
    }
}
