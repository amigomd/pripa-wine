<?php
namespace App\View\Cell;

use Cake\View\Cell;

class NavigationCell extends Cell
{

    public function display()
    {
        // key is controller name uppercase eg. Users
        $menu = [
          'Menus' => [
               'name' => 'Menu',
               'icon' => 'align-left',
               'link' => 'menus'
           ],
            'Pages' => [
                'name' => 'Pages',
                'icon' => 'file-text',
                'link' => 'pages'
            ],

            'Gallery' => [
                'name' => 'Gallery',
                'icon' => 'camera',
                'link' => 'gallery'
            ],
            'Wines' => [
                'name' => 'Wines',
                'icon' => 'glass ',
                'link' => 'wines'
            ],
            'Shops' => [
                'name' => 'Shops',
                'icon' => 'shopping-cart',
                'link' => 'shops'
            ],
            'Domains' => [
                'name' => 'Domains',
                'icon' => 'file-text',
                'link' => 'domains',

            ], 'Awards' => [
                'name' => 'Awards',
                'icon' => 'trophy',
                'link' => 'awards',

            ],
            'Users' => [
                'name' => 'Users',
                'icon' => 'user',
                'link' => 'users',
                'submenu' => [
                    'Modules' => [
                        'name' => 'Modules',
                        'link' => 'modules',
                    ],
                    'Roles' => [
                        'name' => 'Roles',
                        'link' => 'roles',
                    ],
                ],
            ],



            'Settings' => [
                'name' => 'Settings',
                'icon' => 'cogs',
                'link' => 'settings',

                'submenu' => [
                    'Translations' => [
                        'name' => 'Translations',
                        'icon' => 'list',
                        'link' => 'translations'
                    ],
                ]
            ],
        ];

        $permissions = [];
        $superUser = false;
		$userData = null;
        if ($userId = $this->request->session()->read('Auth.User.id') ) {
            $roleId = $this->request->session()->read('Auth.User.role_id');
            $userData = $this->request->session()->read('Auth.User');
            if ($roleId) {
                $this->loadModel('Permissions');
                $permissions = $this->Permissions->find('list',[
                    'keyField' => 'actions_module.action.name',
                    'valueField' => 'actions_module.action.id',
                    'groupField' => 'actions_module.module.name',
                ])->contain([
                    'ActionsModules.Modules',
                    'ActionsModules.Actions'
                ])->where([
                    'Permissions.role_id' => $roleId,
                    'Permissions.allow' => true,
                ])->toArray();
            }

            if ( in_array($userId, [AMIGO_USER_ID, ADMIN_USER_ID]) ) {
                $superUser = true;
            }
        }

        $this->set(compact('menu','permissions','superUser','userData'));
    }

}
