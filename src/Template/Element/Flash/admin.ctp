<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<header class="header">
    <div class="message message-<?php echo $params['class'] ?> animated slideInDown">
        <span><i class="icon-exclamation-triangle"></i> <?php echo $message ?></span>
    </div>
</header>
<script>
	setTimeout(function(){
		$('.message.animated').slideToggle();
	},5000)
</script>