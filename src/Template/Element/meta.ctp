		<?php
		//$title_for_layout = $page_title == 'Errors' ? __($page_title) : $page_title;
		$title = isset($page_title) ? ($page_title ? $page_title . ' – ' : '') . conf('Config.projectName-'.LANG) : conf('Config.projectName-'.LANG);
		$baseUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://':'http://').$_SERVER['HTTP_HOST'].'/';
		$url = $baseUrl . seoify();
		if(isset($meta_description) && $meta_description){
		    $description = $meta_description;
		}elseif(isset($page_content) && $page_content){
		    $description = $page_content;
		}elseif (empty($description)){
		    $description = conf('Meta.description-' . LANG);
		}
		$og_image = !empty($og_image) ? $og_image : dev_conf('Config.fullUrl').'img/logo.jpg';
		?>

		<title><?php echo isset($title) ? summarize($title,70) : ''; ?></title>
		<base href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://':'http://').$_SERVER['HTTP_HOST'].'/' ?>">
		<meta name="description" content="<?php echo escape_meta(summarize($description, 152)) ?>">

		<!-- Open Graph -->
		<meta property="og:type" content="website">
		<meta property="og:site_name" content="<?php echo escape_meta(conf('Config.projectName-'.LANG)) ?>">
		<meta property="og:title" content="<?php echo escape_meta($title) ?>">
		<meta property="og:description" content="<?php echo escape_meta(summarize($description, 456)) ?>">
		<meta property="og:url" content="<?php echo $url ?>">
		<meta property="og:image" content="<?php echo $og_image ;?>">

		<link rel="canonical" href="<?php echo $url ?>">
