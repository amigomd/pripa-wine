<footer class="footer">
    <div class="container">
        <?= nested_recursive_menu_hom_ft($menu22, $this, true) ?>

        <?php  if (conf('Social.fb') != null) { ?>
            <a class="facebook" href="<?=conf('Social.fb');?>">
                <svg width="11" height="20" viewBox="0 0 11 20" xmlns="http://www.w3.org/2000/svg">
                    <path d="M10.27 1v2.94H8.52c-1.37 0-1.63.66-1.63 1.6v2.11h3.27l-.43 3.3H6.89v8.48H3.48v-8.47H.63v-3.3h2.85V5.21C3.48 2.4 5.2.86 7.73.86c1.2 0 2.24.09 2.54.13z" fill="#FFF" fill-rule="evenodd"/>
                </svg>
            </a>
        <?php } else { ?>
            <a class="facebook" href="<?=conf('Social.gps');?>" target="_blank">
                <svg width="18" height="22" viewBox="0 0 18 22" xmlns="http://www.w3.org/2000/svg">
                  <path d="M9 1a8.03 8.03 0 0 1 7.28 11.37c-.86 2.03-2.76 5.16-6.92 8.5a.57.57 0 0 1-.7 0c-4.16-3.34-6.06-6.47-6.92-8.5a7.91 7.91 0 0 1-.73-3.35A8.02 8.02 0 0 1 9 1zm6.75 9.32A6.89 6.89 0 0 0 9 2.12a6.89 6.89 0 0 0-6.75 8.2v.03l.04.18c.11.48.27.94.47 1.38v.01c.79 1.86 2.5 4.7 6.23 7.78 3.73-3.09 5.45-5.92 6.23-7.78v-.01c.21-.44.37-.9.47-1.37v-.01l.04-.18v-.03zM9 5a4 4 0 1 1-.01 8.01A4 4 0 0 1 9 5zm0 7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" fill="#FFF" fill-rule="nonzero" stroke="#FFF" stroke-width=".9"/>
                </svg>
            </a>
        <?php }  ?>

        <img class="line" src="../img/line.png" width="274px">

        <div class="footer-links">
            <?php  if (conf('Social.fb') != null) { ?>
                <a href="<?=conf('Social.gps');?>"><?= __ds('GPS coordinates') ?></a>
            <?php }  ?>
            <a href="https://amigo.studio/" target="_blank">Website by amigo.studio</a>
        </div>
    </div>
</footer>
