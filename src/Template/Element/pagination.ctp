<?php
$url['url']['language'] = LANG;
if(isset($this->request->query['sort'])){
	$url['url']['sort'] = $this->request->query['sort'] == 'asc' ? 'desc' : 'asc';
}
if(isset($this->request->query['category'])){
	$url['url']['category'] = (int)$this->request->query['category'];
}
$this->Paginator->options($url);
$this->Paginator->setTemplates([
	'number'       => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
	'current'      => ' <li class="page-item active"><a class="page-link" href="#">{{text}}</a></li>',
	'prevActive'   => '<a class="pager-prev" href="{{url}}"><i class="icon-caret-left"></i></a>',
	'nextActive'   => '<a class="pager-next" href="{{url}}"><i class="icon-caret-right"></i></a>',
	'nextDisabled' => '',
	'prevDisabled' => '',
	'ellipsis'=> '<a>...</a>'
]);
?>
<nav aria-label="Page navigation">
	<ul class="pagination text-center list-pagination">
		<?= $this->Paginator->numbers(['first' => 1, 'last' => 1, 'modulus' => 4]) ?>
	</ul>
</nav>