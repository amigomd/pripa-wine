<section class="subscribe-section section-grey">
    <div class="subscribe-section-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 ">
                    <h4><?php echo __ds('Subscribe to stay updated with our latest Top7 updates');?></h4>
                    <p><?php echo __ds('Your email will never be shared with anyone for any reason!');?></p>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <form class="subscribe-form clearfix"
                          action="https://top7.agilecrm.com/formsubmit<?//php echo $this->Url->build(['controller' => 'Pages', 'action' => 'subscribe','language' => (LANG == 'en' ? null : LANG)]); ?>">

                        <input type="hidden" id="_agile_form_name" name="_agile_form_name" value="Newsletter Subscribe">
                        <input type="hidden" id="_agile_domain" name="_agile_domain" value="top7">
                        <input type="hidden" id="_agile_api" name="_agile_api" value="u6cc39k80rggk2gps9sugoiuhe">
                        <input type="hidden" id="_agile_redirect_url" name="_agile_redirect_url" value="https://top7.io/thanks">
                        <input type="hidden" id="_agile_document_url" name="_agile_document_url" value="">
                        <input type="hidden" id="_agile_confirmation_msg" name="_agile_confirmation_msg" value="">
                        <input type="hidden" id="_agile_form_id_tags" name="tags" value="">

                        <input type="hidden" id="_agile_form_id" name="_agile_form_id" value="5674823384563712">

                        <div class="form-row">
                            <input class="form-input" placeholder="Your email address" name="email" type="email" required="">
                        </div>
                        <button class="btn btn-medium btn-navy" type="submit"><span><?php echo __ds('Subscribe me');?></span></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
