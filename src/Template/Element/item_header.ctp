<div class="grid center-vertically page-header">
	<div class="hidden-xs col-sm-3">
		<a class="btn-circle btn-circle-back" href="<?php echo
		$this->request->session()->read('BackFront.' . MODEL_NAME) ?
			$this->request->session()->read('BackFront.' . MODEL_NAME) :
			$this->Url->build(['controller'=>CONTROLLER_NAME, 'action'=>'index', 'language'=>LANG]
		); ?>">
			<svg width="50" height="50" viewBox="0 0 50 50" xmlns="http://www.w3.org/2000/svg">
				<g fill="none" fill-rule="evenodd">
					<circle fill="#fff" cx="25" cy="25" r="25"/>
					<path d="M18.4 26H35v-2H18.9l4.6-4.6-1.43-1.4-6 6H16v.07l-1 1 .17.17-.17.17 7.07 7.1 1.42-1.43L18.4 26z" fill="#7eddd1"/>
				</g>
			</svg>
		</a>
	</div>
	<div class="col-xs-12 col-sm-6">
		<h1 class="page-title"><?php echo isset($title) ? $title : $page->title ?></h1>
	</div>
	<?php if(isset($page->views)){?>
		<div class="col-md-3 page-header-navigation text-right">
			<span class="counter page-header-counter"><b><?php echo $page->views; ?></b> <?php echo __ds('vizualizări'); ?></span>
		</div>
	<?php } ?>
</div>