<div class="breadcrumbs">
	<div class="container clearfix">
		<div class="breadcrumbs-block clearfix">
			<a href="<?php echo $this->Url->build(['controller'=>'Pages','action'=>'display','language'=>LANG]); ?>"><?= __ds('Pagina principală') ?></a><span>–</span>
			<?php if(!empty($breadcrumbs)){
				$last = $breadcrumbs[count($breadcrumbs)-1];
				unset($breadcrumbs[count($breadcrumbs)-1]);
				if(!empty($breadcrumbs)){
					foreach ($breadcrumbs as $breadcrumb){?>
						<a href="<?php echo $breadcrumb['url']?>"><?php echo $breadcrumb['title']?></a><span>–</span>
					<?php } ?>
				<?php } ?>
				<?php echo $last['title'];?>
			<?php } ?>
		</div>
	</div>
</div>