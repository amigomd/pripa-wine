<form id="admin-login-form" method="POST">
	<h1><?php echo conf('Config.projectName-rom') ?></h1>
	<div class="col-block">
		<label>Username</label>
		<input class="form-control" type="text" name="username" >
	</div>
	<div class="col-block">
		<label>Password</label>
		<div class="password-block">
			<span class="show-pass">
				<i class="icon-eye"></i>
				<i class="icon-eye-slash"></i>
			</span>
			<input class="form-control" type="password" name="password">
		</div>
	</div>
	<div class="col-block">
		<input value="Submit" class="btn btn-medium btn-block btn-submit btn-green" type="submit">

		<a class="btn btn-grey-empty btn-block" href="/admin/users/recovery">Forgot password?</a>
	</div>
</form>
