<?php
echo $this->element('header', ['itemPage' => true,'langSwitcher'=>true]);

$isset_publish = $page->isNew() || is_bool($page->published) || is_int($page->published) ? true : false;

echo $this->Html->div('content-block',
    $this->Form->create($page, ['class' => "form-page"])
    .
    $this->Form->control('id',['type' => 'hidden'])
    .
    $this->Html->div('row',
        $this->Form->control('full_name',[
            'lang'=>true,
            'label'=>false,
            'templates' => ['inputContainer' => '{{content}}'],
            'templateVars' => [
                'label'=>__da('Full Name'),
                'class' => 'col-sm-12',
                'inputclass' => 'title-input'
            ]
        ])
    )
    .
    $this->Html->div('row',
        $this->Form->control('job_title', [
            'lang' => true,
            'label' => false,
            'templates' => ['inputContainer' => '{{content}}'],
            'templateVars' => [
                'label' => __da('Job Title'),
                'class' => 'col-sm-12'
            ]
        ])
    )
    .
    $this->Html->div('row',
        $this->Form->control('image', [
            'type' => 'hidden',
            'templateVars' => [
                'label' => __da('Image'),
                'class' => 'col-sm-3'
            ]
        ])
    )
    .
    $this->element('index/save_actions')
    .
    $this->Form->end()
);
?>