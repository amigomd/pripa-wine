<?php

echo $this->element('header');

echo $this->element('index/table',[
    'model_name' => MODEL_NAME,
    'items' => ${Cake\Utility\Inflector::variable(CONTROLLER_NAME)},
    'fields' => [
        [
            'checkboxes' => true,
        ],
        [
            'name' => 'id',
            'view_name' => '#',
            'class' => 'table-position',
        ],
        [
            'name' => 'full_name',
            'view_name' => __da('Full Name'),
            'is_link' => true,
            'i18n' => true
        ],
        [
            'actions' => ['edit', 'delete', 'publish'],
        ],
    ],
]);
?>



