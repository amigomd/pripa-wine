<?php
    echo $this->element('header',['header'=>false]);
?>
<div class="content-controls clearfix">
    <?php if(count(dev_conf('Config.menuCategory'))>1) { ?>
        <div class="col-sm-2">
            <select class="dropdown select2-search" id="menu-cat-index">
                <?php foreach(dev_conf('Config.menuCategory') as $key => $type) { ?>
                    <option value="<?php echo $key;?>" <?php echo isset($this->request->query['type']) &&
                                                                        $this->request->query['type'] == $key ?
                                                                        'selected' : '' ?>><?php echo $type;?>
                    </option>
                <?php } ?>
            </select>
        </div>
    <?php } ?>
    <div class="col-sm-2">
        <a href="<?php echo $this->Url->build(['controller'=>'menus','action'=>'index','active'=>isset($this->request->query['active']) ? false : 1]+$this->request->query) ?>" class="btn btn-warning menu-filter">
            <?php echo isset($this->request->query['active']) ? __da('Show all') : __da('Hide inactive'); ?>
        </a>
    </div>
</div>
<div class="content-block" id="content">
    <?php echo nested_recursive_menu($menu,$this,true); ?>
</div>
<script>
    $(function () {
        $('.menu-filter').click(function (e) {
            e.preventDefault();
            $(this).toggleClass('clicked');

            u = new Url();
            if($(this).hasClass('clicked')){
                delete u.query['active'];
                tag = {active:0};
                $(this).html('<?php echo __da('Hide inactive'); ?>');
                $('.menu-unpublished').removeClass('hidden');
            }else{
                u.query['active'] = 1;
                tag = {active:1};
                $(this).html('<?php echo __da('Show all'); ?>');
                $('.menu-unpublished').addClass('hidden')
            }
            history.pushState(tag, "", u);
        });
        $(window).on("popstate", function (e) {
            location.reload();
        });
    });
</script>

