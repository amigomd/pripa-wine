<?php
echo $this->element('header', ['itemPage' => true,'langSwitcher'=>true]);
echo $this->Html->div('content-block',
    $this->Form->create($menu, array('class' => "form-page"))
    .
    $this->Html->div('row',
        $this->Form->control('category_id',[
            'options'=>dev_conf('Config.menuCategory'),
            'label'=>__da('Menu Category'),
            'templateVars' => [
                'class' => 'col-sm-2',
            ]
        ])
        .
        $this->Form->control('parent',[
            'options' => $menuParents,
            'value'=>is_null($menu->parent_id) ? '0' : $menu->parent_id,
            'label' => __da('Position'),
            'templateVars' => [
                'class' => 'col-sm-2',
            ]
        ])
        .
        $this->Form->control('type',[
            'options' => dev_conf('Config.menuPosition'),
            'label' => __da('Type'),
            'templateVars' => [
                'class' => 'col-sm-2',
            ]
        ])
        .
        $this->Form->control('page_id',[
            'options' => $pages,
            'label' => __da('Pages'),
            'class' => 'select2-search',
            'templateVars' => [
                'class' => 'col-sm-2 changed',
                'template'=>'select2',
                'divclass' => 'styled-select-select2'
            ]
        ])
        .
        $this->Form->control('external_link',[
            'value'=>$menu,
            'lang'=>true,
            'label'=>false,
            'templates' => ['inputContainer' => '<div class="col-block col-sm-6 changed">{{content}}</div>'],
            'templateVars' => [
                'label' => __da('External link'),
            ]
        ])
    )
    .
    $this->Html->div('row changed type'.($menu->type != 2 ? 'hidden' : ''),
        $this->Form->control('title',[
            'lang'=>true,
            'label'=>false,
            'templates' => ['inputContainer' => '{{content}}'],
            'templateVars' => [
                'label'=>__da('Title'),
                'class' => 'col-sm-12',
            ]
        ])
    )
    .
    $this->Html->div('row',
        $this->Form->control('published',[
            'type' => 'checkbox',
            'templateVars' => [
                'label'=>__da('Published'),
                'class' => 'col-sm-6 checkbox-block',
            ]
        ])
        .
        $this->Form->control(__da('Save'),[
            'type'=>'submit',
            'templateVars' => [
                'class' => 'btn btn-green btn-large pull-right',
                'divclass'=>'col-block col-sm-6 col-save-buttons'
            ]
        ])
    )
    .
    $this->Form->end()
);
?>
<script>
    $(function () {
        MenuEdit();
        $('#type').change(function () {
            MenuEdit();
        })
    });
    function MenuEdit() {
        $('.changed').addClass('hidden');
        $('.changed').eq(parseInt($('#type').val())-1).removeClass('hidden');
        if($('#type').val() == 0){
            $('.changed').addClass('hidden');
        }
        if($('#type').val() == 2 || $('#type').val() == 0){
            $('.typehidden').removeClass('hidden');
        }
    }
</script>
