<h3>Users Activity</h3>
<table class="table">
	<tr>
		<th>User</th>
		<th>Module</th>
		<th>Date</th>
		<th>Link</th>
	</tr>
	<?php foreach ($last_activity as $last):?>
		<tr>
			<td><a href="<?php echo '/admin/users/edit/'.$last->user->id;?>"><?php echo $last->user->full_name ?></a></td>
			<td><?php echo $last->controller ?></td>
			<td><?php echo toNormalDate(date('Y-m-d H:i:s', strtotime($last->created)),'rom',true).' '.date('H:i:s', strtotime($last->created)); ?></td>
			<td><a href="<?php echo '/admin/'.strtolower($last->controller).'/edit/'.$last->item_id;?>">link</a></td>
		</tr>
	<?php endforeach; ?>
</table>