<?php
    $class = (isset($class)) ? "form-control " . $class :  "form-control";
    $label = (isset($label)) ? $label : $name;
    $lang = (isset($lang)) ? $lang : false;
    $type = (isset($type)) ? $type : 'text';
?>

<?php if($lang){ ?>

    <?php foreach ($languages as $key => $language){?>
        <div class="lang-group lang-<?php echo $language['code'];?>" >
            <label for="ArticleTitleEng"><?php echo $label?> <span><?php echo $language['title'];?></span></label>
            <?php echo $this->Form->control(
                "_translations.$language[code].$name",
                [
                    'class' => $class,
                    'type' => $type,
                    'label' => false
                ]
            );?>
        </div>
    <?php } ?>

<?php }else{ ?>

    <label for="ArticleTitleEng"><?php echo $lable?> <span><?php echo $language['title'];?></span></label>
    <?php echo $this->Form->control(
        "_translations.$language[code].$name",
        [
            'class' => $class,
            'type' => $type,
            'label' => false
        ]
    );?>

<?php } ?>
