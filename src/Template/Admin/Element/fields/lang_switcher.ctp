<div class="lang-switcher clearfix">
    <?php
    $i = 0;
    foreach (lang() as $key => $language){
        if(!$language->admin) continue;
//        if(!\Cake\Core\Configure::read('Settings.Languages')[$language->code]) continue;
        ?>
        <a <?php echo $i==0 ? 'class="current"' : ''?> href="#" data-lang="<?php echo $language->code;?>">
            <?php echo $language->title;?>
        </a>
    <?php
        $i++;
    } ?>
    <span class="move-block"></span>
</div>