<?php
	if(!isset($header)) {
		$header = true;
	}
?>
<?php /* ?>
<header class="header hidden">
	<div class="message message-success animated slideInDown">
		<i class="icon-exclamation-triangle"></i><span></span>
	</div>
</header>
<header class="header hidden">
	<div class="message message-error animated slideInDown">
		<i class="icon-exclamation-triangle"></i><span></span>
	</div>
</header>
<?php */?>
<header class="content-header clearfix">
	<!-- page title is generated from js  -->
	<h2 class="page-title"></h2>

	<?php
	if (empty($itemPage)) {
		echo $this->element('index/add_button');
	} elseif ( ! empty($langSwitcher)) {
		echo $this->element('fields/lang_switcher');
	}
	?>

</header>

<?php
if (!empty($itemPage)) {

} else {
	if($header) {
		echo $this->element('../Admin/Element/header_actions');
	}
}
?>

<?php //echo $this->Flash->render() ?>
