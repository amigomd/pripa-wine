<div class="panel-group" id="accordion">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h4 class="panel-title">
				<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
					Seo
				</a>
			</h4>
		</div>
		<div id="collapseOne" class="panel-collapse collapse">
			<div class="panel-body">
				<?php echo
					$this->Html->div('row',
						$this->Form->control('meta_title',[
							'lang'=>true,
							'label'=>false,
							'templates' => ['inputContainer' => '{{content}}'],
							'templateVars' => [
								'label'=>__da('Meta Title'),
								'class' => 'col-sm-12',
							]
						])
					)
					.
					$this->Html->div('row',
						$this->Form->control('meta_description',[
							'lang'=>true,
							'label'=>false,
							'templates' => ['inputContainer' => '{{content}}'],
							'templateVars' => [
								'label'=>__da('Meta Description'),
								'class' => 'col-sm-12',
							]
						])
					)
					.
					$this->Html->div('row',
						$this->Form->control('meta_keywords',[
							'lang'=>true,
							'label'=>false,
							'templates' => ['inputContainer' => '{{content}}'],
							'templateVars' => [
								'label'=>__da('Meta keywords'),
								'class' => 'col-sm-12',
							]
						])
					)?>
			</div>
		</div>
	</div>
</div>