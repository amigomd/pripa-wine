<div class="chose-action clearfix">
    <?php
    if ($superUser || ! empty($permissions) ) {
        $footerActions = [];
        if ( isset($permissions[MODEL_NAME]['delete']) || $superUser ) {
            $footerActions[1] = __da('Delete');
        }
        if ( isset($permissions[MODEL_NAME]['publish']) || $superUser ) {
            $footerActions[2] = __da('Publish');
        }

        if (! empty($footerActions) ) {
    ?>
    <div class="styled-select action icon-caret-down">
        <select class="dropdown footer-action">
            <option value="-1"><?php echo __da('Acțiuni') ?></option>
            <?php   foreach($footerActions as $key => $label) { ?>
                <option value="<?php echo $key ?>"><?php echo $label ?></option>
            <?php } ?>
        </select>
    </div>
    <button class="btn btn-small btn-default footer-action-button"><?php echo __da('Apply') ?></button>
    <?php
        }
    }
    ?>

    <?php echo $this->element('index/paginator') ?>

</div>

<script>
    $(function(){
        $('.content-block tr th .checkbox').click(function(){
            $('.content-block tr td .checkbox').prop("checked", $(this).is(":checked"));
        });
        $('.footer-action-button').click(function(){
            if($('.footer-action').val() == '-1'){
                alert('<?php echo __da('Choose action');?>');
            }else{
                if($('.content-block tr td .checkbox:checked').length == 0){
                    alert('<?php echo __da('Select one of the items');?>');
                }else{
                    if(confirm('<?php echo __da('Are you sure?');?>')){
                        $.ajax({
                            type: "GET",
                            dataType: "html",
                            url: window.location.pathname+'/footerAction?action='+$('.footer-action').val()+'&'+$('.content-block tr td .checkbox:checked').serialize(),
                        }).done(function(data) {
                            location.reload();
                        });
                    }
                }
            }
        });
    });
</script>
