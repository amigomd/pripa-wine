<?php
$paginator = $this->Paginator;

if (isset($paginator)) {
    $sortKey = $paginator->sortKey();
    $sortDir = $paginator->sortDir();
}
?>
<!-- Выводим названия колонок -->
<thead>
<tr>
    <?php
    foreach ($fields as $field) {
        if ( ! isset($field['model'])){
            $field['model'] = $model_name;
        }

        if ( ! isset($field['class'])){
            $field['class'] = '';
        }
        ?>

        <?php if ( isset($field['checkboxes']) && ! empty($field['checkboxes']) ) { ?>
            <th class="table-check">
                <?php
                    echo $this->Form->checkbox(false, array(
                        'hiddenField' => false,
                        'class' => 'check-all'
                    ));
                ?>
            </th>
        <?php } ?>

        <?php if ( isset($field['view_name']) ) { ?>
            <th<?php echo isset($field['class']) && ! empty($field['class']) ? ' class="' . $field['class'] . '" ' : ''; ?>>
                <?php
				if(isset($field['select'])){?>
                    <select
                        name="<?php echo $field['select']['name'] ?>"
                        class="<?php echo $field['ajax'] ? 'select2-search-ajax select2-ajax-list' : 'menu-list-search';?> list-search"
                        data-model="<?php echo $field['model'];?>"
                        data-name="<?php echo isset($field['association']['name']) ? $field['association']['name'] : '';?>">
                        <?php
                        if($field['select']['data']){
                            foreach ($field['select']['data'] as $fieldKey => $fieldValue){?>
                                <option value="<?php echo $fieldKey; ?>"
                                    <?php echo isset($this->request->query[$field['select']['name']]) && $this->request->query[$field['select']['name']] == $fieldKey ? 'selected' : ''; ?>>
                                    <?php echo $fieldValue;?>
                                </option>
                            <?php } ?>
                        <?php }else{?>
                            <option value="">----</option>
                        <?php } ?>
                    </select>
				<?php }else {
					if (!empty($sortKey) && isset($field['name']) && $sortKey == $field['model'] . '.' . $field['name']) {
						// если к полю применяется сортировка
						$classSort = 'sorting ' . $sortDir;
					} else {
						$classSort = 'sorting';
					}

					if (!isset($field['name']) || (isset($field['sort']) && $field['sort'] == false)) { // если к полю не применяется сортировка
						echo $field['view_name']; // выводим только название поля
					} else {
//                        $sortField = isset($field['association']) ? $field['association']['model_name'].'.'.
						echo $paginator->sort(
							$field['model'] . '.' . $field['name'],
							'<span class="' . $classSort . '" >' . __da($field['view_name']) . '</span>',
							['class' => $classSort, 'escape' => false]
						); // линк для сортировки таблицы по этому полю
					}
				}
                ?>
            </th>
        <?php } ?>

        <?php if ( isset($field['actions']) && ! empty($field['actions']) ) { ?>
            <th class="table-actions"></th>
        <?php } ?>

    <?php } ?>
</tr>
</thead>
<!-- конец названий колонок -->