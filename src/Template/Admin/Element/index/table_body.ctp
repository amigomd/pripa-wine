<tbody <?php echo $items && isset($items->first()->{dev_conf('Config.sortableField')}) && is_int($items->first()->{dev_conf('Config.sortableField')}) ? 'id="admin-list"' : '' ?> class="admin-list-page">
    <?php foreach ($items as $item){ ?>
        <tr <?php if(isset($item['published']) && $item['published'] == false) echo 'class="unpublished"'?> id="positions_<?= $item->id ?>">
            <?php
                foreach ($fields as $field) {
                $tdClass = isset($field['class']) && ! empty($field['class']) ? 'class="' . $field['class'] . '"' : '';$item
            ?>
    
                <?php if ( isset($field['checkboxes']) && ! empty($field['checkboxes']) ) { ?>
                    <td class="table-check">
                        <?php
                        echo $this->Form->checkbox(false, [
                            'checked' => false,
                            'name' => 'checked[]',
                            'value' => $item->id,
                            'hiddenField' => false,
                        ]);
                        ?>
                    </td>
                <?php } ?>
    
                <?php if ( isset($field['name']) && ! empty($field['name']) ) { ?>
    
                    <td <?php echo $tdClass ?>>
                    <?php
					    $controller = isset($field['association']['controller_name']) ? $field['association']['controller_name'] : CONTROLLER_NAME;
					    $id = $item->id;

                        // в случае если надо вывести данные с другой таблицы
                        if(isset($field['association']) && $item->{$field['association']['target']}) {
                            $id = $item->{$field['association']['target']}->id;
                            $field_text = '';
                             if(isset($field['association']['name'])){
                                 foreach (explode(',',$field['association']['name']) as $field_name){
                                     $field_text .= $item->{$field['association']['target']}->{$field_name}.' ';
                                 }
                             }else{
                                 $field_text = $item->{$field['association']['target']}->{$field['name']};
                             }
						}elseif(isset($field['options']) && isset($field['options'][$item->{$field['name']}])){
                            $field_text = $field['options'][$item->{$field['name']}];
                        }else{
                            if(isset($field['lang'])){
                                $field_text = $item->translation($field['lang'])->{$field['name']};
                            }else{
                                $field_text = $item->{$field['name']};
                            }
                        }
                        // link field
                        if ( isset($field['is_link']) && ! empty($field['is_link']) ) {
                            if(CONTROLLER_NAME == 'questions'){
                                echo '<a href="/admin/'.$controller.'/edit/'.$id.'">'.htmlspecialchars_decode($field_text, ENT_QUOTES).'</a>';
                            }else{
                                echo $this->Html->link($field_text, [
                                        'controller' => $controller,
                                        'action' => 'edit',
                                        $id,
                                    ]
                                );
                            }
                        }
    
                        // date field
                        elseif (in_array($field['name'], dev_conf('Config.dateFields'))){
                            if ($field_text) {
								echo $this->Amigo->toNormalDate(date('Y-m-d', strtotime($field_text)),'rom');
//                                echo date(dev_conf('Config.dateFormat'), strtotime($field_text));
                            }
                        }
    
                        // string field
                        else {
                            echo $field_text;
                        }
                    ?>
                    </td>
                <?php } ?>
                    <?php
                    // actions
    
                    if ( isset($field['actions']) && ! empty($field['actions']) ) {
                        $actions = array_combine($field['actions'], $field['actions']);
    
    //                    if ( isset($item->module) && ! empty($item->module) ) {
    //                        unset($actions['delete']);
    //                    }
    
                        echo '<td class="table-actions">' . $this->element('index/actions', [
                            'item' => $item,
                            'actions' => $actions,
                        ] ) .'</td>';
                    }
                    ?>
            <?php } ?>
        </tr>
    <?php } ?>

</tbody>