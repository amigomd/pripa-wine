<?php

    if(!isset($item->id)){
        $id = $item;
        $item = (object)null;
        $item->id = $id;
    }
    // edit
    if ( isset($actions['edit']) ) {
        if ( isset($permissions[MODEL_NAME]['edit']) || $superUser ) {
            echo $this->Html->link(
                '<i class="icon-pencil"></i>',
                ['action' => 'edit', $item->id],
                ['title' => __da('Edit'), 'escape' => false]
            );
        }
    }

    //clone
    if ( isset($actions['clone']) ) {
        if ( isset($permissions[MODEL_NAME]['add']) || $superUser ) {
            echo $this->Html->link(
                '<i class="icon-copy"></i>',
                ['action' => 'copy', $item->id],
                ['title' => __da('Clone'), 'escape' => false]
            );
        }
    }

    // publish
    if ( isset($actions['publish']) ) {
        if ( isset($permissions[MODEL_NAME]['publish']) || $superUser ) {
            echo $this->Html->link(
                '<i class="icon-eye"></i>',
                ['action' => 'publish', $item->id],
                ['title' => __da('Publish'), 'escape' => false]
            );
        }
    }

    // delete

    if ( isset($actions['delete']) ) {
        if ( (MODEL_NAME == 'Menus' && CONTROLLER_NAME == 'menus') ) {
            echo '<a href="/admin/menus/delete/'.$item->id.'"><i class="icon-close"></i></a>';
        }else{
            echo $this->Html->link(
                '<i class="icon-close"></i>',
                ['action' => 'delete', $item->id],
                [
                    'title'   => __da('Delete'),
                    'escape'  => false,
                    'confirm' => __da('Are you sure you want to delete # {0}?', $item->id)
                ]
            );
        }
    }
?>


<?php
/*
    <a href="#" title="edit"><i class="icon-pencil"></i></a>
    <a href="#" title="publish"><i class="icon-eye"></i></a><!-- <i class="icon-leanpub"></i> -->
    <a href="#" title="view"><i class="icon-crosshairs"></i></a>
    <a href="#" title="link"><i class="icon-chain"></i></a><!-- <i class="icon-chain"></i> -->
    <a href="#" title="delete"><i class="icon-close"></i></a>
*/
?>