<?php
$isset_publish = $page->isNew() || is_bool($page->published) || is_int($page->published) ? true : false;
//$isset_publish = $gallery->isNew() || $page->isNew() || is_bool($gallery->published) || is_bool($page->published) || is_int($gallery->published) || is_bool($page->published) ? true : false;
echo $this->Html->div('row',
		$this->Form->control('published',[
			'type' => 'checkbox',
			'templateVars' => [
				'label'=>__da('Published'),
				'class' => 'col-sm-6 checkbox-block '.(!$isset_publish ? 'hidden' : ''),
			]
		])
		.
		$this->Html->div('col-block col-block col-save-buttons '.($isset_publish? 'col-sm-6' : 'col-sm-12'),
			$this->Form->control(__da('Save'),[
				'type'=>'submit',
				'ajax'=>true,
				'templates' => ['submitContainer' => '{{content}}'],
				'templateVars' => [
					'class' => 'btn btn-green btn-large pull-right save-redirect',
				]
			])
			.
			$this->Form->control(__da('Save and continue'),[
				'type'=>'submit',
				'ajax'=>true,
				'templates' => ['submitContainer' => '{{content}}'],
				'templateVars' => [
					'class' => 'btn btn-grey btn-large pull-right save-continue',
				]
			])
		)
	);
?>