<head>
    <meta charset="utf-8" />
    <base href="<?php echo (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://':'http://').$_SERVER['HTTP_HOST'].'/' ?>">
    <title>Amigo <?= h($this->fetch('title')) ?></title>
    <meta name="description" content="Amigo CMS 3" />

    <!-- Mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=0, width=1024">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="theme-color" content="#4aba47">
    <?= $this->element('favicons') ?>
    <script>
        var Controller_Name = '<?php echo $this->request->getParam('controller'); ?>';
        var Action_Name = '<?php echo ACTION_NAME; ?>';
    </script>
    <?php
        echo $this->Html->css('../admin-assets/css/final.css?v=' . time() );

        echo $this->Html->script('../common/js/jquery-1.12.0.min.js');
        if(ACTION_NAME == 'add' || ACTION_NAME == 'edit' || $this->request->getParam('controller') == 'Comparisons' || $this->request->getParam('controller') == 'Comparisons') {
			echo $this->Html->script('../admin-assets/plugin/tinymce/js/tinymce/tinymce.min.js');
		}
	    echo $this->Html->script('../admin-assets/js/final.js?v=' . time());
	?>
    <!--[if lte IE 8]>
    <script src="/common/js/html5shiv.js" type="text/javascript"></script>
    <![endif]-->

</head>