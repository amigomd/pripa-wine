<?php if(conf('Maintenance.status') == 1){?>
	<header class="header">
	    <div class="message message-attention">
	        <span><i class="icon-exclamation-triangle"></i> <?php echo conf('Maintenance.message-rom') ?></span>
	    </div>
	</header>
<?php } ?>