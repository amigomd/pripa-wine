<?php
	echo $this->element('header', ['itemPage' => true,'langSwitcher'=>true]);
	$isset_publish = $page->isNew() || is_bool($page->published) || is_int($page->published) ? true : false;
	echo $this->Html->div('content-block',
		$this->Form->create($page, ['class' => "form-page"])
		.
		$this->Form->control('id',['type' => 'hidden'])

        .
        $this->Html->div('row',
            $this->Form->control('city',[
                'lang'=>true,
                'label'=>false,
                'templates' => ['inputContainer' => '{{content}}'],
                'templateVars' => [
                    'label'=>__da('City'),
                    'class' => 'col-sm-6',
                    'inputclass' => 'title-input'
                ]
            ])
        )

		.
		$this->Html->div('row',
			$this->Form->control('adress',[
				'lang'=>true,
				'label'=>false,
				'templates' => ['inputContainer' => '{{content}}'],
				'templateVars' => [
					'label'=>__da('Street'),
					'class' => 'col-sm-6',
                    'inputclass' => 'title-input'
				]
			])
		)
        .
        $this->Html->div('row',
            $this->Form->control('phone',[
                'lang'=>true,
                'label'=>false,
                'templates' => ['inputContainer' => '{{content}}'],
                'templateVars' => [
                    'label'=>__da('Phone'),
                    'class' => 'col-sm-6',
                    'inputclass' => 'title-input'
                ]
            ])
        )
		.
		$this->Html->div('row',
			$this->Form->control('link',[
				'lang'=>true,
				'label'=>false,
				'templates' => ['inputContainer' => '{{content}}'],
				'templateVars' => [
					'label'=>__da('Website'),
					'class' => 'col-sm-6',
					 'inputclass' => 'title-input'
//                    'class' => 'col-sm-12 tinymce',
				]
			])
		)
        .
        $this->Html->div('row',
            $this->Form->control('image', [
                'type' => 'hidden',
                'templateVars' => [
                    'label' => __da('Image'),
                    'class' => 'col-sm-3'
                ]
            ])
        )
        .
        $this->Html->div('row',
            $this->Form->control('published',[
                'type' => 'checkbox',
                'templateVars' => [
                    'label'=>__da('Published'),
                    'class' => 'col-sm-2 checkbox-block '.(!$isset_publish ? 'hidden' : ''),
                ]
            ])
            .
            $this->Html->div('col-block col-block col-save-buttons '.($isset_publish? 'col-sm-4' : 'col-sm-12'),
                $this->Form->control(__da('Save'),[
                    'type'=>'submit',
                    'ajax'=>true,
                    'templates' => ['submitContainer' => '{{content}}'],
                    'templateVars' => [
                        'class' => 'btn btn-green btn-large pull-right save-redirect',
                    ]
                ])
                .
                $this->Form->control(__da('Save and continue'),[
                    'type'=>'submit',
                    'ajax'=>true,
                    'templates' => ['submitContainer' => '{{content}}'],
                    'templateVars' => [
                        'class' => 'btn btn-grey btn-large pull-right save-continue',
                    ]
                ])
            )
        )
		.
		$this->Form->end()
	);
?>
