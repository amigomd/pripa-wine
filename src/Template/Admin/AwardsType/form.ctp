<?php
echo $this->element('header', ['itemPage' => true,'langSwitcher'=>true]);

$isset_publish = $page->isNew() || is_bool($page->published) || is_int($page->published) ? true : false;

echo $this->Html->div('content-block',
    $this->Form->create($page, ['class' => "form-page"])
    .
    $this->Form->control('id',['type' => 'hidden'])
    .
    $this->Html->div('row',
        $this->Form->control('title',[
            'lang'=>true,
            'label'=>false,
            'templates' => ['inputContainer' => '{{content}}'],
            'templateVars' => [
                'label'=>__da('Title'),
                'class' => 'col-sm-12',
                'inputclass' => 'title-input'
            ]
        ])
    )
    .
    $this->Html->div('row',
        $this->Form->control('description', [
            'lang' => true,
            'label' => false,
            'type' => 'textarea',
            'templates' => ['inputContainer' => '{{content}}'],
            'templateVars' => [
                'label' => __da('Description'),
                'class' => 'col-sm-12',
                'inputclass' => 'title-input'
            ]
        ])
    )
    .
    $this->Html->div('row',
        $this->Form->control('image', [
            'type' => 'hidden',
            'templateVars' => [
                'label' => __da('Image'),
                'class' => 'col-sm-3'
            ]
        ])
    )
    .

    $this->element('index/save_actions')
    .
    $this->Form->end()
);
?>

