<?php
echo $this->element('header', ['itemPage' => true,'langSwitcher'=>true]);

$isset_publish = $page->isNew() || is_bool($page->published) || is_int($page->published) ? true : false;

echo $this->Html->div('content-block',
    $this->Form->create($page, ['class' => "form-page"])
    .
    $this->Form->control('id',['type' => 'hidden'])
    .
    $this->Html->div('row',
             $this->Form->control('year',[
                 'lang'=>false,
                 'label'=>'Year',
                // 'templates' => ['inputContainer' => '{{content}}'],
                 'templateVars' => [
                     'label'=>__da('Year'),
                     'class' => 'col-sm-6',
                     'inputclass' => 'title-textarea'
                 ]
             ])
         )
    .

        $this->Html->div('row',
            $this->Form->control('awards_id', [
                'options' => dev_conf('Config.awardType'),
                'label' => __da('Type'),
                'class' => 'select2-search select-category',
                'templateVars' => [
                    'template'=>'select2',
                    'class' => 'col-sm-6',
                    'divclass' => 'styled-select-select2'
                ]
            ])
        )
    .
         $this->Html->div('row',
             $this->Form->control('description', [
                 'lang' => true,
                 'label' => false,
                 'type' => 'textarea',
                 'templates' => ['inputContainer' => '{{content}}'],
                 'templateVars' => [
                     'label' => __da('Description'),
                     'class' => 'col-sm-6'
                 ]
             ])
         )

    .

    $this->Html->div('row',
        $this->Form->control('image', [
            'type' => 'hidden',
            'templateVars' => [
                'label' => __da('Image'),
                'class' => 'col-sm-3'
            ]
        ])
    )

    .
    $this->Html->div('row',
        $this->Form->control('published',[
            'type' => 'checkbox',
            'templateVars' => [
                'label'=>__da('Published'),
                'class' => 'col-sm-1 checkbox-block '.(!$isset_publish ? 'hidden' : ''),
            ]
        ])
        .
        $this->Form->control('multiply', [
            'type' => 'checkbox',
            'templateVars' => [
                'label' => __da('multiply awards'),
                'class' => 'col-sm-1 checkbox-block'
            ]
        ])
        .

        $this->Html->div('col-block col-block col-save-buttons '.($isset_publish? 'col-sm-4' : 'col-sm-12'),
            $this->Form->control(__da('Save'),[
                'type'=>'submit',
                'ajax'=>true,
                'templates' => ['submitContainer' => '{{content}}'],
                'templateVars' => [
                    'class' => 'btn btn-green btn-large pull-right save-redirect',
                ]
            ])
            .
            $this->Form->control(__da('Save and continue'),[
                'type'=>'submit',
                'ajax'=>true,
                'templates' => ['submitContainer' => '{{content}}'],
                'templateVars' => [
                    'class' => 'btn btn-grey btn-large pull-right save-continue',
                ]
            ])
        )
    )

    .

    $this->Form->end()
);
?>