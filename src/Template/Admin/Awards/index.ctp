<?php

echo $this->element('header');

echo $this->element('index/table',[
    'model_name' => MODEL_NAME,
    'items' => ${Cake\Utility\Inflector::variable(CONTROLLER_NAME)},
    'fields' => [
        [
            'checkboxes' => true,
        ],
        /*[
            'name' => 'id',
            'view_name' => '#',
            'class' => 'col-sm-1 table-position',
        ],*/
        [
            'name' => 'description',
            'view_name' => __ds('Description'),
            'is_link' => true,
            'i18n' => true
        ],
        [
            'actions' => ['edit', 'delete', 'publish'],
        ],
    ],
]);
?>


