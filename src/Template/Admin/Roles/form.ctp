<?php

echo $this->element('header', ['itemPage' => true]);

function custom_search ($permissions, $moduleActionId)
{
    foreach ($permissions as $key => $permission) {
        if ($permission['action_module_id'] == $moduleActionId) {
            return $permission;
        }
    }

    return array();
}

$moduleTrs = [];
$moduleTds = [];
foreach ($modules as $module) {
    $moduleTds = [ 0 => $module->name ];
    // disponible actions
    foreach ($actions as $actionId => $action) {
        $checkBox = '';
        $key = $module->id . $actionId;

        // disponible actions per module
        foreach ($module['actions'] as $moduleAction) {
            // if module has action
            if( $moduleAction->id == $actionId ){

                $checkBox .= $this->Form->input('permissions[' . $key . '][action_module_id]', [
                    'type' => 'hidden',
                    'value' => $moduleAction->_joinData['id'],
                ]);

                // check if permission exist in db and show data
                $allowed = false;
                if ( $role['permissions'] ) {
                    $permission = custom_search($role['permissions'],$moduleAction->_joinData['id']);
                    if ($permission) {
                        $checkBox .= $this->Form->input('permissions[' . $key . '][id]', [
                            'type' => 'hidden',
                            'value' => $permission->id,
                        ]);
                        $allowed = $permission['allow'];
                    }
                }

                $checkBox .= $this->Form->checkbox('permissions[' . $key . '][allow]',[
                    'label' => false,
                    'checked' => (bool) $allowed,
                ]);

            }
        }
        $moduleTds[] = $checkBox;
    }
    $moduleTrs[] = $moduleTds;
}

echo $this->Html->div('content-block',
    $this->Form->create($role, array('class' => "form-page"))
    .
    $this->Html->div('row',
        $this->Form->control('name',[
            'templateVars' => [
                'class' => 'col-sm-3'
            ]
        ])
    )
    .
    $this->Html->tag('div',
        $this->Html->tag('div',
            $this->Html->tag('table',
                // begin content table
                $this->Html->tag('thead',
                    $this->Html->tableHeaders(
                        array_merge([ ' ' ], $actions)
                    )
                )
                .
                $this->Html->tableCells($moduleTrs),
                // end content table
                ['class' => 'data-table table-align-center'] // class for table
            ),
        ['class' => 'col-sm-6'] // class for second div
        ),
    ['class' => 'row'] // class for first div
    )
    .
    $this->Html->div('row',
        $this->Form->control(__d('admin','Save'),[
            'type'=>'submit',
            'templateVars' => [
                'class' => 'btn btn-green btn-large pull-right',
                'divclass'=>'col-block col-sm-12 col-save-buttons'
            ]
        ])
    )
    .
    $this->Form->end()
);


?>
