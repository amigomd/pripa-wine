
<?php
	if(!isset($header)) {
		$header = true;
	}
?>
<?php /* ?>
<header class="header hidden">
	<div class="message message-success animated slideInDown">
		<i class="icon-exclamation-triangle"></i><span></span>
	</div>
</header>
<header class="header hidden">
	<div class="message message-error animated slideInDown">
		<i class="icon-exclamation-triangle"></i><span></span>
	</div>
</header>
<?php */?>
<header class="content-header clearfix">
	<!-- page title is generated from js  -->
	<h2 class="page-title"></h2>

	<?php
	if (empty($itemPage)) {

	} elseif ( ! empty($langSwitcher)) {
		echo $this->element('fields/lang_switcher');
	}
	?>

</header>

<?php
if (!empty($itemPage)) {

} else {
	if($header) {
		echo $this->element('../Admin/Element/header_actions');
	}
}
?>

<?php

echo $this->element('index/table',[
    'model_name' => MODEL_NAME,
    'items' => ${Cake\Utility\Inflector::variable(CONTROLLER_NAME)},
    'fields' => [
        [
            'checkboxes' => true,
        ],
       /* [
            'name' => 'id',
            'view_name' => '#',
            'class' => 'col-sm-1 table-position',
        ],*/
        [
            'name' => 'title',
            'view_name' => __ds('Title'),
            'is_link' => true,
            'i18n' => true
        ],


        [
            'actions' => ['edit'],
        ],
    ],
]);
?>


