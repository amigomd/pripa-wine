<?php
echo $this->element('header', ['itemPage' => true,'langSwitcher'=>true]);

$isset_publish = $page->isNew() || is_bool($page->published) || is_int($page->published) ? true : false;

echo $this->Html->div('content-block',
    $this->Form->create($page, ['class' => "form-page"])
    .
    $this->Form->control('id',['type' => 'hidden'])
    .
    $this->Html->div('row',
        $this->Form->control('title', [
            'lang' => true,
            'label' => false,
            'type' => 'textarea',
            'templates' => ['inputContainer' => '{{content}}'],
            'templateVars' => [
                'label' => __da('Description'),
                'class' => 'col-sm-12  title-general',
                'inputclass' => 'title-input'
            ]
        ])
    )
    .
    $this->Html->div('row',
        $this->Form->control('image', [
            'type' => 'hidden',
            'templateVars' => [
                'label' => __da('Image'),
                'class' => 'col-sm-3'
            ]
        ])
    )
    .

    $this->element('index/save_actions')
    .
    $this->Form->end()
);
?>
<script src="//cdn.jsdelivr.net/npm/medium-editor@latest/dist/js/medium-editor.min.js"></script>
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/medium-editor@latest/dist/css/medium-editor.min.css" type="text/css" media="screen" charset="utf-8">
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/medium-editor@5.23.3/dist/css/themes/bootstrap.css" type="text/css" media="screen" charset="utf-8">
<style>
    .medium-editor-element {
        padding-left: 20px;
        width:100%;
        min-height: 100px;
        color: #333;
        padding: 5px 30px 5px 10px;
        border: 1px solid #ccc;
        border-radius: 0;
        resize: none;
        background-color: white;
        overflow-y: overlay;
    }
</style>
<script>
    $(function () {
        $('select[name=type]').change(function (e) {
            $('.cross').addClass('hidden');
            if($(this).val() == 'cross'){
                $('.cross').removeClass('hidden');
            }
        });

        var editor1 = new MediumEditor('.title-general textarea',{
            paste: {
                forcePlainText: true,
                cleanPastedHTML: false
            },
            enter: '<br>',
            placeholder: {
                text: 'Click to edit'
            },
            toolbar: {
                /* These are the default options for the toolbar,
                   if nothing is passed this is what is used */
                allowMultiParagraphSelection: false,
                buttons: ['bold'],
                diffLeft: 0,
                diffTop: -10,
                firstButtonClass: 'medium-editor-button-first',
                lastButtonClass: 'medium-editor-button-last',
                relativeContainer: null,
                standardizeSelectionStart: false,
                static: false,

                /* options which only apply when static is true */
                align: 'center',
                sticky: false,
                updateOnEmptySelection: false
            }
        });
        // editor1.subscribe("editableKeydownEnter", function (event, element) {
        //   if (event.key == 'Enter') {
        //     event.preventDefault()
        //     var node = MediumEditor.selection.getSelectionStart(this.options.ownerDocument)
        //     MediumEditor.util.insertHTMLCommand(this.options.ownerDocument, "<br>")
        //   }
        // }.bind(editor1))
    });
</script>


