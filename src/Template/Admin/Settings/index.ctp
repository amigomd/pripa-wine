<header class="content-header clearfix">
    <h2 class="page-title"></h2>

    <?php echo $this->element('fields/lang_switcher'); ?>

</header>

<div class="content-block">
    <?php
    echo $this->Form->create(null,['class'=>'settings-page','enctype'=>'multipart/form-data']); ?>
    <div class="row row-bkg">

        <div class="tabs tabs-content">
            <ul role="tablist" class="nav nav-tabs">
                <li class="active" role="presentation">
                    <a aria-controls="dropdown1" data-toggle="tab" role="tab" href="#dropdown1"><?php echo __da('Setările site-ului') ?></a>
                </li>

                <li role="presentation">
                    <a aria-controls="dropdown2" data-toggle="tab" role="tab" href="#dropdown2"><?php echo __da('Multiligual') ?></a>
                </li>



                <li role="presentation">
                    <a aria-controls="dropdown4" data-toggle="tab" role="tab" href="#dropdown4" onclick="$('#load_google_map').val() == '0' ? setTimeout(function(){initMap()},500) : ''">
                        <?php echo __da('Contacte') ?>
                    </a>
                </li>
                <li role="presentation">
                    <a aria-controls="dropdown5" data-toggle="tab" role="tab" href="#dropdown5"><?php echo __da('Google and Hotjar') ?></a>
                </li>


            </ul>

            <div class="tab-content">
                <div id="dropdown1" class="tab-pane fade in active" role="tabpanel">

                    <div class="row row-padding">
                        <div class="col-block col-sm-3">
                            <h3><?php echo __da('Site name') ?></h3>
                        </div>
                        <div>
                            <?php echo $this->Form->control('Config.projectName-{LANG}',[
                                'value'=>$setting['Config'],
                                'lang'=>true,
                                'label'=>false,
                                'prefix'=>'projectName-',
                                'templates' => ['inputContainer' => '{{content}}'],
                                'templateVars' => [
                                    'label'=>__da('Project Name'),
                                    'class' => 'col-sm-12',
                                ]
                            ]);?>
                        </div>
                        <div>
                            <?php echo $this->Form->control('Config.slogan-{LANG}',[
                                'value'=>$setting['Config'],
                                'lang'=>true,
                                'label'=>false,
                                'prefix'=>'slogan-',
                                'templates' => ['inputContainer' => '{{content}}'],
                                'templateVars' => [
                                    'label'=>__da('Slogan'),
                                    'class' => 'col-sm-12',
                                ]
                            ]);?>
                        </div>
                        <div class="col-block col-sm-4">
                            <h3><?php echo __da('Favicon') ?></h3>

                        <div class="col-block col-sm-4">
                            <div class="images-gallery clearfix">
                                <div class="gallery-item">
                                    <div class="<?php echo !is_file(WWW_ROOT.'img/favicons/favicon.ico') ? 'hidden' : ''; ?>">
                                        <div class="image"><img id="favicon-img" src="/img/favicons/ms-icon-150x150.png?v=<?php echo rand(0,1000); ?>" alt=""></div>
                                        <button class="btnDeleteItem"><i class="icon-trash-o"></i></button>
                                    </div>

                                    <input type="file" id="favicon" name="favicon" style="display: none;">
                                    <button class="btnAddItem <?php echo is_file(WWW_ROOT.'img/favicons/favicon.ico') ? 'hidden' : ''; ?>"></button>
                                </div>
                            </div>
                        </div>
                        </div>
                        <script>
                            $('.btnAddItem').click(function (e) {
                                e.preventDefault();
                                $(this).prev().click();
                            });
                            $('.btnDeleteItem').click(function (e) {
                                e.preventDefault();
                                if (!confirm('Are you sure?')) {
                                    return false;
                                }
                                item = this;
                                $(item).parent().addClass('hidden').parent().find('.btnAddItem').removeClass('hidden');
                                $.ajax('/admin/settings/deleteFavicon', {
                                    method: "POST",
                                    dataType: "json",
                                    success: function (response) {
                                    },
                                    error: function () {
                                    }
                                });

                            });

                            function readFile() {

                                if (this.files && this.files[0]) {

                                    var FR= new FileReader();

                                    FR.addEventListener("load", function(e) {
                                        $("#favicon-img").parent().parent().removeClass('hidden').next().next().addClass('hidden');
                                        document.getElementById("favicon-img").src = e.target.result;
                                    });

                                    FR.readAsDataURL( this.files[0] );
                                }

                            }

                            document.getElementById("favicon").addEventListener("change", readFile);

                        </script>

                        <div class="col-block col-sm-4">
                            <h3><?php echo __da('Main Page') ?></h3>

                        <div class="col-block col-sm-4">
                            <div class="images-gallery clearfix">

                                <div class="images-gallery clearfix">
                                    <div class="gallery-item logo2">
                                        <div class="<?php echo !is_file(WWW_ROOT.'img/logos2/'.conf('logo2.name')) ? 'hidden' : ''; ?>">
                                            <div class="image"><img id="logo2-img" src="/img/logos2/<?= conf('logo2.name') ?>" alt=""></div>
                                            <button class="btnDeleteItem"><i class="icon-trash-o"></i></button>
                                        </div>
                                        <input type="file" id="logo2" name="logo2" style="display: none;">
                                        <button class="btnAddItem <?php echo is_file(WWW_ROOT.'img/logos2/'.conf('logo2.name')) ? 'hidden' : ''; ?>"></button>
                                    </div>
                                </div>
                            </div>

                        </div>

                            <script>
                                $('.logo2 .btnAddItem').click(function (e) {
                                    e.preventDefault();
                                    $(this).prev().click();
                                });
                                $('.logo2 .btnDeleteItem').click(function (e) {
                                    e.preventDefault();
                                    if (!confirm('Are you sure?')) {
                                        return false;
                                    }
                                    item = this;
                                    $(item).parent().addClass('hidden').parent().find('.btnAddItem').removeClass('hidden');
                                    $.ajax('/admin/settings/deleteL', {
                                        method: "POST",
                                        dataType: "json",
                                        success: function (response) {
                                        },
                                        error: function () {
                                        }
                                    });

                                });

                                function readFile() {

                                    if (this.files && this.files[0]) {

                                        var FR= new FileReader();

                                        FR.addEventListener("load", function(e) {
                                            $("#logo2-img").parent().parent().removeClass('hidden').next().next().addClass('hidden');
                                            document.getElementById("logo2-img").src = e.target.result;
                                        });

                                        FR.readAsDataURL( this.files[0] );
                                    }

                                }

                                document.getElementById("logo2").addEventListener("change", readFile);

                            </script>
                        </div>

                        <div class="col-block col-sm-4">
                            <h3><?php echo __da('Domain Page') ?></h3>

                        <div class="col-block col-sm-4">
                            <div class="images-gallery clearfix">
                                <div class="gallery-item popup">
                                    <div class="<?php echo !is_file(WWW_ROOT.'img/popup/'.conf('popup.name')) ? 'hidden' : ''; ?>">
                                        <div class="image"><img id="popup-img" src="/img/popup/<?= conf('popup.name') ?>" alt=""></div>
                                        <button class="btnDeleteItem"><i class="icon-trash-o"></i></button>
                                    </div>
                                    <input type="file" id="popup" name="popup" style="display: none;">
                                    <button class="btnAddItem <?php echo is_file(WWW_ROOT.'img/popup/'.conf('popup.name')) ? 'hidden' : ''; ?>"></button>
                                </div>
                            </div>
                        </div>
                            <script>
                                $('.popup .btnAddItem').click(function (e) {
                                    e.preventDefault();
                                    $(this).prev().click();
                                });
                                $('.popup .btnDeleteItem').click(function (e) {
                                    e.preventDefault();
                                    if (!confirm('Are you sure?')) {
                                        return false;
                                    }
                                    item = this;
                                    $(item).parent().addClass('hidden').parent().find('.btnAddItem').removeClass('hidden');
                                    $.ajax('/admin/settings/deletePopup', {
                                        method: "POST",
                                        dataType: "json",
                                        success: function (response) {
                                        },
                                        error: function () {
                                        }
                                    });

                                });

                                function readFile() {

                                    if (this.files && this.files[0]) {

                                        var FR= new FileReader();

                                        FR.addEventListener("load", function(e) {
                                            $("#popup-img").parent().parent().removeClass('hidden').next().next().addClass('hidden');
                                            document.getElementById("popup-img").src = e.target.result;
                                        });

                                        FR.readAsDataURL( this.files[0] );
                                    }

                                }

                                document.getElementById("popup").addEventListener("change", readFile);

                            </script>
                        </div>

                    </div>





                    <div class="row row-padding">
                        <div class="col-block col-sm-12">
                            <h3><?php echo __da('Social Networks') ?></h3>
                        </div>
                        <div>

                            <?php echo $this->Form->control('Social.fb',['label' => 'Facebook','value'=>$setting['Social']['fb'],'templateVars' => ['class' => 'col-sm-6']]);?>
                            <?php echo $this->Form->control('Social.gps',['label' => 'GPS coordinates ','value'=>$setting['Social']['gps'],'templateVars' => ['class' => 'col-sm-6']]);?>

                        </div>
                    </div>

                    <?/*?>
                    <div class="row row-padding">
                        <div class="col-block col-sm-12">
                            <h3><?php echo __da('Notification') ?></h3>
                        </div>
                        <div>
                            <?php echo $this->Form->control('Notification.from_email',['value'=>$setting['Notification']['from_email'],'templateVars' => ['class' => 'col-sm-6']]);?>
                            <?php echo $this->Form->control('Notification.to_email',['value'=>$setting['Notification']['to_email'],'templateVars' => ['class' => 'col-sm-6']]);?>
                        </div>
                    </div>
 <?/*?>
                    <div class="row row-padding">
                        <div class="col-block col-sm-12">
                            <h3><?php echo __da('Home page') ?></h3>
                        </div>
                        <div>
                            <?php echo $this->Form->control('Home.title-{LANG}',[
                                'value'=>$setting['Home'],
                                'lang'=>true,
                                'label'=>false,
                                'prefix'=>'title-',
                                'templates' => ['inputContainer' => '{{content}}'],
                                'templateVars' => [
                                    'label'=>__da('Title'),
                                    'class' => 'col-sm-12',
                                ]
                            ]);?>
                        </div>
                        <div>
                            <?php echo $this->Form->control('Home.text-{LANG}',[
                                'value'=>$setting['Home'],
                                'lang'=>true,
                                'label'=>false,
                                'prefix'=>'text-',
                                'templates' => ['inputContainer' => '{{content}}'],
                                'templateVars' => [
                                    'label'=>__da('Short description'),
                                    'class' => 'col-sm-12',
                                ]
                            ]);?>
                        </div>
                        <div>
                            <?php echo $this->Form->control('Home.link-{LANG}',[
                                'value'=>$setting['Home'],
                                'lang'=>true,
                                'label'=>false,
                                'prefix'=>'link-',
                                'templates' => ['inputContainer' => '{{content}}'],
                                'templateVars' => [
                                    'label'=>__da('Link'),
                                    'class' => 'col-sm-12',
                                ]
                            ]);?>
                        </div>
                    </div>
                    <?*/?>

                    <div class="row row-padding">
                        <div class="col-block col-sm-12">
                            <h3><?php echo __da('Meta') ?></h3>
                        </div>

                        <div>
                            <?php echo $this->Form->control('Meta.title-{LANG}',[
                                'value'=>$setting['Meta'],
                                'lang'=>true,
                                'label'=>false,
                                'prefix'=>'title-',
                                'templates' => ['inputContainer' => '{{content}}'],
                                'templateVars' => [
                                    'label'=>__da('Meta title'),
                                    'class' => 'col-sm-12',
                                ]
                            ]);?>
                        </div>

                        <div>
                            <?php echo $this->Form->control('Meta.description-{LANG}',[
                                'value'=>$setting['Meta'],
                                'lang'=>true,
                                'label'=>false,
                                'prefix'=>'description-',
                                'templates' => ['inputContainer' => '{{content}}'],
                                'templateVars' => [
                                    'label'=>__da('Meta description'),
                                    'class' => 'col-sm-12',
                                ]
                            ]);?>
                        </div>
                    </div>

                    <div class="row row-padding">
                        <?php echo $this->Form->control('Clear cache',[
                            'label'=>false,
                            'type'=>'button',
                            'class'=>'btn btn-large btn-default',
                            'templateVars' => [
                                'divclass'=>'col-sm-12'
                            ]]);?>
                    </div>

                    <div class="row row-padding">
                        <div class="col-block col-title col-sm-12">
                            <h3><?php echo __da('Lucrari tehnice') ?></h3>
                            <?php echo $this->Form->control('Maintenance.status',[
                                'checked'=>$setting['Maintenance']['status'],
                                'type'=>'checkbox',
                                'templateVars' => [
                                    'class' => 'checkbox-block clearfix'
                                ]]);?>
                            <?php echo $this->Form->control('Maintenance.nolimit',[
                                'checked'=>$setting['Maintenance']['nolimit'],
                                'type'=>'checkbox',
                                'templateVars' => [
                                    'class' => 'checkbox-block clearfix'
                                ]]);?>
                        </div>
                        <?php echo $this->Form->control('Maintenance.ips',['value'=>$setting['Maintenance']['ips'],'templateVars' => ['class' => 'col-sm-6']]);?>
                        <div>
                            <?php echo $this->Form->control('Maintenance.message-{LANG}',[
                                'value'=>$setting['Maintenance'],
                                'lang'=>true,
                                'label'=>false,
                                'prefix'=>'message-',
                                'templates' => ['inputContainer' => '{{content}}'],
                                'templateVars' => [
                                    'label'=>__da('Message maintenance'),
                                    'class' => 'col-sm-6',
                                ]
                            ]);?>
                        </div>
                    </div>

                </div>
                <div id="dropdown2" class="tab-pane fade" role="tabpanel">

                    <div class="row row-padding">
                        <div class="col-block col-sm-12">
                            <h3><?php echo __da('Limbi') ?></h3>
                        </div>

                        <div class="col-block col-sm-3">
                            <label for="ArticleDescEng"><?php echo __da('Limbi de administrare') ?></label>

                            <div class="row">
                                <?php foreach (Cake\Core\Configure::read('Languages') as $language) {?>
                                    <?php echo $this->Form->control('Languages.'.$language->code,[
                                        'label'=>$language->title,
                                        'checked'=>$setting['Languages'][$language->code],
                                        'type'=>'checkbox',
                                        'templateVars' => [
                                            'class' => 'checkbox-block clearfix col-sm-4'
                                        ]]);?>
                                <?php } ?>
                            </div>

                        </div>
                        <div class="col-block col-sm-3">
                            <label for="ArticleDescEng"><?php echo __da('Limbi pe site') ?></label>

                            <div class="row">
                                <?php foreach (Cake\Core\Configure::read('Languages') as $language) {?>
                                    <?php echo $this->Form->control('LanguagesFront.'.$language->code,[
                                        'label'=>$language->title,
                                        'checked'=>$setting['LanguagesFront'][$language->code],
                                        'type'=>'checkbox',
                                        'templateVars' => [
                                            'class' => 'checkbox-block clearfix col-sm-4'
                                        ]]);?>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                        echo $this->Form->control('Language.admin',[
                            'value'=>intval($setting['Language']['admin']),
                            'options'=>$language_list,
                            'class'=>'form-control',
                            'templateVars' => [
                                'class' => 'col-block col-sm-3'
                            ]]);
                        echo $this->Form->control('Language.front',[
                            'value'=>$setting['Language']['front'],
                            'options'=>$language_list,
                            'class'=>'form-control',
                            'templateVars' => [
                                'class' => 'col-block col-sm-3'
                            ]]);
                        ?>
                    </div>


                </div>


                <div id="dropdown4" class="tab-pane fade" role="tabpanel">
                    <input type="hidden" id="load_google_map" value="0">
                    <div class="row row-padding">
                        <div class="col-block col-sm-12">
                            <h3><?php echo __da('Contacte') ?></h3>
                        </div>
                        <?php
                        echo $this->Form->control('Contact.address',[
                            'value'=>$setting['Contact']['address'],
                            'templateVars' => [
                                'class' => 'col-sm-3'
                            ]]);
                        echo $this->Form->control('Contact.phone_mobile',[
                            'value'=>$setting['Contact']['phone_mobile'],
                            'templateVars' => [
                                'class' => 'col-sm-3'
                            ]]);
                        echo $this->Form->control('Contact.schendle',[
                            'value'=>$setting['Contact']['schendle'],
                            'templateVars' => [
                                'class' => 'col-sm-3'
                            ]]);
                        echo $this->Form->control('Contact.email',[
                            'value'=>$setting['Contact']['email'],
                            'templateVars' => [
                                'class' => 'col-sm-3'
                            ]]);
                        ?>
                        <div class="col-block col-sm-12">
                            <h3><?php echo __da('Gaseste-ne cu aplicatia preferata') ?></h3>
                        </div>
                        <?php
                        echo $this->Form->control('App.googlemap',[
                            'value'=>$setting['App']['googlemap'],
                            'label' => 'Google map',
                            'templateVars' => [
                                'class' => 'col-sm-6'
                            ]]);
                        echo $this->Form->control('App.applemap',[
                            'value'=>$setting['App']['applemap'],
                            'label' => 'Apple Maps',
                            'templateVars' => [
                                'class' => 'col-sm-6'
                            ]]);

                        ?>
                        <div class="col-block col-sm-12">
                            <?php echo $this->Form->input('Map.zoom',['value'=>$setting['Map']['zoom'],'type'=>'hidden']); ?>
                            <?php echo $this->Form->input('Map.lat',['value'=>$setting['Map']['lat'],'type'=>'hidden']); ?>
                            <?php echo $this->Form->input('Map.lng',['value'=>$setting['Map']['lng'],'type'=>'hidden']); ?>
                            <div id="map" style="height: 400px;width: 100%;"></div>
                        </div>
                    </div>
                </div>

                <div id="dropdown5" class="tab-pane fade" role="tabpanel">
                    <div class="row row-padding">
                        <div class="col-block col-sm-3">
                            <?php echo $this->Form->control('Google.analitics',[
                                'value'=>$setting['Google']['analitics'],
                                'type' => 'textarea',
                                'escape' => false,
                                'class'=>'form-control']);?>
                        </div>
                        <div class="col-block col-sm-3">
                            <?php echo $this->Form->control('Hotjar.code',[
                                'value'=>$setting['Hotjar']['code'],
                                'type' => 'textarea',
                                'escape' => false,
                                'label' => 'Hotjar',
                                'class'=>'form-control'
                            ]);?>
                        </div>
                    </div>
                </div>


                <div id="dropdown6" class="tab-pane fade" role="tabpanel">
                    <div class="row row-padding">
                        <div class="col-block col-sm-6">
                            <?= $this->Form->control('DomainPage.text-{LANG}',[
                                'value' => $setting['DomainPage'],
                                'lang' => true,
                                'label' => __da('DomainPage text'),
                                'prefix' => 'text-',
                                'templates' => ['inputContainer' => '{{content}}']
                            ]) ?>


                        </div>

                        <div class="row row-padding">


                    <!-- coloana a doua --->
                    <div class="row row-padding">
                        <div class="col-block col-sm-6">
                            <?= $this->Form->control('DomainPage.text2-{LANG}',[
                                'value' => $setting['DomainPage'],
                                'lang' => true,

                                'label' => __da('DomainPage text2'),
                                'prefix' => 'text2-',
                                'templates' => ['inputContainer' => '{{content}}']
                            ]) ?>



                        </div>




                    </div>


                    <!-- coloana a trea --->
                    <div class="row row-padding">
                        <div class="col-block col-sm-6">
                            <?= $this->Form->control('DomainPage.text4-{LANG}',[
                                'value' => $setting['DomainPage'],
                                'lang' => true,
                                'label' => __da('DomainPage text4'),
                                'prefix' => 'text4-',
                                'templates' => ['inputContainer' => '{{content}}']
                            ]) ?>


                        </div>


                        <div class="col-block col-sm-6">
                            <div class="images-gallery clearfix">
                                <div class="gallery-item logo3">
                                    <div class="<?php echo !is_file(WWW_ROOT.'img/logos3/'.conf('logo3.name')) ? 'hidden' : ''; ?>">
                                        <div class="image"><img id="logo3-img" src="/img/logos3/<?= conf('logo3.name') ?>" alt=""></div>
                                        <button class="btnDeleteItem"><i class="icon-trash-o"></i></button>
                                    </div>
                                    <input type="file" id="logo3" name="logo3" style="display: none;">
                                    <button class="btnAddItem <?php echo is_file(WWW_ROOT.'img/logos3/'.conf('logo3.name')) ? 'hidden' : ''; ?>"></button>
                                </div>
                            </div>
                        </div>

                        <script>
                            $('.logo3 .btnAddItem').click(function (e) {
                                e.preventDefault();
                                $(this).prev().click();
                            });
                            $('.logo3 .btnDeleteItem').click(function (e) {
                                e.preventDefault();
                                if (!confirm('Are you sure?')) {
                                    return false;
                                }
                                item = this;
                                $(item).parent().addClass('hidden').parent().find('.btnAddItem').removeClass('hidden');
                                $.ajax('/admin/settings/deleteL', {
                                    method: "POST",
                                    dataType: "json",
                                    success: function (response) {
                                    },
                                    error: function () {
                                    }
                                });

                            });

                            function readFile() {

                                if (this.files && this.files[0]) {

                                    var FR= new FileReader();

                                    FR.addEventListener("load", function(e) {
                                        $("#logo2-img").parent().parent().removeClass('hidden').next().next().addClass('hidden');
                                        document.getElementById("logo3-img").src = e.target.result;
                                    });

                                    FR.readAsDataURL( this.files[0] );
                                }

                            }

                            document.getElementById("logo3").addEventListener("change", readFile);

                        </script>

                    </div>


                </div>


            </div>
        </div>

    </div>

    <div class="row row-padding">
        <!--  <div class="col-block col-sm-12">
             <input type="submit" class="btn btn-green btn-large pull-right" value="Salvează">
         </div> -->
        <?php echo $this->Form->control('Salvează',['type'=>'submit','templateVars' => ['class' => 'btn btn-green btn-large pull-right','divclass'=>'col-sm-12']]);?>
    </div>
    <?= $this->Form->end() ?>
    <script>
        var z   = <?php echo $setting['Map']['zoom']  ? $setting['Map']['zoom'] :  10 ?>;
        var lat = <?php echo $setting['Map']['lat']   ? $setting['Map']['lat']  : 47.04111801  ?>;
        var lng = <?php echo $setting['Map']['lng']   ? $setting['Map']['lng']  :28.8652038 ?>;
        var markers = [];
        function initMap() {
            $('#load_google_map').val(1);
            var uluru = {lat: lat, lng: lng};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: z,
                scrollwheel: false,
                center: uluru
            });
            <?php if($setting['Map']['lat'] && $setting['Map']['lng']){?>
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
            google.maps.event.addListener(marker,'click',function() {
                setMapOnAll(null);
            });
            markers.push(marker);
            <?php } ?>
            google.maps.event.addListener(map, 'zoom_changed', function() {
                z = map.getZoom();
                $('#map-zoom').val(z);
            });
            google.maps.event.addListener(map, "click", function(event) {
                var lat = event.latLng.lat();
                var lng = event.latLng.lng();
                $('#map-lat').val(lat);
                $('#map-lng').val(lng);
                $('#map-zoom').val(z);
                placeMarker(event.latLng);
            });
            function placeMarker(location) {
                setMapOnAll(null);
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                markers.push(marker);
                google.maps.event.addListener(marker,'click',function() {
                    setMapOnAll(null);
                });
            }
        }
        // Sets the map on all markers in the array.
        function setMapOnAll(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
            markers = [];
        }
    </script>
    <script>
        // Clear Cache Button
        $('#clear-cache').click(function (e) {
            e.preventDefault();
            $.ajax({
                type: "POST",
                url: "/admin/settings/clearCache",
                dataType: "json",
                cache: false,
                success: function (response) {
                    if (typeof response.success !== 'undefined') {
                        window.location.reload();
                    }
                }
            });
        });
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqUCVfzDLd9J21uAZQ3bCczFdAQBYb93U"></script>
</div>