<!DOCTYPE html>
<html class="<?php echo LANG ?>">
    <head>
        <?= $this->Html->charset() ?>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="format-detection" content="telephone=no">
        <meta name="theme-color" content="#34495E">

        <?php echo $this->element('meta');?>

        <?php echo $this->fetch('before-css'); ?>
        <?php echo $this->Html->css('/css/final.css?v='.filemtime('css/final.css')); ?>
        <?php echo $this->fetch('after-css'); ?>

        <link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:800|Roboto:300,400,500,700&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">

        <?php echo $this->element('favicons'); ?>
        <?php if (conf('Google.analitics') != null): ?>
            <?= conf('Google.analitics') ?>
        <?php endif; ?>

        <?php if (conf('Google.remarketing') != null): ?>
            <?= conf('Google.remarketing') ?>
        <?php endif; ?>

    </head>
    <body class="

     <?php
    if (ACTION_NAME === 'index') {
        echo  'home_page';
    } else if (ACTION_NAME === 'wines') {
        echo 'simple_page';
    } else {
        echo 'other_page';
    }
    ?>
   <?php echo 'tmp-'.substr(md5(CONTROLLER_NAME.ACTION_NAME),0,6) ?>">

        <?php echo $this->element('header') ?>
        <main>
            <?= $this->fetch('content') ?>
            <?php if((ACTION_NAME === 'index') || (ACTION_NAME === 'wines') ){
                echo $this->element('footer');
            } else {
                echo $this->element('footer2');
            }?>
        </main>

        <?php echo $this->fetch('before_js') ?>
        <?php echo $this->Html->script('/js/final.js?v='.filemtime('js/final.js') ); ?>
        <?php echo $this->fetch('after_js') ?>
    </body>
</html>
