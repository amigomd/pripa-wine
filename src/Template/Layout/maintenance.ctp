<!DOCTYPE html>
<html class="maintenance-page">
    <head>
        <?= $this->Html->charset() ?>

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="format-detection" content="telephone=no">
        <meta name="theme-color" content="#6C38A0">

        <?= $this->element('meta') ?>
        <?= $this->element('favicons') ?>

        <link type="text/css" rel="stylesheet" href="/common/maintenance/css/normalize.css?v=1">

        <!-- Client CSS -->
        <link type="text/css" rel="stylesheet" href="/common/maintenance/css/styles.css?v=2">

        <?php if (conf('Google.analitics') != null): ?>
            <?= conf('Google.analitics') ?>
        <?php endif; ?>

        <?php if (conf('Google.remarketing') != null): ?>
            <?= conf('Google.remarketing') ?>
        <?php endif; ?>

    </head>

    <body>
    <div class="wrapper">
        <div class="main-block">
            <div class="main-wrap">
                <div class="cover-container clearfix">
                    <a class="logo" href="<?= $this->Url->build(['controller' => 'Pages', 'action' => 'display']) ?>">
                        <svg width="70" height="45" viewBox="0 0 70 45" xmlns="http://www.w3.org/2000/svg">
                            <path d="M51.14 17.53c0 1.91-1.54 3.46-3.45 3.46h-2.85V2.36h3.45a2.85 2.85 0 0 1 2.85 2.85v12.32zM49.03.13a4.5 4.5 0 0 1 4.5 4.5v13.5a5.1 5.1 0 0 1-5.1 5.1h-3.57V42.2c0 .62.38 1.2.95 1.45l.47.2c.25.12.4.37.4.64V45h-6.04v-.51a.7.7 0 0 1 .4-.63l.48-.21c.57-.26.94-.83.94-1.45V2.92c0-.62-.37-1.18-.95-1.44l-.46-.2a.7.7 0 0 1-.41-.64V.13h8.39zM8.42.13a4.5 4.5 0 0 1 4.5 4.5v13.5a5.1 5.1 0 0 1-5.1 5.1H4.23V42.2c0 .62.37 1.2.94 1.45l.47.2c.25.12.41.37.41.64V45H0v-.51a.7.7 0 0 1 .4-.63l.48-.21c.57-.26.96-.83.96-1.45V2.93c0-.63-.36-1.2-.94-1.45l-.47-.2a.7.7 0 0 1-.4-.64V.13h8.39zm2.11 17.4V5.21a2.85 2.85 0 0 0-2.85-2.85H4.23v18.63h2.85c1.9 0 3.45-1.55 3.45-3.46zM69.6 43.86a.7.7 0 0 1 .41.63V45h-6.05v-.51a.7.7 0 0 1 .4-.63l.48-.21c.57-.26.94-.83.94-1.45l.01-18.98h-6.47V42.2c0 .62.37 1.2.94 1.45l.47.2c.25.12.41.37.41.64V45h-6.05v-.51a.7.7 0 0 1 .4-.63l.48-.21c.57-.26.94-.83.94-1.45l.01-36.73A5.47 5.47 0 0 1 62.38 0h.34a5.47 5.47 0 0 1 5.46 5.47V42.2c0 .62.37 1.2.94 1.45l.47.2zM59.31 20.9h6.47V5.58a3.24 3.24 0 0 0-6.47 0V20.9zM31.68.64V.13h6.05v.5a.7.7 0 0 1-.4.64l-.47.21c-.58.26-.95.82-.95 1.45V42.2c0 .62.37 1.2.95 1.45l.46.2c.25.12.41.37.41.64V45h-6.05v-.51a.7.7 0 0 1 .41-.63l.47-.21c.57-.26.94-.83.94-1.45V2.93c0-.63-.37-1.2-.94-1.45l-.47-.2a.7.7 0 0 1-.4-.64zm-2.33 43.22a.7.7 0 0 1 .41.63V45H27.1c-.85 0-1.54-.7-1.54-1.54V26.32c0-1.65-1.3-3-2.92-3.1h-3.45V42.2c0 .62.38 1.2.96 1.45l.46.2c.25.12.41.37.41.64V45h-6.05v-.51a.7.7 0 0 1 .41-.63l.47-.21c.57-.26.96-.83.96-1.45V2.93c0-.63-.37-1.2-.95-1.45l-.46-.2a.7.7 0 0 1-.41-.64V.13h8.39a4.5 4.5 0 0 1 4.5 4.5v13.61c0 1.51-.67 2.87-1.74 3.78 1.1.87 1.8 2.21 1.8 3.72v16.7c0 .48.28.91.71 1.1l.7.32zm-3.86-26.2V5.2a2.85 2.85 0 0 0-2.85-2.85H19.2v18.62h2.97a3.33 3.33 0 0 0 3.33-3.32z" fill="#FFF" style="
fill: black;
"fill-rule="evenodd"/>
                        </svg>
                    </a>
                    <h3><?= conf('Maintenance.message-'.LANG) ?></h3>
                    <div class="contacts">
                        <?php if (conf('Contact.email') != null) { ?>
                            <a href="mailto:<?= conf('Contact.email') ?>"><?= conf('Contact.email') ?></a>
                        <?php } ?>
                        <?php if (conf('Contact.phone') != null) { ?>
                            <?php $phone = explode(',', conf('Contact.phone')) ?>
                            <a href="tel:<?= isset($phone[0]) ? $phone[0] : '' ?>"><?= isset($phone[0]) ? $phone[0] : '' ?></a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </body>
</html>



