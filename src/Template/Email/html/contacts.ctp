<table style="width: 100%; max-width: 600px; margin: 0 auto; font-family: Arial, Helvetica, sans-serif; font-size: 14px; font-weight: 400; color: #000000; table-layout: fixed;-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale;" cellpadding="0" cellspacing="0">
    <tr>
        <td style="border: 1px solid #9e9e9e3b;padding: 6px 6px;"><?php echo __ds('Email or phone') ?></td>
        <td style="border: 1px solid #9e9e9e3b;padding: 6px 6px;"><?php echo $request->phone  ?></td>
    </tr>
    <tr>
        <td style="border: 1px solid #9e9e9e3b;padding: 6px 6px;"><?php echo __ds('Message') ?></td>
        <td style="border: 1px solid #9e9e9e3b;padding: 6px 6px;"><?php echo $request->message  ?></td>
    </tr>
</table>