<section class="simple-section page-section">


    <div id="fullpage">
        <div class="section">
            <div class="first-block">
                <div class="slide-image" style="background-image: url('/img/logos2/<?=conf('logo2.name')?>"></div>
                <div class="main-title">
                    <?php if (__ds('Config.projectName-eng') != null): ?>
                        <div class="h5"><?=conf('Config.projectName-'.LANG); ?></div>
                    <?php endif; ?>
                    <img class="line" src="img/line.png" width="274px" >
                    <?php if (conf('Config.slogan-eng') != null): ?>
                        <div class="h1"><?=conf('Config.slogan-'.LANG);?></div>
                    <?php endif; ?>
                </div>
                 <div class="down">
                    <svg width="27" height="19" viewBox="0 0 27 19" xmlns="http://www.w3.org/2000/svg">
                        <path d="M2 1h23L13.5 17z" stroke="#FFF" stroke-width="2" fill="none" fill-rule="evenodd"/>
                    </svg>
                </div>
            </div>
        </div>
        <div class="section">
            <div class="second-block">
                <div class="swiper-container horizontal-swiper">

                    <div class="swiper-wrapper">
                        <?php foreach ($sliders as $key => $slider) { ?>
                        <div class="swiper-slide">
                            <div class="slide-image" style="background-image: url('<?= $slider->getImageItem('image','gallery','Gallery')?>"></div>
                            <div class="title-block"><h3><?=$slider->title;?></h3></div>

                            <div class="slider-block">
                                <div class="slider-block-wrap">
                                    <div class="slider-block-inner"><?=$slider->description; ?></div>
                                </div>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                    <div class="button-next"></div>
                    <div class="button-prev"></div>
                </div>

                <div class="line-1 hidden-xs hidden-sm"></div>
                <div class="line-2 hidden-xs hidden-sm"></div>
                <div class="line-3 hidden-xs hidden-sm"></div>
                <div class="line-4 hidden-xs hidden-sm"></div>
                <div class="line-5 hidden-xs hidden-sm"></div>
            </div>
        </div>
    </div>

</section>

<?php $this->start('after_js') ?>

<script>

    // var swiper_v = new Swiper('.vertical-swiper', {
    //     direction: 'vertical',
    //     mousewheel: true,
    //     speed: 500,
    //     simulateTouch: false,
    //     // followFinger: false,
    //     // allowTouchMove: false,
    //     // touchReleaseOnEdges: true,
    //     // resistance: false,
    //     // preventClicksPropagation: false,
    //     // slideToClickedSlide: true,
    //     freeMode: true,

    //     // pagination: {
    //     //   el: '.swiper-pagination',
    //     //   // clickable: true,
    //     // },
    //     on: {
    //         init: function () {
    //             console.log('swiper initialized');
    //         },
    //         slideChange: function () {
    //             console.log('swiper Change');
    //             $('.slide-lines').addClass('animate');
    //         },
    //     },
    // });


    var swiper_h = new Swiper('.horizontal-swiper', {
        // direction: 'vertical',
        // mousewheel: true,
        speed: 500,
        effect: 'fade',
        // simulateTouch: false,
        // autoHeight: true,
        keyboard: true,
        loop: true,
        // forceToAxis: true,
        pagination: {
            el: '.swiper-pagination',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.button-next',
            prevEl: '.button-prev',
        },
        // freeMode: true,
        // scrollbar: {
        //     el: '.swiper-scrollbar',
        // },
    });



    (function() {

        // $('#fullpage').fullpage({
        //     //options here
        //     autoScrolling:true,
        //     scrollHorizontally: true
        // });

        $('#fullpage').fullpage({
            responsiveWidth: 1024,
            // autoScrolling:false,
            onLeave: function( section, origin, destination, direction){
                // var loadedSlide = this;

                console.log('111');
                $('.second-block').addClass('animate');

                //first slide of the second section
                // if(section.anchor == 'secondPage' && destination.index == 1){
                //     alert("First slide loaded");
                // }

                // //second slide of the second section (supposing #secondSlide is the
                // //anchor for the second slide)
                // if(section.index == 1 && destination.anchor == 'secondSlide'){
                //     alert("Second slide loaded");
                // }
            }
        });




        // change "revapi1" here to whatever API name your slider uses (see notes below)

        // $('body').on('mousewheel DOMMouseScroll', function(event) {

        //     window_w = $(window).width();
        //     if (window_w > 1024 ) {
        //         if(event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
        //             console.log('top');
        //             var pos = $('.first-block').offset();
        //             $('html,body').stop().animate({
        //           scrollTop: pos.top
        //         }, 700);
        //         }
        //         else {
        //             console.log('down');
        //             var pos = $('.second-block').offset();
        //             $('html,body').stop().animate({
        //                 scrollTop: pos.top
        //             }, 700, function() {
        //                 $('.second-block').addClass('animate');
        //             });
        //         }
        //         event.preventDefault();
        //     }
        // });

        // var pos_load = $('.second-block').offset();

        // if ($(window).scrollTop() >= pos_load.top ) {
        //
        // }


    })();

</script>
<?php $this->end('after_js') ?>
