<section class="simple-section body-section contacts-page" data-midnight="black">
        <div class="container">
            <div class="row row-margin-100">
                <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-1 text-right top-page-title">
                    <h1><?= $page->title ?></h1>
                    <div class="h1"><?= $page->sub_title ?></div>
                </div>
                <div class="col-xs-12 col-sm-5 col-sm-offset-1 col-md-5 col-md-offset-1 top-page-desc">
                    <h3><?= $page->content ?></h3>

                    <div class="contacts-table clearfix">
                        <div class="table-address"><?=conf('Contact.address')?></div>
                        <div class="table-hours"><?=conf('Contact.schendle')?></div>
                        <div class="table-email"><a href="mailto:<?=conf('Contact.email')?>"><?=conf('Contact.email')?></a></div>
                        <div class="table-phone"><a href="tel:<?=conf('Contact.phone_mobile')?>"><?=conf('Contact.phone_mobile')?></a></div>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">

                    <?php if (count($shops) >= 1) { ?>
                        <h6><?= __ds('parteneri') ?></h6>
                    <?php } ?>

                    <div class="row">
                        <? foreach ($shops as $shop) { ?>
                        <div class="col-xs-12 col-sm-4 col-lg-3">
                            <div class="shop-block row">
                                <div class="col-xs-12">
                                    <div class="shop-image">
                                        <img src="<?= $shop->getImageItem('image','shop','Shops')?>">
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="shop-desc">
                                        <p><?= $shop->city; ?></p>
                                        <p><?= $shop->adress; ?></p>
                                        <p><a href="tel:<?= $shop->phone; ?>"><?= $shop->phone; ?></a></p>
                                        <p><a href="https://<?= $shop->link; ?> " target="_blank"><?= $shop->link; ?></a></p>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="map-section" data-midnight="black">
        <div class="contacts-map" id="map"></div>
    </section>
<?php $this->start('after_js') ?>
<script>
    // Start midnight
    $(document).ready(function(){
        // Change this to the correct selector.
        $('.header__logo').midnight();
        $('.trigger-block').midnight();
    });
</script>


<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD-Hj7f3_cCJIbEHFTeIOW8zbKyp4p2hXo&libraries=places"></script>

<?= $this->Html->script('/js/infobox.js') ?>

<script>
    // When the window has finished loading create our google map below
    google.maps.event.addDomListener(window, 'load', init);

    function init() {
        // Basic options for a simple Google Map
        // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions

       var  Lat = <?= conf('Map.lat') ?>;
       var Lng = <?= conf('Map.lng') ?>;

       var zoom = <?= conf('Map.zoom') ?>;

        var LatShift = 0.00132;
        Lat += LatShift;

        var mapOptions = {
            // How zoomed in you want the map to start at (always required)
            zoom: zoom,
            disableDefaultUI: true,
            scrollwheel: false,
            zoomControl: true,

            // The latitude and longitude to center the map (always required)
            center: new google.maps.LatLng(Lat, Lng), // New York

            // How you would like to style the map.
            // This is where you would paste any style found on Snazzy Maps.
            // styles: [{"stylers":[{"saturation":-100},{"gamma":1}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"on"},{"saturation":50},{"gamma":0},{"hue":"#50a5d1"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#333333"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"weight":0.5},{"color":"#333333"}]},{"featureType":"transit.station","elementType":"labels.icon","stylers":[{"gamma":1},{"saturation":50}]}]
        };

        // Get the HTML DOM element that will contain your map
        // We are using a div with id="map" seen below in the <body>
        var mapElement = document.getElementById('map');

        // Create the Google Map using our element and options defined above
        var map = new google.maps.Map(mapElement, mapOptions);

        Lat -= 0.00050;

        // Let's also add a marker while we're at it
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(Lat, Lng),
            map: map,
            optimized: false,
            zIndex:99999999,
            icon: 'img/pin.svg',
        });

        var boxText = document.createElement("div");
        boxText.style.cssText = "background: #f8f8f8; padding: 25px 60px 30px 30px; text-align: left; font-size: 18px; line-height:24px; width: 175px; font-family: Georgia, serif;";
        boxText.innerHTML = "<p style='margin: 0 0 25px;'><?= __ds('GMAP') ?></p><a href='<?=conf('App.googlemap')?>' style='margin: 0 30px 0 0'><img src='img/gmap.png' width='50' height='50'></a><a href='<?=conf('App.applemap')?>'><img src='img/amap.png' width='50' height='50'></a>";

        var myOptions = {
            content: boxText
            ,disableAutoPan: false
            ,maxWidth: 0
            ,pixelOffset: new google.maps.Size(-132, 20)
            ,zIndex: null
            ,infoBoxClearance: new google.maps.Size(1, 1)
            // ,isHidden: false
            ,pane: "floatPane"
            ,enableEventPropagation: false
        };


        var ib = new InfoBox(myOptions);

        ib.open(map,marker);

    }
</script>
<?php $this->end('after_js') ?>
