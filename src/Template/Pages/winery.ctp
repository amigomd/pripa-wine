
<section class="simple-section image-section" style="background-image: url('/img/popup/<?=conf('popup.name')?>');">
    <div class="container">
        <div class="table-block-wrap">
            <div class="table-block-inner">
                <div class="page-title">
                    <h1><?= $page->title ?></h1>
                    <div class="h1"><?= $page->sub_title ?></div>
                </div>
            </div>
        </div>
    </div>

    <div class="down">
        <svg width="27" height="19" viewBox="0 0 27 19" xmlns="http://www.w3.org/2000/svg">
            <path d="M2 1h23L13.5 17z" stroke="#FFF" stroke-width="2" fill="none" fill-rule="evenodd"/>
        </svg>
    </div>
</section>
<?php
$items = array();
foreach($domains as $domain) {
    $items[] = $domain;
}
//print_r($items);
?>

<section class="simple-section body-section winery-section" data-midnight="black">
    <div class="container">
        <div class="row row-margin-70">
            <div class="col-xs-12 col-md-4 col-md-offset-2">
                <h3 class="first-desc"><?=$items[0]['title'];?></h3>
            </div>
            <div class="col-xs-12 col-md-4 col-md-offset-1">
                <img class="mob-width" src="/pic/domains/6/<?=$items[0]['image'];?>">
            </div>
        </div>

        <div class="row row-margin-150">
            <div class="col-xs-3 col-md-2 col-md-offset-1 content-align-center domains-small-image">
                <img src="/pic/domains/7/<?=$items[1]['image'];?>">
            </div>
            <div class="col-xs-9 col-md-3">
                <p class="first-desc"><?=$items[1]['title'];?></p>
            </div>
        </div>

        <div class="row row-margin-150 row-vertical">
            <div class="col-xs-12 col-md-4 col-md-offset-2">
                <img class="mob-width mob-width-left" src="/pic/domains/8/<?=$items[2]['image'];?>">
            </div>
            <div class="col-xs-12 col-md-3 col-md-offset-1 align-self-center">
                <p class="first-desc"><?=$items[2]['title'];?></p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-4 col-md-offset-1 col-md-push-6">
                <img class="mob-width" src="/pic/domains/9/<?=$items[3]['image'];?>">
            </div>
            <div class="col-xs-12 col-md-3 col-md-offset-3 col-md-pull-5">
                <p class="first-desc"><?=$items[3]['title'];?></p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-1">
                <img class="last-img" src="../img/tools.png">
            </div>
        </div>
    </div>
</section>


<?php $this->start('after_js') ?>
<script>
    // Start midnight
    $(document).ready(function(){
        // Change this to the correct selector.
        $('.header__logo').midnight();
        $('.trigger-block').midnight();
    });
</script>
<?php $this->end('after_js') ?>


