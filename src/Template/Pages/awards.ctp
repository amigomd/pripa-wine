<section class="simple-section body-section awards-page" data-midnight="black">
        <div class="container">
            <div class="row row-margin-100">
                <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-1 text-right top-page-title">
                    <h1><?= $page->title ?></h1>
                    <div class="h1"><?= $page->sub_title ?></div>
                </div>
                <div class="col-xs-12 col-sm-5 col-sm-offset-1 col-md-5 col-md-offset-1 top-page-desc">
                    <h3><?= $page->content ?></h3>
                </div>
            </div>

            <div class="row">

                <? foreach ($awards as  $key => $award) { ?>
                <div class="col-xs-12 col-sm-6 col-md-4 <?=$key%2==0 ? 'col-md-offset-2' : '' ?>">
                    <div class="awards-block row <?=$award->multiply == 1 ? 'few-awards' : '' ?>">
                        <div class="col-xs-6 col-sm-6">
                            <div class="awards-block-image">
                                <img src="<?= $award->getImageItem('image','awards','Awards')?>">
                                <div class="position <?=$award->awards_id;?>"></div>
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6">
                            <div class="awards-block-desc">
                                <?=$award->description;?>
                                <time> <?= $award->year; ?></time>
                            </div>
                        </div>
                    </div>
                </div>
               <?php }?>
            </div>

        </div>
    </section>

<?php $this->start('after_js') ?>
<script>
    // Start midnight
    $(document).ready(function(){
        // Change this to the correct selector.
        $('.header__logo').midnight();
        $('.trigger-block').midnight();
    });
</script>
<?php $this->end('after_js') ?>


