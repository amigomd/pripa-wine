<section class="page-section page-title">
	<div class="container">
		<h1 class="text-center"><?php echo $page->title;?></h1>
	</div>
</section>
<section class="page-section page-content simple-page">
	<div class="container">

		<div class="row row-center">
			<div class="col-xs-12 col-md-10">
                <?php echo $page->descriptions;?>
			</div>
		</div>

	</div>
</section>
