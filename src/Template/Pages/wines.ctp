    <section class="simple-section page-section wines-page">
        <div class="swiper-container wines-swiper">
            <div class="swiper-wrapper">
                <? foreach ($wines as $wine) { ?>
                <div class="swiper-slide">
                    <div class="slide-image" style="background-image: url('<?= $wine->getImageItem('image','winery','Wines')?>');"></div>
                    <div class="slide-desc">
                        <div class="slide-desc-wrap">
                            <div class="slide-desc-inner">
                                <h4><?=$wine->title;?></h4>
                                <div class="desc-table clearfix">
                                    <div class="table-year"><?=$wine->year;?></div>
                                    <div class="table-type"><?=$wine->type;?></div>
                                    <div class="table-alc"><?=$wine->alc;?></div>
                                    <div class="table-vol"><?=$wine->volume;?></div>
                                </div>

                                <p> <?=$wine->description;?></p>


                                <div class="close-slide-desc"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }?>


            </div>
            <div class="swiper-pagination"></div>
        </div>
    </section>

    <?php $this->start('after_js') ?>

<script>
    var swiper_w = new Swiper('.wines-swiper', {
        // direction: 'vertical',
        // mousewheel: true,
        speed: 500,
        // effect: 'fade',
        pagination: {
            el: '.swiper-pagination',
            // type: 'fraction',
        },
        // slidesPerView: 3,
        breakpointsInverse: true,
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 0
            },
            // when window width is >= 480px
            768: {
                slidesPerView: 2,
                spaceBetween: 2
            },
            1025: {
                slidesPerView: 3,
                spaceBetween: 2
            }
        },
    });
</script>
    <?php $this->end('after_js') ?>
