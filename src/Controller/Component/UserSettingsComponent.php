<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Event\EventDispatcherTrait;
use Cake\ORM\TableRegistry;

/**
 * User log component
 */
class UserSettingsComponent extends Component
{
	use EventDispatcherTrait;
	/**
	 * Request object
	 *
	 * @var \Cake\Http\ServerRequest
	 */
	public $request;

	/**
	 * Response object
	 *
	 * @var \Cake\Network\Response
	 */
	public $response;

	/**
	 * Instance of the Session object
	 *
	 * @var \Cake\Network\Session
	 * @deprecated 3.1.0 Will be removed in 4.0
	 */
	public $session;

	/**
	 * TableRegistry
	 * @var \Cake\ORM\TableRegistry
	 */
	private $_userSettings;

	/**
	 * @var Controller
	 */
	private $_controller;

	/**
	 * @var Controller
	 */
	private $_default_settings = ['per_page' => 10];

	private $_saved_settings = [];


	public function initialize(array $config)
	{
		$controller = $this->_controller = $this->_registry->getController();
		$this->eventManager($controller->eventManager());
		$this->response =& $controller->response;
		$this->session = $controller->request->session();
		$this->_userSettings = TableRegistry::get('UserSettings');
	}

	/**
	 * Set key in db
	 * @param $key
	 * @param $value
	 */
	public function set($key,$value = null){
		if(in_array($key,array_keys($this->_default_settings)) && $this->session->check('Auth.User.id')) {
			$setting = $this->_userSettings->find()->where(['col_key' => $key,'user_id'=>$this->session->read('Auth.User.id')])->first();
			if (!$setting) {
				$setting = $this->_userSettings->newEntity();
				$setting->value = $value ? $value : $this->_default_settings[$key];
			}else{
				$setting->value = $value ? $value : $setting->value;
			}
			$setting->user_id = $this->session->read('Auth.User.id');
			$setting->col_key = $key;
			$this->_userSettings->save($setting);
			return $setting->value;
		}
	}

	/**
	 * @param $key
	 * @return |Cake\ORM\ResultSet
	 */
	public function get($key){
		if(isset($this->_saved_settings[$key])){
			return $this->_saved_settings[$key];
		}
		$value = $this->set($key);
		$this->_saved_settings[$key] = $value;
		return $value;
	}

}