<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

/**
 * AmigoFile component
 */
class AmigoFileComponent extends Component
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    /**
     * Ошибка.
     * @var string
     */
    public $error = '';

    /**
     * Загрузить файл на сервер.
     * @param string $file Массив данных загруженного на сервер файла ($_FILES).
     * @param string $folder Папка в которую необходимо скопировать загруженный файл.
     * Если параметр не указан, файл будет скопирован в текущую папку.
     * @param string $filename Название файла, которое будет создано при копировании.
     * Еслис параметр не указан, название файла не будет изменено.
     * @param array $allowedExtensions Массив допустимых расширений для загрузки на сервер.
     * @return string Название файла.
     */
    public function upload($file, $folder = null, $filename = null, $allowedExtensions = array()) {
        //Существует ли файл
        if (empty($file) || empty($file['tmp_name'])) {
            $this->error = 'Файл пуст.';
            return false;
        }

        //Удаление лишних символов и пробелов
        $newFileName = empty($filename) ? $file['name'] : $filename;
        $newFileName = preg_replace("/[^\da-z.]/i", "_", $newFileName);

        //Проверка если есть уже такой файл на сервере
        $verifyDestinationImage = new File($folder.DS.$newFileName);
        if($verifyDestinationImage->exists()){
            $newFileName = uniqid().'_'.$newFileName;
        }

        // Создание папки и загрузка картинки на сервер
        $dir = new Folder();
        if ($dir->create($folder,0777)) {
            $tmp_file = new File($file['tmp_name']);
            if(!$tmp_file->exists()){
                $this->error = 'Файл пуст.';
                return false;
            }
            $new_file = new File($folder . DS . $newFileName);
            if (!$tmp_file->copy($folder . DS . $newFileName)) {
                $this->error = 'Не удалось переместить загруженный файл в каталог.';
                return false;
            }
            $new_file->close();
            $tmp_file->delete();
            return $newFileName;
        }else{
            $this->error = 'Временная папка не создана.';
        }
//
//        // Определить папку, в которую должен быть загружен файл.
//        $cwd = getcwd() . DS;
//        $dstFolder = !$folder ? './' : $cwd . $folder . DS;
//        $controllerFolderArray = explode('/', $folder);
//
//        $old = umask(0); // Full Permissions
//        if (isset($controllerFolderArray[1])) {
//            $controllerFolder = $cwd . $controllerFolderArray[0] . DS . $controllerFolderArray[1] . DS;
//            @mkdir($controllerFolder, 0777, true);
//            if (isset($controllerFolderArray[2])) {
//                $idFolder = $controllerFolder . DS . $controllerFolderArray[2] . DS;
//                @mkdir($idFolder, 0777, true);
//            }
//        }

//        @mkdir($folder, 0777, true);
        // Full Permissions
//        umask($old);

        // Проверить расширение файла.
//        $extension = mb_strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
//        if (!empty($allowedExtensions) && is_array($allowedExtensions)) {
//            if (!in_array($extension, $allowedExtensions)) {
//                $this->error = 'Недопустимое расширение файла: ' . $extension;
//                return false;
//            }
//        }
////
//        $old = umask(0); // Full Permissions
////        // Сформировать конечный путь к файлу.
//        $newFilename = empty($filename) ? $file['name'] : $filename;
//        $newFilename = preg_replace("/[^\da-z.]/i", "_", $newFilename); // replace non numeric symbol with _ , except point
//        $destination = preg_replace('#/+#', '/', $dstFolder);
//        @mkdir($destination, 0777, true);
//        umask($old); // Full Permissions
//
//        $destination .= $newFilename;
////
//        // Если файл в указанной папке уже существует, сгенерировать другое название для файла.
//        if (file_exists($destination)) {
//            $newFilename = pathinfo($newFilename, PATHINFO_FILENAME) . '-' . mt_rand(10000, 100000) . '.' . $extension;
//            $destination = $dstFolder . $newFilename;
//        }
//
//        // Upload.
//        if (!move_uploaded_file($file['tmp_name'], $destination)) {
//            $this->error = 'Не удалось переместить загруженный файл в каталог.';
//            return false;
//        }
//
//        return $newFilename;
    }

    /**
     * Копировать файл.
     * @param string $srcPath Копируемый файл.
     * @param string $dstPath Название каталога или полный путь к новому файлу.
     * @return boolean
     */
    public static function copy($srcPath, $dstPath) {
        if (!is_file($srcPath)) {
//            $this->error = 'Не удается копировать файл (' . $srcPath . '), т.к. он не найден.';
            return false;
        }

        if (is_dir($dstPath)) {
            $filename = pathinfo($srcPath, PATHINFO_BASENAME);
            $dstPath = $dstPath . '/' . $filename;
        }
        if (!@copy($srcPath, $dstPath)) {
//            $this->error = 'Не удается копировать файл (' . $srcPath . ') в "' . $dstPath . '".';
            return false;
        }

        return true;
    }

    /**
     * Переместить файл.
     * @param string $srcPath Перемещаемый файл.
     * @param string $dstPath Название каталога или полный путь к новому файлу.
     * @return boolean
     */
    public function move($srcPath, $dstPath) {
        if (!is_file($srcPath)) {
            $this->error = 'Не удается переместить файл (' . $srcPath . '), т.к. он не найден.';
            return false;
        }

        if (is_dir($dstPath)) {
            $filename = pathinfo($srcPath, PATHINFO_BASENAME);
            $dstPath = $dstPath . '/' . $filename;
        }

        if (!@rename($srcPath, $dstPath)) {
            $this->error = 'Не удается переместить файл (' . $srcPath . ') в "' . $dstPath . '".';
            return false;
        }

        return true;
    }

    /**
     * Удалить файл.
     * @param string $path Путь к файлу.
     * @return boolean
     */
    public function delete($path) {
        if (!is_file($path)) {
            $this->error = 'Ошибка удаления. Файл (' . $path . ') не найден.';
            return false;
        }

        if (!@unlink($path)) {
            $this->error = 'Не удалось удалить файл (' . $path . ').';
            return false;
        }

        return true;
    }

    public function resizeByWidth($srcPath, $newWidth, $dstPath = null, $stretch = false) {
        $imageSize = $this->getImageSize($srcPath);
        if (empty($imageSize)) {
            return false;
        }
        $scale = $newWidth / $imageSize[0];

        if (empty($dstPath)) {
            $dstPath = $srcPath;
        }

        if ($stretch == false && $newWidth >= $imageSize[0]) {
            $this->copy($srcPath, $dstPath);
            return array('width' => $imageSize[0], 'height' => $imageSize[1]);
        }

        return $this->resizeByScale($srcPath, $scale, $dstPath);
    }

    public function resizeByHeight($srcPath, $newHeight, $dstPath = null, $stretch = false) {
        $imageSize = $this->getImageSize($srcPath);
        if (empty($imageSize)) {
            return false;
        }
        $scale = $newHeight / $imageSize[1];

        if (empty($dstPath)) {
            $dstPath = $srcPath;
        }

        if ($stretch == false && $newHeight >= $imageSize[1]) {
            $this->copy($srcPath, $dstPath);
            return array('width' => $imageSize[0], 'height' => $imageSize[1]);
        }

        $this->resizeByScale($srcPath, $scale, $dstPath);
    }

    public function resizeByScale($srcPath, $scale, $dstPath = null, $stretch = false) {
        $imageSize = $this->getImageSize($srcPath);
        if (empty($imageSize)) {
            return false;
        }
        $imageType = $imageSize[2];

        $newWidth = round($imageSize[0] * $scale, 0);
        $newHeight = round($imageSize[1] * $scale, 0);

        switch ($imageType) {
            case IMAGETYPE_JPEG: $srcImage = imagecreatefromjpeg($srcPath);
                break;
            case IMAGETYPE_PNG: $srcImage = imagecreatefrompng($srcPath);
                break;
            case IMAGETYPE_GIF: $srcImage = imagecreatefromgif($srcPath);
                break;
            default:
                $this->error = 'Неверный тип изображения: ' . $imageType;
                return false;
        }

        $newImage = imagecreatetruecolor($newWidth, $newHeight);
        imagealphablending($newImage, false);
        imagesavealpha($newImage, true);
        imagecolorallocatealpha($newImage, 255, 255, 255, 127);
        imagecopyresampled($newImage, $srcImage, 0, 0, 0, 0, $newWidth, $newHeight, $imageSize[0], $imageSize[1]);

        if (empty($dstPath)) {
            $dstPath = $srcPath;
        } elseif (is_dir($dstPath)) {
            $dstPath .= '/' . basename($srcPath);
        }

        switch ($imageType) {
            case IMAGETYPE_JPEG: imagejpeg($newImage, $dstPath, dev_conf('Config.jpgQuality'));
                break;
            case IMAGETYPE_PNG: imagepng($newImage, $dstPath, dev_conf('Config.pngCompression'));
                break;
            case IMAGETYPE_GIF: imagegif($newImage, $dstPath);
                break;
        }

        imagedestroy($srcImage);
        imagedestroy($newImage);

        return array('width' => $newWidth, 'height' => $newHeight);
    }

    public function crop($srcPath, $x, $y, $w, $h, $dstPath = null) {
        $w = round($w, 0);
        $h = round($h, 0);

        if (empty($dstPath)) {
            $dstPath = $srcPath;
        } elseif (is_dir($dstPath)) {
            $dstPath .= '/' . basename($srcPath);
        }

        $imageSize = $this->getImageSize($srcPath);
        if (empty($imageSize)) {
            return false;
        }
        $imageType = $imageSize[2];

        switch ($imageType) {
            case IMAGETYPE_JPEG: $srcImage = imagecreatefromjpeg($srcPath);
                break;
            case IMAGETYPE_PNG: $srcImage = imagecreatefrompng($srcPath);
                break;
            case IMAGETYPE_GIF: $srcImage = imagecreatefromgif($srcPath);
                break;
            default:
                $this->error = 'Неверный тип изображения: ' . $imageType;
                return false;
        }

        $newImage = imagecreatetruecolor($w, $h);
        imagealphablending($newImage, false);
        imagesavealpha($newImage, true);
        imagecolorallocatealpha($newImage, 255, 255, 255, 127);
        imagecopyresampled($newImage, $srcImage, 0, 0, $x, $y, $w, $h, $w, $h);

        switch ($imageType) {
            case IMAGETYPE_JPEG: imagejpeg($newImage, $dstPath, 100);
                break;
            case IMAGETYPE_PNG: imagepng($newImage, $dstPath, 0);
                break;
            case IMAGETYPE_GIF: imagegif($newImage, $dstPath);
                break;
        }

        imagedestroy($srcImage);
        imagedestroy($newImage);
        return true;
    }

    function smartCrop($srcPath, $newWidth, $newHeight, $dstPath = null) {
        if (empty($dstPath)) {
            $dstPath = $srcPath;
        } elseif (is_dir($dstPath)) {
            $dstPath .= '/' . basename($srcPath);
        }

        $imageSize = $this->getImageSize($srcPath);
        if (empty($imageSize)) {
            return false;
        }

        $resizeByWidthScale = $newWidth / $imageSize[0];
        $resizeByHeightScale = $newHeight / $imageSize[1];
        $scale = $resizeByWidthScale > $resizeByHeightScale ? $resizeByWidthScale : $resizeByHeightScale;

        $this->resizeByScale($srcPath, $scale, $dstPath);
        $imageSize = getimagesize($dstPath);

        // если надо режем сверху и снизу
        if ($resizeByWidthScale < $resizeByHeightScale) {
            $x = ($imageSize[0] - $newWidth) / 2;
            $y = 0;
        } else {
            $x = 0;
            $y = ($imageSize[1] - $newHeight) / 2;
        }

        return $this->crop($dstPath, $x, $y, $newWidth, $newHeight);
    }

    /**
     * Получить информацию о изображении.
     * @param string $path Путь к файлу.
     * @return boolean|array Массив данных идентичных результату вызова функции getimagesize(), или
     * <b>false</b> если возникла ошибка или файл не является изображением.
     */
    private function getImageSize($path) {
        if (!is_file($path)) {
            return false;
        }

        $output = getimagesize($path);
        $imageType = $output[2];

        if (!in_array($imageType, array(IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_GIF))) {
            $this->error = 'Операция прервана. Формат файла (' . $path . ') не распознан как допустимый формат изображения.';
            return false;
        }

        return $output;
    }

    public function getSize($path, $human = false) {
        if (!is_file($path)) {
            $this->error = 'Не удалось вычислить размер изображения (' . $path . '). Файл не существует.';
            return false;
        }

        $bytes = filesize($path);

        if ($human) {
            return $this->humanFilesize($bytes);
        }

        return $bytes;
    }

    private function humanFilesize($bytes, $decimals = 2) {
        //$sz = 'BKMGTP';
        $sz = array('bytes', 'KB', 'MB', 'GB', 'TB', 'PB');
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . ' ' . @$sz[$factor];
    }

    /**
     * Оптимизировать изображение.
     * @param string $src - Путь к исходному изображению, которое необходимо оптимизировать.
     * @param int $quality - Качество от 0 до 100, где 0 худшее, а 100 лучшее качество изображения.
     * @param string $dst - Путь к новому, оптимизоваронному изображеию. Если параметр не указан, исходное изображение перезаписывается оптимизированным.
     */
    public function optimize($src, $quality, $dst = null) {
        if (!($imageSize = $this->getImageSize($src))) {
            return false;
        }

        if (!empty($dst)) {
            $pathinfo = pathinfo($dst);
            $old = umask(0); // Full Permissions
            @mkdir($pathinfo['dirname'], 0777, true);
            umask($old); // Full Permissions
        } else {
            $dst = $src;
        }

        $imageType = $imageSize[2];

        switch ($imageType) {
            case IMAGETYPE_JPEG:
                $srcImg = imagecreatefromjpeg($src);
                imagejpeg($srcImg, $dst, $quality);
                break;
            case IMAGETYPE_PNG:
                $srcImg = imagecreatefrompng($src);
                $pngQuality = (100 - $quality) / 100;
                $pngQuality = $pngQuality > 9 ? 9 : $pngQuality;
                imagepng($srcImg, $dst, $pngQuality);
                break;
            case IMAGETYPE_GIF:
                $srcImg = imagecreatefromgif($src);
                imagegif($srcImg, $dst);
                break;
            default: return false;
        }

        return true;
    }

    public static function getImageDimension($file){
    	if(is_file($file)) {
			$size = getimagesize($file);
			$sizes['w'] = $size[0];
			$sizes['h'] = $size[1];
		}else{
			$sizes['w'] = 0;
			$sizes['h'] = 0;
		}
        return $sizes;
    }

    public static function replaceExtensionToPng($file){
        $extension = mb_strtolower(pathinfo($file, PATHINFO_EXTENSION));
        return str_replace($extension,'png',$file);
    }

    public static function getExtension($file){
        $extension = mb_strtolower(pathinfo($file, PATHINFO_EXTENSION));
        return $extension;
    }

    public static function getCropPrefix($config){
        return isset($config['crop']['prefix']) ? $config['crop']['prefix'] : 'crop_';
    }

    public function getUploadErrors($file,$config){
        $errors = [];
        $size = self::getImageDimension($file['tmp_name']);
		$file_extension = mb_strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
		if(isset($config['avalaibleExtension']) && !empty($config['avalaibleExtension'])){
			if(!in_array($file_extension,$config['avalaibleExtension'])){
				$errors[] = __da('Формат файла неверный. Список доступных для загрузки: ').implode(' , ',$config['avalaibleExtension']);
			}
		}
        if(isset($config['upload'])){
            if(isset($config['upload']['maxWidth']) && $size['w'] > $config['upload']['maxWidth']){
                $errors[] = __da('Ширина картинки должна быть меньше ').$config['upload']['maxWidth'];
            }
            if(isset($config['upload']['maxHeight']) && $size['h'] > $config['upload']['maxHeight']){
                $errors[] = __da('Высота картинки должна быть меньше ').$config['upload']['maxHeight'];
            }
            if(isset($config['upload']['minWidth']) && $size['w'] < $config['upload']['minWidth']){
                $errors[] = __da('Ширина картинки должна быть больше ').$config['upload']['minWidth'];
            }
            if(isset($config['upload']['minHeight']) && $size['h'] < $config['upload']['minHeight']){
                $errors[] = __da('Высота картинки должна быть больше ').$config['upload']['minHeight'];
            }
            if(isset($config['upload']['maxSize'])){
                $maxSize = preg_replace("/[^0-9]/", "",$config['upload']['maxSize'])* 1024 * 1024;
                $imageSize = $this->getSize($file['tmp_name']);
                if($maxSize < $imageSize){
                    $errors[] = __da('Размер загружаемой картинки должен быть меньше чем ').$config['upload']['maxSize'].' MB';
                }
            }
        }
        return $errors;
    }

	public function getFileErrors($file,$config){
		$errors = [];
		$file_extension = mb_strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
		if(isset($config['avalaibleExtension']) && !empty($config['avalaibleExtension'])){
			if(!in_array($file_extension,$config['avalaibleExtension'])){
				$errors[] = __da('Формат файла неверный. Список доступных для загрузки: ').implode(' , ',$config['avalaibleExtension']);
			}
		}
		return $errors;
	}

	public function reArrayFiles(&$file_post) {

		$file_ary = array();
		$file_count = count($file_post['name']);
		$file_keys = array_keys($file_post);

		for ($i=0; $i<$file_count; $i++) {
			foreach ($file_keys as $key) {
				$file_ary[$i][$key] = $file_post[$key][$i];
			}
		}

		return $file_ary;
	}
}
