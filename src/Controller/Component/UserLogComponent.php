<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Event\EventDispatcherTrait;
use Cake\ORM\TableRegistry;

/**
 * User log component
 */
class UserLogComponent extends Component
{
	use EventDispatcherTrait;
	/**
	 * Request object
	 *
	 * @var \Cake\Http\ServerRequest
	 */
	public $request;

	/**
	 * Response object
	 *
	 * @var \Cake\Network\Response
	 */
	public $response;

	/**
	 * Instance of the Session object
	 *
	 * @var \Cake\Network\Session
	 * @deprecated 3.1.0 Will be removed in 4.0
	 */
	public $session;

	/**
	 * Controller actions for which user validation is not required.
	 *
	 * @var array
	 * @see \Cake\Controller\Component\UserLogComponent::allow()
	 */
	public $allowedActions = [];

	/**
	 * TableRegistry
	 * @var \Cake\ORM\TableRegistry
	 */
	private $_userLog;

	private $_controller;

	public function initialize(array $config)
	{
		$controller = $this->_controller = $this->_registry->getController();
		$this->eventManager($controller->eventManager());
		$this->response =& $controller->response;
		$this->session = $controller->request->session();
		$this->_userLog = TableRegistry::get('UserLog');
	}

	/**
	 *
	 */
	public function newLog(){
		$action = $this->_controller->request->getParam('action');
		if($this->session->check('Auth.User.id') && in_array($action,$this->allowedActions)){
			$user_log = $this->_userLog->newEntity();
			$user_log->user_id = $this->session->read('Auth.User.id');
			$user_log->module_id = dev_conf('Modules.'.$this->_controller->name) ? dev_conf('Modules.'.$this->_controller->name)->id : null;
			$user_log->controller = $this->_controller->name;
			$user_log->action = $action;
			$user_log->url = (string)$this->request->getUri();
			$user_log->request_url = serialize($this->request->getUri());
			$user_log->other_data = json_encode($this->request->data);
			$user_log->item_id = isset($this->request->data['id']) ? $this->request->data['id'] : (isset($this->request->params['id']) ? $this->request->params['id'] : null);
			$user_log->ip = $this->request->clientIp();
			$user_log->created = date('Y-m-d H:i:s');
			$this->_userLog->save($user_log);
		}
	}

	public function allow($actions = null)
	{
		if ($actions === null) {
			$this->allowedActions = ['delete'];

			return;
		}
		$this->allowedActions = array_merge($this->allowedActions, (array)$actions);
	}
}