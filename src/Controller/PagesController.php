<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Cache\Cache;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{


    public function index() {

        $this->loadModel('Pages');
        $pages = $this->Pages->find('active')->order(['position' => 'ASC'])->all()->toArray();
        $this->set('pages',$pages);
        $this->loadModel('Gallery');
        $sliders= $this->Gallery->find()
            ->where(['Gallery.published' => 1])
            ->order(['Gallery.position' => 'desc'])
            ->all()->toArray();

        $this->set(compact(
            'sliders'
        ));
        $this->render('home');
    }

    public function view($slug)
    {
        $this->getSetPage($slug);
    }


    public function contact($slug = null)
    {
        $this->getSetPage($slug);
        $this->loadModel('Pages');


        $pages = $this->Pages->find()->all()->toArray();
        $this->loadModel('Shops');
        $shops= $this->Shops->find()
            ->where(['Shops.published' => 1])
            ->order(['Shops.position' => 'desc'])
            ->all()->toArray();

        $this->set(compact(
            'shops'
        ));

        $this->set(compact('pages'));
    }

    public function awards($slug = null)
    {
        $this->loadModel('Awards');
        $this->getSetPage($slug);
        $awards= $this->Awards->find()
            ->where(['Awards.published' => 1])
            ->order(['Awards.year' => 'desc'])
            ->all();
        $this->set(compact('awards'));
        $this->render('awards');
    }

    public function wines()
    {
        $this->loadModel('Pages');
        $pages = $this->Pages->find('active')->order(['position' => 'ASC'])->all()->toArray();
        $this->set('pages',$pages);

        $this->loadModel('Wines');
        $wines= $this->Wines->find()
            ->where(['Wines.published' => 1])
            ->order(['Wines.position' => 'desc'])
            ->all()->toArray();

        $this->set(compact(
            'wines'
        ));
        $this->render('wines');
    }


    public function winery($slug = null)
    {
        $this->getSetPage($slug);
        $this->loadModel('Pages');
        $this->loadModel('Domains');


        $pages = $this->Pages->find()->all()->toArray();
        $pages = $this->Pages->find('active')->order(['position' => 'ASC'])->all()->toArray();
        $this->set('pages',$pages);
        $this->loadModel('Domains');
        $domains= $this->Domains->find()
            ->where(['Domains.published' => 1])
            ->order(['Domains.position' => 'desc'])
            ->all()->toArray();

        $this->set(compact(
            'domains'
        ));
        $this->set(compact('pages'));
        $this->render('winery');


    }


    public function display()
    {

        $this->loadModel('Pages');
        $pages = $this->Pages->find('active')->order(['position' => 'ASC'])->all()->toArray();
        $this->set('pages',$pages);
		$this->render('home');
    }

}
