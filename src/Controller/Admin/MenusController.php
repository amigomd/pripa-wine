<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;
use Cake\Validation\Validator;
use Cake\Utility\Text;

/**
 * Menus Controller
 *
 * @property \App\Model\Table\MenusTable $Menus
 */
class MenusController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $menuParents = ['0' =>'-----'] + $this->Menus->find('list',[
                'keyField'=>'id',
                'valueField'=>'title'
            ])->where(['parent_id IS' => NULL])->all()->toArray();
        $this->set(compact('menuParents'));
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */

    public function index()
    {
        parent::index();
        $menu = $this->Menus->find('threaded')
            ->where(["category_id"=>isset($this->request->query['type']) ? $this->request->query['type'] : 0])
            ->contain('Pages')
            ->order("Menus.position ASC")->all();
        $this->set(compact('menu'));
    }


    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $menu = $this->Menus->newEntity();
        if ($this->request->is('post')) {
            $data = $this->patch($this->request->getData());
            $menu = $this->Menus->patchEntity($menu, $data);
            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('The menu has been saved.'));
                $this->formRedirect();
            }
            $this->Flash->error(__('The menu could not be saved. Please, try again.'));
        }


        $pages = [null=>'-----'] + $this->Pages->find('list',[
                'keyField'=>'id',
                'valueField'=>'title'
            ])->all()->toArray();

        $this->set(compact('menu','pages'));
        $this->render('form');
    }

    /**
     * Edit method
     *
     * @param string|null $id Menu id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $menu = $this->Menus->find('translations')->where(['Menus.id'=>$id])->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->patch($this->request->getData());
            $menu = $this->Menus->patchEntity($menu,$data);
            if ($this->Menus->save($menu)) {
                $this->Flash->success(__('The menu has been saved.'));

                $this->formRedirect();
            }
            $this->Flash->error(__('The menu could not be saved. Please, try again.'));
        }

        $pages = [null=>'-----'] + $this->Pages->find('list',[
                'keyField'=>'id',
                'valueField'=>'title'
            ])->all()->toArray();

        $this->set(compact('menu','pages'));
        $this->render('form');
    }

    /**
     * Save with other params
     * @param $data
     */
    private function patch($data){
        //parent
        $parent = (int)$data['parent'];
        if($parent > 0){
            $data['parent_id'] = $parent;
        }
        return $data;
    }

}
