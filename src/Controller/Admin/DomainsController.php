<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;

class DomainsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        parent::index();
        $this->set('title_for_layout', 'Domains');
        $this->loadModel('Domains');
        $this->paginate['order']['Domains.id'] = 'desc';
        $domains = $this->paginate($this->Domains);
        $this->set(compact('domains'));
    }

    public function add() {
        $this->loadModel('Domains');
        $this->set('title_for_layout', 'Domains : Add');
        $page = $this->Domains->newEntity($this->request->data);
        $this->set(compact('page'));
        $this->formValidation(false);
    }

    public function edit($id = NULL) {
        $this->loadModel('Domains');

        $page = $this->Domains
            ->find('translations')
            ->where(['Domains.id'=>$id])
            ->first();

        $this->set('title_for_layout', 'Domains : '.$page->full_name);
        if (empty($page)) {
            throw new NotFoundException('Could not find that page.');
        } else {
            $this->set(compact('page'));
        }
        $this->formValidation(false);
    }

}
