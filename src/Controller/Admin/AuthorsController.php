<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;


/**
 * News Controller
 *
 * @property \App\Model\Table\NewsTable $News
 */
class AuthorsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        parent::index();
        $this->set('title_for_layout', 'Authors');
        $this->loadModel('Authors');
        $this->paginate['order']['Authors.id'] = 'desc';
        $authors = $this->paginate($this->Authors);
        $this->set(compact('authors'));
    }

    public function add() {
        $this->loadModel('Authors');
        $this->set('title_for_layout', 'Authors : Add');
        $page = $this->Authors->newEntity($this->request->data);
        $this->set(compact('page'));
        $this->formValidation(false);
    }

    public function edit($id = NULL) {
        $this->loadModel('Authors');

        $page = $this->Authors
            ->find('translations')
            ->where(['Authors.id'=>$id])
            ->first();

        $this->set('title_for_layout', 'Authors : '.$page->full_name);
        if (empty($page)) {
            throw new NotFoundException('Could not find that page.');
        } else {
            $this->set(compact('page'));
        }
        $this->formValidation(false);
    }
}
