<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;
use Cake\Validation\Validator;
use Cake\Utility\Text;

class TestsController extends AppController {

    public function initialize()
    {
        $this->loadModel('Tests');
        parent::initialize();
    }

    public function index() {
        parent::index();
        $this->set('title_for_layout', 'Tests');
        $this->paginate['order']['Tests.id'] = 'desc';
        $tests = $this->paginate($this->Tests);
        $this->set(compact('tests'));

    }

    public function edit($id = NULL) {
        if(!defined('LANG')){
            define('LANG','rom');
        }
        $page = $this->Tests->find()->where(['Tests.id'=>$id])->contain(['Categories'])->first();
        $this->set('title_for_layout', 'Tests : '.$page->title);
        if (empty($page)) {
            throw new NotFoundException('Could not find that page.');
        } else {
            $this->set(compact('page'));
        }

        $questions = json_decode($page->questions);
        $questionList = [];
        if(isset($questions->{'category-'.$page->category_id})){
            $questionList = json_decode($questions->{'category-'.$page->category_id});
        }

        $questions = [];
        $questionList = (array) $questionList;
        if(count($questionList)){
            $this->loadModel('Questions');
            $questions = $this->Questions->find();
            $questions->find('active');
            $questions->where([$this->Questions->translationField('title').' !=' => ""]);
            $questions = $questions->andWhere(['Questions.id IN' => array_values($questionList)])->all();
        }
        $this->set('questions',$questions);

        $this->formValidation(false);
    }

}
