<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;

class GalleryController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        parent::index();
        $this->set('title_for_layout', 'Gallery');
        $this->loadModel('Gallery');
        $this->paginate['order']['Gallery.id'] = 'desc';
        $gallery = $this->paginate($this->Gallery);
        $this->set(compact('gallery'));
    }

    public function add() {
        $this->loadModel('Gallery');
        $this->set('title_for_layout', 'Gallery : Add');
        $page = $this->Gallery->newEntity($this->request->data);
        $this->set(compact('page'));
        $this->formValidation(false);
    }

    public function edit($id = NULL) {
        $this->loadModel('Gallery');

        $page = $this->Gallery
            ->find('translations')
            ->where(['Gallery.id'=>$id])
            ->first();

        $this->set('title_for_layout', 'Gallery : '.$page->full_name);
        if (empty($page)) {
            throw new NotFoundException('Could not find that page.');
        } else {
            $this->set(compact('page'));
        }
        $this->formValidation(false);
    }

}
