<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;

class AwardsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        parent::index();
        $this->set('title_for_layout', 'Awards');
        $this->loadModel('Awards');
        $this->paginate['order']['Awards.id'] = 'desc';
        $awards= $this->paginate($this->Awards);
        $this->set(compact('awards'));
    }

    public function add() {
        $this->loadModel('Awards');
        $this->set('title_for_layout', 'Awards : Add');
        $page = $this->Awards->newEntity($this->request->data);
        $this->set(compact('page'));
        $this->formValidation(false);
    }

    public function edit($id = NULL) {
        $this->loadModel('Awards');
        $page = $this->Awards
            ->find('translations')
            ->where(['Awards.id'=>$id])
            ->first();

        $this->set('title_for_layout', 'Awards : '.$page->title);
        if (empty($page)) {
            throw new NotFoundException('Could not find that page.');
        } else {
            $this->set(compact('page'));
        }
        $this->formValidation(false);
    }

}
