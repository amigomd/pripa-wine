<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;
use Cake\Validation\Validator;
use Cake\Utility\Text;

class PagesController extends AppController {

    public function index() {

        parent::index();
        $this->set('title_for_layout', 'Pages');
        $this->loadModel('Pages');
        $this->paginate['order']['Pages.position'] = 'asc';
        $pages = $this->paginate($this->Pages);
        foreach ($pages as $page){
            $a = $page;
        }
        $this->set(compact('pages'));

    }

    public function add() {
        $this->set('title_for_layout', 'Page : Add');
        $this->loadModel('Pages');
        $page = $this->Pages->newEntity($this->request->data);

        $this->set(compact('page'));
        $this->formValidation(false);
    }

    public function edit($id = NULL) {
        $this->loadModel('Pages');
        $page = $this->Pages->find('translations')->where(['Pages.id'=>$id])->first();
        $this->set('title_for_layout', 'Page : '.$page->title);
        if (empty($page)) {
            throw new NotFoundException('Could not find that page.');
        } else {
            $this->set(compact('page'));
        }
        $this->formValidation(false);
    }

}
