<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;

class AwardsTypeController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        parent::index();
        $this->set('title_for_layout', 'AwardsType');
        $this->loadModel('AwardsType');
        $this->paginate['order']['AwardsType.id'] = 'desc';
        $awardstype= $this->paginate($this->AwardsType);
        $this->set(compact('awardstype'));
    }

    public function add() {
        $this->loadModel('AwardsType');
        $this->set('title_for_layout', 'AwardsType : Add');
        $page = $this->AwardsType->newEntity($this->request->data);
        $this->set(compact('page'));
        $this->formValidation(false);
    }

    public function edit($id = NULL) {
        $this->loadModel('AwardsType');
        $page = $this->AwardsType
            ->find('translations')
            ->where(['AwardsType.id'=>$id])
            ->first();

        $this->set('title_for_layout', 'AwardsType : '.$page->title);
        if (empty($page)) {
            throw new NotFoundException('Could not find that page.');
        } else {
            $this->set(compact('page'));
        }
        $this->formValidation(false);
    }

}
