<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;

class ShopsController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        parent::index();
        $this->set('title_for_layout', 'Shops');
        $this->loadModel('Shops');
        $this->paginate['order']['Shops.position'] = 'asc';
        $shops = $this->paginate($this->Shops);
        $this->set(compact('shops'));

    }

    public function add() {
        $this->loadModel('Shops');
        $this->set('title_for_layout', 'Shops : Add');
        $page = $this->Shops->newEntity($this->request->data);
        $this->set(compact('page'));
        $this->formValidation(false);
    }

    public function edit($id = NULL) {
        $this->loadModel('Shops');

        $page = $this->Shops
            ->find('translations')
            ->where(['Shops.id'=>$id])
            ->first();

        $this->set('title_for_layout', 'Shops : '.$page->full_name);
        if (empty($page)) {
            throw new NotFoundException('Could not find that page.');
        } else {
            $this->set(compact('page'));
        }
        $this->formValidation(false);
    }

}
