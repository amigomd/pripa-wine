<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;
use Cake\Validation\Validator;
use Cake\Utility\Text;


class WinesController extends AppController {

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index() {
        parent::index();
        $this->set('title_for_layout', 'Wines');
        $this->loadModel('Wines');
        $this->paginate['order']['Wines.id'] = 'desc';
        $wines = $this->paginate($this->Wines);
        $this->set(compact('wines'));
    }

    public function add() {
        $this->loadModel('Wines');
        $this->set('title_for_layout', 'Wines : Add');
        $page = $this->Wines->newEntity($this->request->data);
        $this->set(compact('page'));
        $this->formValidation(false);
    }

    public function edit($id = NULL) {
        $this->loadModel('Wines');
        $page = $this->Wines
            ->find('translations')
            ->where(['Wines.id'=>$id])
            ->first();

        $this->set('title_for_layout', 'Wines : '.$page->full_name);
        if (empty($page)) {
            throw new NotFoundException('Could not find that page.');
        } else {
            $this->set(compact('page'));
        }
        $this->formValidation(false);
    }

}
