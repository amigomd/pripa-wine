<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class CategoriesController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    public function index($slug = null)
    {
        $this->_contain = ['Subcategories'];
        $category = $this->getSetPage($slug);

        $this->loadModel('Questions');
        $questions = $this->Questions->find();
        $questions->find('active');
        $questions->where([$this->Questions->translationField('title').' !=' => ""]);

        $questions->innerJoinWith('Subcategories');
        $questions->where(['Subcategories.category_id' => $category->id]);
        $questions->group('Questions.id');
        
        $selectedQuestions = [];
        $questionsSelected = null;
        if($this->request->cookies){
            foreach ($this->request->cookies as $key => $cookie){
                if($key == 'category-'.$category->id){
                    $items = json_decode($cookie);
                    $selectedQuestions = (array) $items;
                    if(count($selectedQuestions)){
                        $questionsSelected = clone $questions;
                        $questionsSelected->andWhere(['Questions.id IN' => array_values($selectedQuestions)])->all();
                    }
                }
            }
        }

        if(count($selectedQuestions) == 0){
            $questionsSelected = clone $questions;
            $questionsSelected->andWhere(['Questions.id IN' => [0]])->all();
        }


        if($this->request->param('subcategory')){
            $slugFind = $this->Slugs->find()
                ->where(['module_id' => dev_conf('Modules')['Subcategories']->id, 'slug' => $this->request->param('subcategory'), 'language_id' => \Cake\Core\Configure::read('LanguagesData.'.LANG)->id])
                ->order('id DESC')
                ->first();
            if($slugFind) {
                $questions->where(['QuestionsSubcategories.subcategory_id' => $slugFind->foreign_key]);
            }
        }
        if($this->request->query('complexity')){
            $questions->andWhere(["Questions.complexity_id" => (int)$this->request->query('complexity')]);
        }
        if($this->request->query('type')){
            $questions->andWhere(["Questions.type" => $this->request->query('type')]);
        }

        $this->set('selectedQuestions',$selectedQuestions);
        $this->set('questionsSelected',$questionsSelected);
        $this->set('questions',$questions);
        if($this->request->is('ajax')){
            $this->viewBuilder()->layout(false);
        }
    }
    public function date($slug = null)
    {
        $this->_contain = ['Subcategories'];
        $category = $this->getSetPage($slug);
        $this->verifyQestions($category);
    }
    public function finish($slug = null)
    {
        if(!$this->request->param('hash')){
            throw new \Cake\Network\Exception\NotFoundException;
        }
        $this->loadModel('Tests');
        $test = $this->Tests->find()->where(["MD5(CONCAT('aed-best-by-amigo',Tests.id))" => $this->request->param('hash')])->first();
        if(!$test){
            throw new \Cake\Network\Exception\NotFoundException;
        }
        $answers = false;
        if($this->request->param('answers') && $this->request->param('answers') == conf('Answers.prefix')){
            $answers = true;
        }
        $this->set('answers',$answers);
        $this->set('test',$test);

        $this->_contain = ['Subcategories'];
        $category = $this->getSetPage($slug);

        if($this->request->param('reorder') && $this->request->param('reorder') == 'reorder'){
            $selectedQuestions = $test->shuffleSelectedQuestion();
            $test->questions = json_encode($selectedQuestions);
            $this->Tests->save($test);
            $selectedQuestions = json_decode($selectedQuestions['category-'.$test->category_id]);
        }else{
            $selectedQuestions = $test->selectedQuestion();
        }

        $this->loadModel('Questions');
        $questions = $this->Questions->find();
        $questions->find('active');
        $questions->where([$this->Questions->translationField('title').' !=' => ""]);
        $questions = $questions->andWhere(['Questions.id IN' => array_values($selectedQuestions)])->order('FIELD(Questions.id, '.implode(',',array_values($selectedQuestions)).')')->all();
        $this->set('questions',$questions);

        if(isset($this->request->params['_ext']) && $this->request->params['_ext'] == 'pdf') {
            $this->autoRender = false;
            $pdf = new CreateTestPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            \TCPDF_FONTS::addTTFfont(ROOT. '/vendor/tecnickcom/tcpdf/fonts/georgiab.ttf', 'georgiaIab', '');
//            $pdf->SetFont($arial, '', 12);
            $pdf->SetFont('dejavusans', '', 12);
            $pdf->SetCreator($this->request->domain());
            $pdf->SetAuthor($this->request->domain());
            $pdf->SetTitle($this->request->domain().' Test');
            $pdf->SetSubject($this->request->domain().' Test');
            $pdf->SetKeywords($this->request->domain());


            $pdf->SetMargins(5, 10, 5);

            $pdf->AddPage('P', 'A4');

            $view = new \Cake\View\View($this->request);
            $testHtml = $view->element('../Categories/pdf/finish'.($answers ? '_answers' : ''),compact('questions','category','test'));
            $pdf->writeHTML($testHtml, true, false, true, false, '');
            if($this->request->query('download') && $this->request->query('download') == 'pdf'){
                $pdf->Output('test_'.date('d.m.Y').'.pdf', 'D');
            }else{
                $pdf->Output('test_'.date('d.m.Y').'.pdf', 'I');
            }
        }
    }

    public function commentForm()
    {
        // validation
        $this->autoRender = false;
        if(!$this->request->query('alias')){
            throw new \Cake\Network\Exception\NotFoundException;
        }
        $category = $this->getSetPage($this->request->query('alias'));
        $this->verifyQestions($category);

        // json response
        $out['success'] = false;
        $this->loadModel('Tests');
        $request = $this->Tests->newEntity($this->request->data);
        $patch = $this->Tests->patchEntity($request, $this->request->data);
        if(!empty($patch->errors())){
            $msg = $this->getValidationError($patch->errors());
            $out['errors'] = $msg;
        }else{
            $request->ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'localhost';
            $request->server = json_encode($_SERVER+$this->request->data);
            $request->questions = json_encode($this->request->cookies);
            $request->category_id = $category->id;
            $this->Tests->save($request);
            $out['success'] = true;
            $out['link'] = \Cake\Routing\Router::url([
                'controller' => $this->request->getParam('controller'),
                'action' => 'finish',
                'slug' => $this->request->query('alias'),
                'hash' => md5('aed-best-by-amigo'.$request->id),
                'language' => (LANG == 'rom' ? '' : LANG)],true
            );
        }

        $this->response->type('json');
        $this->response->body(json_encode($out));
        $this->response->statusCode(200);
        $this->response->send();
        $this->response->stop();

    }

    private function verifyQestions($category){
        $items = [];
        $this->loadModel('Questions');
        $questions = $this->Questions->find();
        $questions->find('active');
        $questions->where([$this->Questions->translationField('title').' !=' => ""]);

        if($this->request->cookies){
            foreach ($this->request->cookies as $key => $cookie){
                if($key == 'category-'.$category->id){
                    $items = (array) json_decode($cookie);
                    if(count($items)){
                        $questions->andWhere(['Questions.id IN' => array_values($items)])->all();
                    }
                }
            }
        }
        if($questions->count() == 0 || count($items) == 0){
            throw new \Cake\Network\Exception\NotFoundException;
        }
        return $items;
    }
}
class CreateTestPDF extends \TCPDF {
    //Page header
    public function Header() {
        $bMargin = 10;
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(true, 0);
        // set bacground image
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();

        $this->SetMargins(5, 10, 5);
        $this->SetFooterMargin(10);
    }
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-8);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, $this->getAliasNumPage().' '.__ds('din').' '.$this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'T');
    }
}