<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
use Cake\Cache\Cache;
use Cake\Event\Event;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends CommonController
{
	/**
	 * @var User object from session
	 */
	public $authUser;
	/**
	 * @var array
	 */
	public $breadcrumbs = [];
	/**
	 * Module list
	 * @var array
	 */
	public $modules = [];
	/**
	 * @var array
	 */
	public $active_menu_ids = [];


	/**
	 * Page contain
	 * @var array
	 */
	protected $_contain;
	/**
	 * Page finder
	 * @var array
	 */
	protected $_finder;
    /**
     * Alternative page table
     * @var string
     */
    protected $_page_table;
    protected $page_title;
    protected $meta_description;
    protected $meta_keywords;

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Slugs');
        $this->loadModel('Pages');

		//Auth front user
		$this->set('authUser', $this->request->session()->read('Auth.Front'));
		$this->authUser = $this->request->session()->read('Auth.Front');

		$this->set('containerClass', 'container');

        /**
         * Set current language on site
         */
        $this->_setLanguage();

		/**
		 * Load site modules
		 */
		$pages = \Cake\ORM\TableRegistry::get('Pages');
        if (($mods = Cache::read('page_'.LANG.'_list','site')) === false) {
		    $mods = $pages->find()->where(['Pages.module !=' => '','Pages.module IS NOT' => 'NULL'])->toArray();
            Cache::write('page_'.LANG.'_list', $mods,'site');
        }
		$modules = [];
		foreach ($mods as $module){
			$modules[$module['module']] = $module;
		}
		$this->set('modules', $modules);

		/**
		 * Save back url
		 */
		if(strtolower($this->request->getParam('action')) == 'index') {
			$this->request->session()->write('BackFront.' . $this->request->getParam('controller'), $this->request->here());
		}

    }

	/**
	 * @param Event $event
	 */
	public function beforeRender(Event $event)
    {
        $this->set('active_menu_ids',$this->active_menu_ids);

        $this->loadModel('Menus');
        $this->loadModel('Pages');

        $menu = $this->Menus->find('threaded',['contain' => 'Pages'])
            ->find('active',['name'=>'Menus'])
            ->where(["category_id"=>0])
            ->order("Menus.position ASC")->all();

        $menu22 = $this->Menus->find('threaded',['contain' => 'Pages'])
            ->find('all',['name'=>'Menus'])
            ->where(["category_id"=>0])
            ->order("Menus.position ASC")->all();

        $this->set(compact(
            'menu','menu22'
        ));
    }



	/**
	 * @param bool $model
	 */
	protected function UpdateViews($model = false)
	{
		$model = $model ? $model : MODEL_NAME;
    	if(isset($this->viewVars['page']['id'])){
			$this->{$model}->find()
				->update()
				->set(['`views` = (`views` + 1)'])
				->where(['id'=>$this->viewVars['page']['id']])
				->execute();
		}
	}

	/**
	 * @param $slug
	 * @param bool $model
	 * @return null
	 */
	protected function getSetPage($slug, $model = false)
	{
        $model = $model ? $model : MODEL_NAME;

        $module_id = dev_conf('Modules')[$model]->id;
        $language_id = dev_conf('LanguagesData')[LANG]->id;
        $page = null;
        $translatedSlugs = [];

		$contain = $this->_contain ? $this->_contain : [];
        if($slug) {
            $slugFind = $this->Slugs->find()
                ->where(['module_id' => $module_id, 'slug' => $slug, 'language_id' => $language_id])
                ->order('id DESC')
                ->first();
			if($slugFind && $slugFind->status == 0){
				$redirect_slug = $this->Slugs->find()
								->where(['module_id' => $module_id,'foreign_key'=>$slugFind->foreign_key, 'language_id' => $language_id])
								->order('id DESC')
								->first();
				if($redirect_slug) {
					$redirect_url = \Cake\Routing\Router::url([
						'controller' => $this->request->getParam('controller'),
						'action'     => ACTION_NAME,
						'language'   => LANG == 'rom' ? null : LANG,
						'slug'       => $redirect_slug->slug], true
					);
					$this->redirect($redirect_url);
				}
			}
            if($slugFind) {
				$this->_page_table = $this->{$model}
					->find($this->_finder ? $this->_finder : 'all')
					->contain($contain)
					->where([$model.'.id'=>$slugFind->foreign_key]);
				$pageSql = md5(serialize($this->_page_table->clause('where')));
                if (($page = Cache::read('page_find_'.LANG.'_'.$pageSql,'site')) === false) {
                    $page = $this->_page_table->first();
                    Cache::write('page_find_'.LANG.'_'.$pageSql, $page,'site');
                }
                if($page) {
                    $translatedSlugs = $page['slug'];
                }
            }
        }else{
        	if(in_array(ACTION_NAME,['index','view'])){
				$this->_page_table = $this->{$model}->find()->where(['module'=>CONTROLLER_NAME]);
			}else{
				$this->_page_table = $this->{$model}->find()->where(['module'=>CONTROLLER_NAME.'/'.ACTION_NAME]);
			}
            $page = $this->_page_table->first();
        }
        if(!$page){
			throw new \Cake\Network\Exception\NotFoundException;
		}


		// Set image for social newtworks
		$og_image = '';
		if(isset($page->image) && $page->image){
        	if(is_file(IMAGE_PATH.CONTROLLER_NAME.DS.$page->id.DS.$page->image)){
				$og_image = IMAGE_PATH_WEB.CONTROLLER_NAME.'/'.$page->id.'/'.$page->image;
			}
		}

        $this->page_title = $this->getMetaTitle($page);
        $this->meta_description = $this->getMetaDesription($page);
        $this->meta_keywords = $this->getMetaKeywords($page);
        $this->set('page_title',$this->page_title);
        $this->set('description',$this->meta_description);
        $this->set('meta_keywords',$this->meta_keywords);
        $this->set('og_image',$og_image);

		$this->set(compact('page','translatedSlugs'));

		return $page;
    }

    private function getMetaTitle($page)
    {
        if(isset($page->meta_title) && $page->meta_title){
            return $page->meta_title;
        }elseif(isset($page->title) && $page->title){
            return $page->title;
        }
        return '';
    }

    private function getMetaKeywords($page)
    {
        if(isset($page->meta_keywords) && $page->meta_keywords){
            return $page->meta_keywords;
        }
        return '';
    }

    private function getMetaDesription($page)
    {
        if(isset($page->meta_description) && $page->meta_description){
            return $page->meta_description;
        }elseif(isset($page->description) && $page->description){
            return $page->description;
        }elseif(isset($page->content) && $page->content){
            return $page->content;
        }
    }

    private function _setLanguage()
	{
        $lang = dev_conf('Languages')[conf('Language.front')]['code'];

        if($this->request->getQuery('language')){
            $lang = $this->request->getQuery('language');
        }

        if(isset($this->request->params['language'])){
            $lang = $this->request->params['language'];
        }
        \Cake\I18n\I18n::locale($lang);
        define('LANG',$lang);
    }

}
