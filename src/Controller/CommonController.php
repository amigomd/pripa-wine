<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Cache\Cache;
use Cake\Controller\Controller;
use Cake\Event\Event;

/**
 * Common Controller
 *
 * Add common methods for site and admin in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class CommonController extends Controller
{
    protected $brandList;

	public function initialize()
	{
		parent::initialize();

		define('CONTROLLER_NAME', strtolower($this->request->getParam('controller')) );
		!defined('MODEL_NAME')  ?  define('MODEL_NAME', $this->request->getParam('controller') ) : '';
		!defined('ACTION_NAME') ? define('ACTION_NAME', strtolower($this->request->getParam('action'))) : '';
		define('IS_ADMIN', isset($this->request->params['prefix']) && $this->request->params['prefix'] == 'admin' );
	}

	public function beforeFilter(Event $event) {
        parent::beforeFilter($event);
        \Cake\Routing\Router::parseNamedParams($this->request);

        $serverProtocol = strpos(strtolower($this->request->env('SERVER_PROTOCOL')), 'https') === FALSE ? 'http' : 'https';
        define('SERVER_PROTOCOL', $serverProtocol);
		if(conf('Maintenance.status') == '1' && !$this->request->session()->read('Auth.User.id') && CONTROLLER_NAME !='users'){
			$this->viewBuilder()->layout('maintenance');
		}
    }

	public function uploadImage($alternative = array()){
		//Проверка откуда идет запрос и ограничения для пользователей сайта
		if(!IS_ADMIN){
            $output[0]['success'] = false;
            $output[0]['error'] = 'Permission denied';
            echo json_encode($output);
            die;
		}

		$this->autoRender = false;
		$output = array();
		$this->loadComponent('AmigoFile');

		// Текушее название загружаемой картинке. Передается через GET
		$current_image_name = isset($this->request->query['name']) ? $this->request->query['name'] : 'name';

		//Нормальный вид
		$multiple = dev_conf('Config.singleImage.'.MODEL_NAME.'.'.$current_image_name.'.multiple') ? true : false;
		if(!isset($alternative['files'])) {
			if($multiple){
				$countFiles = count($_FILES['photo']['name'][$current_image_name]);
				for ($i=0; $i < $countFiles; $i++) {
					$files[$i]['name']     = $_FILES['photo']['name'][$current_image_name][$i];
					$files[$i]['type']     = $_FILES['photo']['type'][$current_image_name][$i];
					$files[$i]['tmp_name'] = $_FILES['photo']['tmp_name'][$current_image_name][$i];
					$files[$i]['error']    = $_FILES['photo']['error'][$current_image_name][$i];
					$files[$i]['size']     = $_FILES['photo']['size'][$current_image_name][$i];
				}
			}else{
				foreach ($_FILES['photo'] as $key => $photo) {
					foreach ($photo as $prop => $value) {
						$files[$prop][$key] = $value;
					}
				}
				$files[0] = $files[$current_image_name];
				unset($files[$current_image_name]);
			}
		}else{
			//Альтернативный вариант. Пример: кроп.
			$files = $alternative['files'];
			$files[0] = $files[$current_image_name];
			unset($files[$current_image_name]);
		}

		//Исключения для галереи
		$gallery_path = '';
		if($current_image_name === 'gallery'){
			$gallery_path = DS.'gallery';
		}
		foreach ($files as $file_key => $file_opt) {
			//Пути к папке абсолютные и для просмотра с браузера
			if(isset($this->request->data['id']) && $this->request->data['id'] && $current_image_name != 'gallery'){
				$path = IMAGE_PATH.strtolower(MODEL_NAME).$gallery_path.DS.$this->request->data['id'];
				$short_path = IMAGE_PATH_WEB.strtolower(MODEL_NAME).$gallery_path.DS.$this->request->data['id'];
			}else{
				if(!isset($alternative['crop'])){ $_POST['random'][$current_image_name] = uniqid();}
				$path = IMAGE_PATH.strtolower(MODEL_NAME).DS.$gallery_path.DS.IMAGE_PATH_TMP.DS.$_POST['random'][$current_image_name];
				$short_path = IMAGE_PATH_WEB.strtolower(MODEL_NAME).DS.$gallery_path.DS.IMAGE_PATH_TMP.DS.$_POST['random'][$current_image_name];
				$output[$file_key]['random'] = $_POST['random'][$current_image_name];
			}

			// Данные из настроек
			$basic_conf = dev_conf('Config.singleImage.'.MODEL_NAME.'.'.$current_image_name);
			if(isset($alternative['crop'])){
				$image_conf = dev_conf('Config.singleImage.'.MODEL_NAME.'.'.$current_image_name.'.crop');
			}else{
				$image_conf = dev_conf('Config.singleImage.'.MODEL_NAME.'.'.$current_image_name);

				//Проверка картинки на правильные размеры и тд
//				$dimensionErrors = $this->AmigoFile->getUploadErrors($file_opt,$image_conf);
//				if(!empty($dimensionErrors)){
//					$output[$file_key]['success'] = false;
//					$output[$file_key]['error'] = $dimensionErrors[0];
//					echo json_encode($output);
//					die;
//				}
			}


			// Загрузка картинки на сервер
			$filename = $this->AmigoFile->upload($file_opt,$path);
			if (empty($filename)) {
				$output[$file_key]['success'] = false;
				$output[$file_key]['error'] = $this->AmigoFile->error;
			}else{
				$activeCrop = isset($image_conf['crop']['active']) && $image_conf['crop']['active'] === true ? true : false;

				// create thumbnails
				$source_image = $path . DS . $filename;
				if (isset($image_conf['thumbs']) && !empty($image_conf['thumbs'])) {
					foreach ($image_conf['thumbs'] as $dir => $thumb) {
						$destination = $path . DS . $dir;

						$createDest = new \Cake\Filesystem\Folder();
						if ($createDest->create($destination, 0777)) {
							if (!empty($thumb['width']) && empty($thumb['height'])) {
								$this->AmigoFile->resizeByWidth($source_image, $thumb['width'], $destination);
							} elseif (empty($thumb['width']) && !empty($thumb['height'])) {
								$this->AmigoFile->resizeByHeight($source_image, $thumb['height'], $destination);
							} elseif (!empty($thumb['width']) && !empty($thumb['height'])) {
								$this->AmigoFile->smartCrop($source_image, $thumb['width'], $thumb['height'], $destination);
							}
						}
					}
				}

				//set preview folder
				$preview_path = $short_path;

				if (isset($basic_conf['preview']['folder'])) {
					$preview_path = $short_path . '/' . $basic_conf['preview']['folder'];
				}
				if(!IS_ADMIN){
					if (isset($basic_conf['preview']['folder_site'])) {
						$preview_path = $short_path . '/' . $basic_conf['preview']['folder_site'];
					}
				}

				$size = getimagesize($path . '/' . $filename);
				$output[$file_key]['success'] = true;
				$output[$file_key]['path'] = $preview_path . '/';
				$output[$file_key]['filename'] = $filename;
				$output[$file_key]['width'] = $size[0];
				$output[$file_key]['height'] = $size[1];
				$output[$file_key]['uniqid'] = uniqid();

				//crop settings
				if ($activeCrop) {
					$output[$file_key]['crop'] = $activeCrop;
					$output[$file_key]['originalImage'] = '/' . $short_path . '/' . $filename;
					$output[$file_key]['aspectRatio'] = isset($image_conf['crop']['aspectRatio']) ? $image_conf['crop']['aspectRatio'] : 16 / 9;
				}
			}

		}

		echo json_encode($output);
		die;
	}

	public function cropBlobImage(){
		$this->autoRender = false;
		$this->loadComponent('AmigoFile');

		$output = array();
		if(isset($this->request->data['crop']) && isset($this->request->data['croppedImage'])){
			$cropData = $this->request->data['crop'];
			$croppedImage = $this->request->data['croppedImage'];

			$image_conf = dev_conf('Config.singleImage.'.MODEL_NAME.'.'.$this->request->query['name']);

			$prefix = $this->AmigoFile->getCropPrefix($image_conf);
			$image_name = $this->AmigoFile->replaceExtensionToPng($prefix.$cropData['filename']);

			$cropFiles[$this->request->query['name']] = $croppedImage;
			$cropFiles[$this->request->query['name']]['name'] = $image_name;
			$this->uploadImage(['files'=>$cropFiles,'crop'=>true]);
		}
	}


	protected function getValidationError($errors){
		$msg = [];
		foreach ($errors as $name => $error){
			if($name == '_translations'){
				foreach ($error as $lang_name => $lang){
					foreach ($lang as $field_name => $field) {
						$msg['_translations[' . $lang_name . ']['.$field_name.']'] = array_values($field)[0];
					}
				}
			}else{
				if(!is_array(array_values($error)[0])) {
					$msg[$name] = array_values($error)[0];
				}
			}
		}
		return $msg;
	}

	protected function getComparisonTable($id,$find = 'translations',$cache = false)
    {
        $this->loadModel('ComparisonsTable');

        if(!defined('LANG')){
            define('LANG','en');

        }
        if (($brandList = Cache::read('brand_'.LANG.'_list_'.$id,'site')) === false || !$cache) {
            $brandList = $this->ComparisonsTable->find($find)
            ->where(['comporison_id' => $id])->contain(
                [
                    'Brands' => function ($query) use ($find) {
                        return $query->find($find);
                    }
                ]
            )->group('brand_id')->order(['position' => 'ASC'])->all()
            ->toArray();
            Cache::write('brand_'.LANG.'_list_'.$id, $brandList,'site');
        }

        $this->brandList = $brandList;

        $this->set('brandList',$brandList);

        if (($options = Cache::read('comparisons_'.LANG.'_parent_list_'.$id,'site')) === false || !$cache) {
            $options = $this->ComparisonsTable->find($find)
            ->where(['comporison_id' => $id])->contain(
                [
                    'Options' => function ($query) use ($find) {
                        return $query->find($find);
                    }
                ]
            )->order(['ComparisonsTable.id' => 'ASC'])->all()->toArray();
            Cache::write('comparisons_'.LANG.'_parent_list_'.$id, $options,'site');
        }
        $optionList = [];
        foreach ($options as $values){
            $arr = $values['option'];
            if(isset($arr['id'])){
                unset($values['option']);
                $values['name'] = $values->getCorrectName($arr['type']);
                if(isset($optionList[$arr['id']])){
                    array_push($optionList[$arr['id']]['items'],$values);
                }else{
                    $arr['items'] = [$values];
                    $optionList[$arr['id']] = $arr;
                }
            }
        }
        $optionList = array_values($optionList);

        $this->set('optionList',$optionList);
    }


}
