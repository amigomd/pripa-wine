<?php $allFields = $settings['translate']['fields']+$moduleFields; ?>
<div class="row">
	<!-- Nav tabs -->
	<ul class="nav nav-tabs">
		<li class="active"><a href="#general" data-toggle="tab">Основные настройки</a></li>
		<li><a href="#translations" data-toggle="tab">Переводы</a></li>
		<li><a href="#images" data-toggle="tab">Изображения</a></li>
		<li><a href="#files" data-toggle="tab">Файлы</a></li>
		<li><a href="#faker" data-toggle="tab">Подстановка данных</a></li>
		<li><a href="#list" data-toggle="tab">Список</a></li>
		<li><a href="#edit" data-toggle="tab">Редактирование</a></li>
		<li><a href="#association" data-toggle="tab">Связи</a></li>
		<li><a href="#trash" data-toggle="tab">Корзина</a></li>
	</ul>

	<!-- Tab panes -->
	<div class="tab-content">
		<div class="tab-pane active" id="general">
			<div class="row">
				<div class="col-block col-sm-2 checkbox-block">
					<input class="form-control" type="hidden" name="<?php echo $module->name ?>[general][seo]" value="0" <?php echo $settings['general']['seo'] == false ? 'checked' : ''?>>
					<label for="general-seo">
						<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[general][seo]" value="1" id="general-seo" <?php echo $settings['general']['seo'] == true ? 'checked' : '' ?>>SEO
					</label>
				</div>
				<div class="col-block col-sm-2 checkbox-block">
					<input class="form-control" type="hidden" name="<?php echo $module->name ?>[general][slug]" value="0" <?php echo $settings['general']['slug'] == false ? 'checked' : ''?>>
					<label for="general-slug">
						<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[general][slug]" value="1" id="general-slug" <?php echo $settings['general']['slug'] == true ? 'checked' : '' ?>>Формированние ссылок
					</label>
				</div>
				<div class="col-block col-sm-4">
					<label for="tags-ids">Поля для формировании ссылок <span></span></label>
					<div class="styled-select-select2">
						<select name="<?php echo $module->name ?>[general][slugFields][]" class="select2-tags-multiple" multiple="multiple">
							<?php foreach ($allFields as $moduleField):?>
								<option value="<?php echo $moduleField ?>" <?php echo !empty($settings['general']['slugFields']) && in_array($moduleField,$settings['general']['slugFields']) ? 'selected' : ''?>><?php echo $moduleField ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>
			<?php
			echo
				$this->Html->div('row',
					$this->Form->control('name',[
						'templateVars' => [
							'class' => 'col-sm-3'
						]
					])
				)
				.
				$this->Html->div('row',
					$this->Form->control('actions._ids',[
						'type' => 'select',
						'multiple' => true,
						'class' => 'form-control',
						'templateVars' => [
							'class' => 'col-sm-3'
						]
					])
				)?>
		</div>
		<div class="tab-pane" id="translations">
			<div class="row">
				<div class="col-block col-sm-4">
					<label  for="tags-ids">Поля <span></span></label>
						<div class="styled-select-select2">
							<select name="<?php echo $module->name ?>[translate][fields][]" class="select2-tags-multiple" multiple="multiple">
								<?php foreach ($settings['translate']['fields'] as $field):?>
									<option value="<?php echo $field ?>" selected><?php echo $field ?></option>
								<?php endforeach; ?>
							</select>
						</div>
				</div>
			</div>
			<div class="row">
				<div class="col-block col-sm-2 checkbox-block">
					<input class="form-control" type="hidden" name="<?php echo $module->name ?>[translate][active]" value="0" <?php echo $settings['translate']['active'] == false ? 'checked' : ''?>>
					<label for="translate-active">
						<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[translate][active]" value="1" id="translate-active" <?php echo $settings['translate']['active'] == true ? 'checked' : '' ?>>Активировать
					</label>
				</div>
				<div class="col-block col-sm-2 checkbox-block">
					<input class="form-control" type="hidden" name="<?php echo $module->name ?>[translate][validator]" value="0" <?php echo $settings['translate']['validator'] == false ? 'checked' : ''?>>
					<label for="translate-validator">
						<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[translate][validator]" value="1" id="translate-validator" <?php echo $settings['translate']['validator'] == true ? 'checked' : '' ?>>Валидатор
					</label>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="images">
			<div class="row">
				<div class="col-block col-sm-4">
					<label for="tags-ids">Поля для картинки <span></span></label>
					<div class="styled-select-select2">
						<select name="<?php echo $module->name ?>[images][single][]" class="select2-tags-multiple" multiple="multiple">
							<?php foreach ($moduleFields as $moduleField):?>
								<option value="<?php echo $moduleField ?>" <?php echo !empty($settings['images']['single']) && in_array($moduleField,$settings['images']['single']) ? 'selected' : ''?>><?php echo $moduleField ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-block col-sm-2">
					<label for="<?php echo $module->name ?>[images][minWidth]">Минимальная ширина <span></span></label>
					<input class="form-control " type="text" name="<?php echo $module->name ?>[images][minWidth]" maxlength="255" id="name" value="<?php echo $settings['images']['minWidth'] ?>">
				</div>
				<div class="col-block col-sm-2">
					<label for="<?php echo $module->name ?>[images][maxWidth]">Максимальная ширина <span></span></label>
					<input class="form-control " type="text" name="<?php echo $module->name ?>[images][maxWidth]" maxlength="255" id="name" value="<?php echo $settings['images']['maxWidth'] ?>">
				</div>
				<div class="col-block col-sm-2">
					<label for="<?php echo $module->name ?>[images][minHeight]">Минимальная высота <span></span></label>
					<input class="form-control " type="text" name="<?php echo $module->name ?>[images][minHeight]" maxlength="255" id="name" value="<?php echo $settings['images']['minHeight'] ?>">
				</div>
				<div class="col-block col-sm-2">
					<label for="<?php echo $module->name ?>[images][maxHeight]">Максимальная высота <span></span></label>
					<input class="form-control " type="text" name="<?php echo $module->name ?>[images][maxHeight]" maxlength="255" id="name" value="<?php echo $settings['images']['maxHeight'] ?>">
				</div>
				<div class="col-block col-sm-4">
					<label  for="tags-ids">Досупные для загрузки форматы <span></span></label>
					<div class="styled-select-select2">
						<select name="<?php echo $module->name ?>[images][avalaibleExtension][]" class="select2-tags-multiple" multiple="multiple">
							<?php foreach ($settings['images']['avalaibleExtension'] as $field):?>
								<option value="<?php echo $field ?>" selected><?php echo $field ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-block col-sm-2 checkbox-block">
					<input class="form-control" type="hidden" name="<?php echo $module->name ?>[images][gallery]" value="0" <?php echo $settings['images']['gallery'] == false ? 'checked' : ''?>>
					<label for="images-gallery">
						<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[images][gallery]" value="1" id="images-gallery" <?php echo $settings['images']['gallery'] == true ? 'checked' : '' ?>>Галерея
					</label>
				</div>
			</div>
			<h1 class="text-center">Настроика изображений</h1>
			<?php $settings['images']['single']['gallery'] = 'gallery'; ?>
			<?php foreach ($settings['images']['single'] as $singleImage):?>
				<div class="row">
					<div class="col-block col-sm-1">
						<h5><?php echo $singleImage ?></h5>
					</div>
					<div class="col-block col-sm-2 checkbox-block">
						<input class="form-control" type="hidden" name="<?php echo $module->name ?>[images][setting][<?php echo $singleImage ?>][active]" value="0" <?php echo $settings['images']['setting'][$singleImage]['active'] == false ? 'checked' : ''?>>
						<label for="images-<?php echo $singleImage ?>-active">
							<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[images][setting][<?php echo $singleImage ?>][active]" value="1" id="images-<?php echo $singleImage ?>-active" <?php echo $settings['images']['setting'][$singleImage]['active'] == true ? 'checked' : '' ?>>Активна
						</label>
					</div>
				</div>
				<?php if(isset($settings['images']['setting'][$singleImage]['thumbs'])) {
					foreach ($settings['images']['setting'][$singleImage]['thumbs'] as $imageThumb):
						 echo $this->element('Modules/images/thumb',['singleImage'=>$singleImage,'item'=>$imageThumb]);
					endforeach;
				}?>
				<div class="row">
					<button type="button" class="btn btn-success" onclick="addImageThumb(this,'<?php echo $singleImage?>',false);">Добавить размеры для оригинальной фотографии</button>
				</div>
				<div class="row">
					<div class="col-block col-sm-3">
						<label for="<?php echo $module->name ?>[images][setting][<?php echo $singleImage ?>][crop][aspectRatio]">Aspect ratio <span></span></label>
						<input class="form-control " type="text" name="<?php echo $module->name ?>[images][setting][<?php echo $singleImage ?>][crop][aspectRatio]" maxlength="255" id="name" value="<?php echo $settings['images']['setting'][$singleImage]['crop']['aspectRatio'] ?>">
					</div>
					<div class="col-block col-sm-2 checkbox-block">
						<input class="form-control" type="hidden" name="<?php echo $module->name ?>[images][setting][<?php echo $singleImage ?>][crop][active]" value="0" <?php echo $settings['images']['setting'][$singleImage]['crop']['active'] == false ? 'checked' : ''?>>
						<label for="images-<?php echo $singleImage ?>-crop-active">
							<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[images][setting][<?php echo $singleImage ?>][crop][active]" value="1" id="images-<?php echo $singleImage ?>-crop-active" <?php echo $settings['images']['setting'][$singleImage]['crop']['active'] == true ? 'checked' : '' ?>>Кроп
						</label>
					</div>
				</div>
				<?php if(isset($settings['images']['setting'][$singleImage]['crop']['thumbs'])) {
					foreach ($settings['images']['setting'][$singleImage]['crop']['thumbs'] as $imageThumb):
						echo $this->element('Modules/images/thumb',['singleImage'=>$singleImage,'item'=>$imageThumb,'crop'=>true]);
					endforeach;
				}?>
				<div class="row">
					<button type="button" class="btn btn-success" onclick="addImageThumb(this,'<?php echo $singleImage?>',true);">Добавить размеры для кропа</button>
				</div>

				<hr>
			<?php endforeach; ?>
		</div>
		<div class="tab-pane" id="files">
			<div class="row">
				<div class="col-block col-sm-4">
					<label for="tags-ids">Поля для файлов <span></span></label>
					<div class="styled-select-select2">
						<select name="<?php echo $module->name ?>[files][single][]" class="select2-tags-multiple" multiple="multiple">
							<?php foreach ($moduleFields as $moduleField):?>
								<option value="<?php echo $moduleField ?>" <?php echo !empty($settings['files']['single']) && in_array($moduleField,$settings['files']['single']) ? 'selected' : ''?>><?php echo $moduleField ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-block col-sm-4">
					<label  for="tags-ids">Досупные для загрузки форматы <span></span></label>
					<div class="styled-select-select2">
						<select name="<?php echo $module->name ?>[files][avalaibleExtension][]" class="select2-tags-multiple" multiple="multiple">
							<?php foreach ($settings['files']['avalaibleExtension'] as $field):?>
								<option value="<?php echo $field ?>" selected><?php echo $field ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-block col-sm-2 checkbox-block">
					<input class="form-control" type="hidden" name="<?php echo $module->name ?>[files][multiple]" value="0" <?php echo $settings['files']['multiple'] == false ? 'checked' : ''?>>
					<label for="files-multiple">
						<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[files][multiple]" value="1" id="files-multiple" <?php echo $settings['files']['multiple'] == true ? 'checked' : '' ?>>Мультизагрузка
					</label>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="faker">
			<div class="row">
				<div class="col-block col-sm-3">
					<label for="<?php echo $module->name ?>-faker-generate">Сгенерировать {n} количество записей<span></span></label>
					<input class="form-control " type="text" name="<?php echo $module->name ?>[faker][generate]" maxlength="255" id="<?php echo $module->name ?>-faker-generate" value="0">
				</div>
			</div>
		</div>
		<div class="tab-pane" id="list">
			<div class="row">
				<div class="col-block col-sm-2 checkbox-block">
					<input class="form-control" type="hidden" name="<?php echo $module->name ?>[list][search]" value="0" <?php echo $settings['list']['search'] == false ? 'checked' : ''?>>
					<label for="list-search">
						<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[list][search]" value="1" id="list-search" <?php echo $settings['list']['search'] == true ? 'checked' : '' ?>>Поиск
					</label>
				</div>
				<div class="col-block col-sm-4">
					<label for="tags-ids">Поиск по полям <span></span></label>
					<div class="styled-select-select2">
						<select name="<?php echo $module->name ?>[list][searchFields][]" class="select2-tags-multiple" multiple="multiple">
							<?php foreach ($allFields as $moduleField):?>
								<option value="<?php echo $moduleField ?>" <?php echo !empty($settings['list']['searchFields']) && in_array($moduleField,$settings['list']['searchFields']) ? 'selected' : ''?>><?php echo $moduleField ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-block col-sm-2 checkbox-block">
					<input class="form-control" type="hidden" name="<?php echo $module->name ?>[list][sortable]" value="0" <?php echo $settings['list']['sortable'] == false ? 'checked' : ''?>>
					<label for="list-sortable">
						<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[list][sortable]" value="1" id="list-sortable" <?php echo $settings['list']['sortable'] == true ? 'checked' : '' ?>>Ручная сортировка
					</label>
				</div>
				<div class="col-block col-sm-4">
					<label for="tags-ids">Сортировка по умолчанию <span></span></label>
					<div class="styled-select-select2">
						<select name="<?php echo $module->name ?>[list][sortableField]" class="select2-tags-multiple">
							<?php foreach ($allFields as $moduleField):?>
								<option value="<?php echo $moduleField ?>" <?php echo $settings['list']['sortableField'] == $moduleField ? 'selected' : ''?>><?php echo $moduleField ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-block col-sm-4">
					<label for="tags-ids">Направление сортировки <span></span></label>
					<div class="styled-select-select2">
						<select name="<?php echo $module->name ?>[list][sortableOrder]" class="select2-tags-multiple">
							<option value="ASC" <?php echo isset($settings['list']['sortableOrder']) && $settings['list']['sortableOrder'] == 'ASC' ? 'selected' : ''?>>По возрастанию</option>
							<option value="DESC" <?php echo isset($settings['list']['sortableOrder']) &&  $settings['list']['sortableOrder'] == 'DESC' ? 'selected' : ''?>>По убыванию</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-block col-sm-2 checkbox-block">
					<input class="form-control" type="hidden" name="<?php echo $module->name ?>[list][grid]" value="0" <?php echo $settings['list']['grid'] == false ? 'checked' : ''?>>
					<label for="list-grid">
						<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[list][grid]" value="1" id="list-grid" <?php echo $settings['list']['grid'] == true ? 'checked' : '' ?>>Сетка
					</label>
				</div>
				<div class="col-block col-sm-4">
					<label for="tags-ids">Поля в списке <span></span></label>
					<div class="styled-select-select2">
						<select name="<?php echo $module->name ?>[list][listFields][]" class="select2-tags-multiple" multiple="multiple">
							<?php foreach ($allFields as $moduleField):?>
								<option value="<?php echo $moduleField ?>" <?php echo !empty($settings['list']['listFields']) && in_array($moduleField,$settings['list']['listFields']) ? 'selected' : ''?>><?php echo $moduleField ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-block col-sm-4">
					<label for="tags-ids">Доступные действия <span></span></label>
					<div class="styled-select-select2">
						<select name="<?php echo $module->name ?>[list][actions][]" class="select2-tags-multiple" multiple>
							<option value="delete" <?php echo isset($settings['list']['actions']) && in_array('delete',$settings['list']['actions']) ? 'selected' : ''?>>Удаление</option>
							<option value="edit" <?php echo isset($settings['list']['actions']) && in_array('edit',$settings['list']['actions']) ? 'selected' : ''?>>Редактирование</option>
							<option value="add" <?php echo isset($settings['list']['actions']) && in_array('add',$settings['list']['actions']) ? 'selected' : ''?>>Добавление</option>
							<option value="clone" <?php echo isset($settings['list']['actions']) && in_array('clone',$settings['list']['actions']) ? 'selected' : ''?>>Копирование</option>
							<option value="publish" <?php echo isset($settings['list']['actions']) && in_array('publish',$settings['list']['actions']) ? 'selected' : ''?>>Публикация</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="edit">
			<div class="row">
				<div class="col-block col-sm-4">
					<label for="tags-ids">Поля для редактора <span></span></label>
					<div class="styled-select-select2">
						<select name="<?php echo $module->name ?>[edit][tinyMceFields][]" class="select2-tags-multiple" multiple="multiple">
							<?php foreach ($allFields as $moduleField):?>
								<option value="<?php echo $moduleField ?>" <?php echo !empty($settings['edit']['tinyMceFields']) && in_array($moduleField,$settings['list']['tinyMceFields']) ? 'selected' : ''?>><?php echo $moduleField ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-block col-sm-2 checkbox-block">
					<input class="form-control" type="hidden" name="<?php echo $module->name ?>[edit][log]" value="0" <?php echo $settings['edit']['log'] == false ? 'checked' : ''?>>
					<label for="edit-log">
						<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[edit][log]" value="1" id="edit-log" <?php echo $settings['edit']['log'] == true ? 'checked' : '' ?>>Запись изменений
					</label>
				</div>
				<div class="col-block col-sm-2 checkbox-block">
					<input class="form-control" type="hidden" name="<?php echo $module->name ?>[edit][revert]" value="0" <?php echo $settings['edit']['revert'] == false ? 'checked' : ''?>>
					<label for="edit-revert">
						<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[edit][revert]" value="1" id="edit-revert" <?php echo $settings['edit']['revert'] == true ? 'checked' : '' ?>>Просмотр и восстановление данных
					</label>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="trash">
			<div class="row">
				<div class="col-block col-sm-2 checkbox-block">
					<input class="form-control" type="hidden" name="<?php echo $module->name ?>[trash][active]" value="0" <?php echo $settings['trash']['active'] == false ? 'checked' : ''?>>
					<label for="trash-active">
						<input type="checkbox" class="checkbox" name="<?php echo $module->name ?>[trash][active]" value="1" id="trash-active" <?php echo $settings['trash']['active'] == true ? 'checked' : '' ?>>Активировать
					</label>
				</div>
			</div>
		</div>
	</div>
</div>