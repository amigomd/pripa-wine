<?php
echo $this->element('header');

echo $this->element('index/table',[
    'model_name' => MODEL_NAME,
    'items' => ${Cake\Utility\Inflector::variable(CONTROLLER_NAME)},
    'fields' => [
        [
            'checkboxes' => true,
        ],
        [
            'name' => 'name',
            'view_name' => 'Name',
            'is_link' => true,
        ],
        [
            'actions' => ['edit', 'delete', 'publish'],
        ],
    ],
]);