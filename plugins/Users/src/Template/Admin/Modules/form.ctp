<?php

echo $this->element('header', ['itemPage' => true]);

echo $this->Html->div('content-block',
	$this->Form->create($module, array('class' => "form-page"))
	.
	$this->Html->div('row',
		$this->Form->control('name',[
			'templateVars' => [
				'class' => 'col-sm-3'
			]
		])
	)
	.
	$this->Html->div('row',
		$this->Form->control('actions._ids',[
			'type' => 'select',
			'multiple' => true,
			'class' => 'form-control',
			'templateVars' => [
				'class' => 'col-sm-3'
			]
		])
	)
	.
	$this->Html->div('row',
		$this->Form->control(__d('admin','Save'),[
			'type'=>'submit',
			'templateVars' => [
				'class' => 'btn btn-green btn-large pull-right',
				'divclass'=>'col-block col-sm-12 col-save-buttons'
			]
		])
	)
	.
	$this->Form->end()
);

