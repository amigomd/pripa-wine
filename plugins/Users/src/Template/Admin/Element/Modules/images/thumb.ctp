<div class="row">
	<div class="col-block col-sm-3">
		<label>Название папки <span></span></label>
		<input class="form-control" type="text" name="<?php echo $module->name ?>[images][setting][<?php echo isset($singleImage) ? $singleImage : '{singleImage}' ?>]<?php echo isset($crop) ? '[crop]' : ( !isset($singleImage) ? '{crop}' : '')  ?>[thumbs][<?php echo isset($item) ? $item['folder'] : '{id}' ?>][folder]" maxlength="255" value="<?php echo isset($item) ? $item['folder'] : ''?>">
	</div>
	<div class="col-block col-sm-3">
		<label>Ширина <span></span></label>
		<input class="form-control" type="text" name="<?php echo $module->name ?>[images][setting][<?php echo isset($singleImage) ? $singleImage : '{singleImage}' ?>]<?php echo isset($crop) ? '[crop]' : ( !isset($singleImage) ? '{crop}' : '')  ?>[thumbs][<?php echo isset($item) ? $item['folder'] : '{id}' ?>][width]" maxlength="255" value="<?php echo isset($item) ? $item['width'] : ''?>">
	</div>
	<div class="col-block col-sm-3">
		<label>Высота <span></span></label>
		<input class="form-control" type="text" name="<?php echo $module->name ?>[images][setting][<?php echo isset($singleImage) ? $singleImage : '{singleImage}' ?>]<?php echo isset($crop) ? '[crop]' : ( !isset($singleImage) ? '{crop}' : '')  ?>[thumbs][<?php echo isset($item) ? $item['folder'] : '{id}' ?>][height]" maxlength="255" value="<?php echo isset($item) ? $item['height'] : ''?>">
	</div>
	<div class="col-block col-sm-3">
		<label>&nbsp;<span></span></label>
		<button type="button" class="btn btn-danger" onclick="$(this).parent().parent().remove();">Удалить</button>
	</div>
</div>