<?php
    echo $this->element('header', ['itemPage' => true]);
    // simple user cannot change her role
    $selectRoles = '';
    if ($superUser) {
        $selectRoles = $this->Form->control('role_id',[
            'empty' => '---',
            'label' => __da('Role'),
            'value' => $page->role_id,
            'templateVars' => [
                'class' => 'col-sm-3'
            ]
        ]);
    }


    echo $this->Html->div('content-block',
        $this->Form->create($page, array('class' => "form-page"))
        .
        $this->Form->control('id',['type' => 'hidden'])
        .
        $this->Html->div('row',
            $this->Form->control('full_name',[
                'templateVars' => [
                    'class' => 'col-sm-3'
                ]
            ])
            .
            $this->Form->control('username',[
                'label' => __da('Email (username)'),
                'templateVars' => [
                    'class' => 'col-sm-3'
                ]
            ])
            .
            $this->Form->control('password_input',[
                'label' => __d('admin','Password'),
                'templateVars' => [
                    'class' => 'col-sm-3'
                ]
            ])
            .
            $selectRoles
        )
        .
        $this->element('index/save_actions')
        .
        $this->Form->end()
    );
?>


