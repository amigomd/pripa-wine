<?php
namespace Users\Controller\Admin;

use Cake\Network\Exception\NotFoundException;
use Cake\Network\Exception\UnauthorizedException;
use App\Controller\Admin\UsersController as BaseUsers;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends BaseUsers
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        parent::index();
        if ($this->Auth->user('id') == AMIGO_USER_ID) { // if amigo user - show all users
            $conditions = [];
        } else if ($this->Auth->user('id') == ADMIN_USER_ID) { // if admin user - show all users, except amigo
            $conditions = [ 'Users.id !=' => AMIGO_USER_ID ];
        } else { // in other cases user can access only his attributes
            $conditions = [ 'Users.id' => $this->Auth->user('id') ];
        }
        $this->paginate['conditions'] = $conditions;
		$this->paginate['contain'] = ['Roles'];

        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $page = $this->Users->newEntity();
//        if ($this->request->is('post')) {
//            $user = $this->Users->patchEntity($user, $this->request->getData());
//            if ($this->Users->save($user)) {
//                $this->Flash->success(__('The user has been saved.'));
//                $this->formRedirect();
//            }
//            $this->Flash->error(__('The user could not be saved. Please, try again.'));
//        }
        $this->loadModel('Roles');
        $roles = $this->Roles->find('list')->toArray();
        $this->set(compact('page', 'roles'));
        $this->formValidation(false);

//        $this->render('form');
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (empty($id)) {
            throw new NotFoundException;
        }

        // check if autorized
        $isNotAuthorized = false;
        if ( $this->Auth->user('id') == ADMIN_USER_ID ) {
            if ($id == AMIGO_USER_ID) { // admin user cannot update amigo user
                $isNotAuthorized = true;
            }
        }
        // user can edit only his attributes, except super user
        if (! in_array($this->Auth->user('id'), [ADMIN_USER_ID, AMIGO_USER_ID]) && $id !== $this->Auth->user('id')) {
            $isNotAuthorized = true;
        }

        if ($isNotAuthorized) {
            throw new UnauthorizedException('admin-401'); // admin-401 is used to change template
        }
        $page = $this->Users->get($id, [
            'contain' => ['Roles']
        ]);
//        if ($this->request->is(['patch', 'post', 'put'])) {
//            $user = $this->Users->patchEntity($user, $this->request->getData());
//            if ($this->Users->save($user)) {
//                $this->Flash->success(__('The user has been saved.'));
//                $this->formRedirect();
//            }
//            $this->Flash->error(__('The user could not be saved. Please, try again.'));
//        }

        $this->loadModel('Roles');
        $roles = $this->Roles->find('list')->toArray();
        $this->set(compact('page', 'roles'));
        $this->formValidation(false);
//        $this->render('form');
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
