<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;


Router::plugin(
	'Users',
	['path' => '/admin'],
	function ($routes) {
		$routes->connect('/users', ['controller' => 'users','prefix'=>'admin', 'action' => 'index' ]);
		$routes->connect('/users/add', ['controller' => 'users','prefix'=>'admin', 'action' => 'add' ]);
		$routes->connect('/users/edit/:id', ['controller'=>'users','action' => 'edit','prefix'=>'admin'], ['id' => '\d+', 'pass' => ['id']]);
		$routes->connect('/users/delete/:id', ['controller'=>'users','action' => 'delete','prefix'=>'admin'], ['id' => '\d+', 'pass' => ['id']]);

		$routes->connect('/roles', ['controller' => 'roles','prefix'=>'admin', 'action' => 'index' ]);
		$routes->connect('/roles/add', ['controller' => 'roles','prefix'=>'admin', 'action' => 'add' ]);
		$routes->connect('/roles/edit/:id', ['controller'=>'roles','action' => 'edit','prefix'=>'admin'], ['id' => '\d+', 'pass' => ['id']]);
		$routes->connect('/roles/publish/:id', ['controller'=>'roles','action' => 'publish','prefix'=>'admin'], ['id' => '\d+', 'pass' => ['id']]);
		$routes->connect('/roles/delete/:id', ['controller'=>'roles','action' => 'delete','prefix'=>'admin'], ['id' => '\d+', 'pass' => ['id']]);

		$routes->connect('/modules', ['controller' => 'modules','prefix'=>'admin', 'action' => 'index' ]);
		$routes->connect('/modules/add', ['controller' => 'modules','prefix'=>'admin', 'action' => 'add' ]);
		$routes->connect('/modules/edit/:id', ['controller'=>'modules','action' => 'edit','prefix'=>'admin'], ['id' => '\d+', 'pass' => ['id']]);
		$routes->connect('/modules/publish/:id', ['controller'=>'modules','action' => 'publish','prefix'=>'admin'], ['id' => '\d+', 'pass' => ['id']]);
		$routes->connect('/modules/delete/:id', ['controller'=>'modules','action' => 'delete','prefix'=>'admin'], ['id' => '\d+', 'pass' => ['id']]);
	}
);