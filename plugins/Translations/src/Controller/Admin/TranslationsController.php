<?php
namespace Translations\Controller\Admin;

use Cake\I18n\I18n;
use App\Controller\Admin\TranslationsController as BaseTranslations;
/**
 * Translations Controller
 *
 * @property \App\Model\Table\TranslationsTable $Translations
 */
class TranslationsController extends BaseTranslations
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
		$domain = (isset($this->request->query['domain']) && $this->request->query['domain'] == 'admin') ? 'admin' : 'site';
        $translations = $this->Translations->find('list',[
                                                'keyField' => array('language_id','id'),
                                                'valueField' => array('content'),
                                                'groupField' => array('msgid'),
												'conditions' => array('domain' => $domain),
                                                ])
                                            ->order(['language_id ASC'])->all()->toArray();

        $this->set(compact('translations', 'domain'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Translation id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = NULL)
    {
        $this->autoRender = false;
        if ($this->request->is(['patch', 'post', 'put'])) {
        	if((int)$this->request->data['id'] == -1){
				$translation = $this->Translations->newEntity();
				$translation->msgid = $this->request->data['msgid'];
				$translation->domain = $this->request->data['domain'];
				$translation->language_id = $this->request->data['language_id'];
				$translation->content = $this->request->data['text'];
				$this->Translations->save($translation);
			}else {
				$translation = $this->Translations->get((int)$this->request->data['id']);
				if ($translation) {
					$translation->content = $this->request->data['text'];
					$this->Translations->save($translation);
				}
			}
        }
    }
}
