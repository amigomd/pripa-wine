<header class="content-header clearfix">
    <h2 class="page-title"></h2>
</header>

<div class="content-controls clearfix">

    <div class="content-search">
        <input type="search" placeholder="Caută" id="translateSearch">
    </div>

    <div class="styled-select action icon-caret-down">
        <select class="dropdown" id="tran-cat-domain">
            <option value="site" <?php if($domain == 'site') echo 'selected="selected"'?>>Site</option>
            <option value="admin" <?php if($domain == 'admin') echo 'selected="selected"'?>>Admin</option>
        </select>
    </div>
</div>


<div class="content-block">
    <table class="data-table">
        <thead>
            <tr>
                <th class="table-check"></th>
                <th><?php echo __da('Source'); ?></th>
                <?php foreach (Cake\Core\Configure::read('Languages') as $language) {
                    if(!$language->admin) continue;?>
                    <th class=""><?php echo $language->title ?></th>
                <?php }?>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($translations as $msgid => $translate) {
                $ids = [];
                foreach ($translate as $tr_id => $keys){
                    $ex = explode(';',$tr_id);
                    $ids[$ex[0]] = ['id'=>$ex[1],'title'=>$keys];
                }
               ?>
                <tr data-keywords="<?php echo $msgid . ' ' . strtolower(implode(' ', $translate))?>" class="keywords" >
                    <td class="table-check"></td>
                    <td class=""><?php echo $msgid; ?></td>
                    <?php foreach (lang() as $id => $language) {
                        if(!$language->admin) continue;?>
                        <td class="">
                            <span class="translate-item"
                                  data-language-id="<?php echo $language->id;?>"
                                  data-domain="<?php echo $domain ?>"
                                  data-id="<?php echo isset($ids[$language->id]['id']) ? $ids[$language->id]['id'] : -1 ?>"
                                  data-msgid="<?php echo $msgid?>"><?php echo isset($ids[$language->id]['title']) ? $ids[$language->id]['title'] : $msgid; ?><i class="icon-pencil"></i></span></td>
                    <?php } ?>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>