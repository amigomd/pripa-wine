<?php
namespace Settings\Controller\Admin;

use App\Controller\Admin\SettingsController as BaseSettings;
use Cake\I18n\I18n;
use Cake\Core\Configure;
/**
 * Settings Controller
 *
 * @property \App\Model\Table\SettingsTable $Settings
 */
class SettingsController extends BaseSettings
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        parent::index();
        $languageTable = \Cake\ORM\TableRegistry::get('Languages');
        if ($this->request->is('post')) {
            $options = [];
            /**
             * Upload favicons
             */
            if(isset($_FILES['favicon']) && $_FILES['favicon']['tmp_name'] && $_FILES['favicon']['error'] == 0){
                $favicon_upload_name ='tmp-'.$_FILES['favicon']['name'];
                $favicon_upload_path = WWW_ROOT.'img/favicons/';

                $deleteDir = new \Cake\Filesystem\Folder($favicon_upload_path);
                if(!is_dir($favicon_upload_path) || $deleteDir->delete()) {
                    $createDir = new \Cake\Filesystem\Folder();
                    if ($createDir->create($favicon_upload_path, 0777) && move_uploaded_file($_FILES['favicon']['tmp_name'], $favicon_upload_path . $favicon_upload_name)) {
                        $source_image = $favicon_upload_path.$favicon_upload_name;
                        foreach (dev_conf('Config.favicons') as $thumb) {
                            $destination = $favicon_upload_path.$thumb['name'];
                            if (!empty($thumb['width']) && empty($thumb['height'])) {
                                $this->AmigoFile->resizeByWidth($source_image, $thumb['width'], $destination);
                            } elseif (empty($thumb['width']) && !empty($thumb['height'])) {
                                $this->AmigoFile->resizeByHeight($source_image, $thumb['height'], $destination);
                            } elseif (!empty($thumb['width']) && !empty($thumb['height'])) {
                                $this->AmigoFile->smartCrop($source_image, $thumb['width'], $thumb['height'], $destination);
                            }
                        }
                    }
                }
            }

            if (isset($_FILES['logo']) && $_FILES['logo']['tmp_name'] && $_FILES['logo']['error'] == 0) {
                $logo_upload_name = $_FILES['logo']['name'];
                $logo_upload_path = WWW_ROOT.'img/logos/';

                $deleteDir = new \Cake\Filesystem\Folder($logo_upload_path, false);
                if(!is_dir($logo_upload_path) || $deleteDir->delete()) {
                    $createDir = new \Cake\Filesystem\Folder();
                    if ($createDir->create($logo_upload_path, 0777)) {
                        move_uploaded_file($_FILES['logo']['tmp_name'], $logo_upload_path . $logo_upload_name);
                    }
                }
            }

            if (isset($_FILES['logo2']) && $_FILES['logo2']['tmp_name'] && $_FILES['logo2']['error'] == 0) {
                $logo2_upload_name = $_FILES['logo2']['name'];
                $logo2_upload_path = WWW_ROOT.'img/logos2/';

                $deleteDir = new \Cake\Filesystem\Folder($logo2_upload_path, false);
                if(!is_dir($logo2_upload_path) || $deleteDir->delete()) {
                    $createDir = new \Cake\Filesystem\Folder();
                    if ($createDir->create($logo2_upload_path, 0777)) {
                        move_uploaded_file($_FILES['logo2']['tmp_name'], $logo2_upload_path . $logo2_upload_name);
                    }
                }
            }
            if (isset($_FILES['logo3']) && $_FILES['logo3']['tmp_name'] && $_FILES['logo3']['error'] == 0) {
                $logo3_upload_name = $_FILES['logo3']['name'];
                $logo3_upload_path = WWW_ROOT.'img/logos3/';

                $deleteDir = new \Cake\Filesystem\Folder($logo3_upload_path, false);
                if(!is_dir($logo3_upload_path) || $deleteDir->delete()) {
                    $createDir = new \Cake\Filesystem\Folder();
                    if ($createDir->create($logo3_upload_path, 0777)) {
                        move_uploaded_file($_FILES['logo3']['tmp_name'], $logo3_upload_path . $logo3_upload_name);
                    }
                }
            }
            if (isset($_FILES['popup']) && $_FILES['popup']['tmp_name'] && $_FILES['popup']['error'] == 0) {
                $popup_upload_name = $_FILES['popup']['name'];
                $popup_upload_path = WWW_ROOT.'img/popup/';

                $deleteDir = new \Cake\Filesystem\Folder($popup_upload_path, false);
                if(!is_dir($popup_upload_path) || $deleteDir->delete()) {
                    $createDir = new \Cake\Filesystem\Folder();
                    if ($createDir->create($popup_upload_path, 0777)) {
                        move_uploaded_file($_FILES['popup']['tmp_name'], $popup_upload_path . $popup_upload_name);
                    }
                }
            }
            /**
             * Save setting in db
             */
            foreach ($this->request->getData() as $main => $item) {
                if(is_array($item)){
                    if ($main == 'logo') {
                        if (isset($_FILES['logo']) && $_FILES['logo']['tmp_name'] && $_FILES['logo']['error'] == 0) {
                            foreach ($item as $key => $value) {
                                $options[$main][$key] = $value;
                                $check = $this->Settings->findByColGroupAndColKey($main, $key)->first();
                                if (!$check) {
                                    $check = $this->Settings->newEntity();
                                }
                                $check->col_group = $main;
                                $check->col_key = $key;
                                $check->value = $value;
                                $this->Settings->save($check);
                            }
                        }
                    } elseif ($main == 'popup') {
                        if (isset($_FILES['popup']) && $_FILES['popup']['tmp_name'] && $_FILES['popup']['error'] == 0) {
                            foreach ($item as $key => $value) {
                                $options[$main][$key] = $value;
                                $check = $this->Settings->findByColGroupAndColKey($main, $key)->first();
                                if (!$check) {
                                    $check = $this->Settings->newEntity();
                                }
                                $check->col_group = $main;
                                $check->col_key = $key;
                                $check->value = $value;
                                $this->Settings->save($check);
                            }
                        }
                    } elseif ($main == 'logo2') {
                        if (isset($_FILES['logo2']) && $_FILES['logo2']['tmp_name'] && $_FILES['logo2']['error'] == 0) {
                            foreach ($item as $key => $value) {
                                $options[$main][$key] = $value;
                                $check = $this->Settings->findByColGroupAndColKey($main, $key)->first();
                                if (!$check) {
                                    $check = $this->Settings->newEntity();
                                }
                                $check->col_group = $main;
                                $check->col_key = $key;
                                $check->value = $value;
                                $this->Settings->save($check);
                            }
                        }
                    }
                    elseif ($main == 'logo3') {
                        if (isset($_FILES['logo3']) && $_FILES['logo3']['tmp_name'] && $_FILES['logo3']['error'] == 0) {
                            foreach ($item as $key => $value) {
                                $options[$main][$key] = $value;
                                $check = $this->Settings->findByColGroupAndColKey($main, $key)->first();
                                if (!$check) {
                                    $check = $this->Settings->newEntity();
                                }
                                $check->col_group = $main;
                                $check->col_key = $key;
                                $check->value = $value;
                                $this->Settings->save($check);
                            }
                        }
                    }else {
                        foreach ($item as $key => $value) {
                            $options[$main][$key] = $value;
                            $check = $this->Settings->findByColGroupAndColKey($main,$key)->first();
                            if(!$check){
                                $check = $this->Settings->newEntity();
                            }
                            $check->col_group = $main;
                            $check->col_key = $key;
                            $check->value = $value;
                            $this->Settings->save($check);
                        }
                    }
                }
            }
            /**
             * Clear cache
             */
            Configure::write('Settings', $options);
            Configure::store('Settings', 'default');
            \Cake\Cache\Cache::clearAll(false);

            foreach (Configure::read('Settings.Languages') as $language => $status) {
                $languageTable->query()->update()->set(['admin' => $status])->where(['code'=>$language])->execute();
            }
            foreach (Configure::read('Settings.LanguagesFront') as $language => $status) {
                $languageTable->query()->update()->set(['site' => $status])->where(['code'=>$language])->execute();
            }

            $this->Flash->set('The '.CONTROLLER_NAME.' has been updated.',
                [
                    'key' => 'admin',
                    'element' => 'admin',
                    'params' => [
                        'class' => 'success'
                    ]]
            );
            return $this->redirect(['action' => 'index']);
        }

        $setting = Configure::read('Settings');
        $language_list = $languageTable->find('list')->all()->toArray();
        $this->set(compact('setting','language_list'));
    }

    /**
     * deleteFavicon method
     * @return \Cake\Network\Response|null
     */
    public function deleteFavicon()
    {
        $this->response->type('json');

        $deleteDir = new \Cake\Filesystem\Folder(WWW_ROOT.'img/favicons/');
        if ($deleteDir->delete()) {
            $this->response->body(json_encode(['success' => true]));
            $this->response->statusCode(200);
        }else {
            $this->response->statusCode(403);
        }

        $this->response->send();
        $this->response->stop();
    }

    public function deletePopup()
    {
        $this->response->type('json');

        $deleteDir = new \Cake\Filesystem\Folder(WWW_ROOT.'img/popup/');
        if ($deleteDir->delete()) {
            $this->response->body(json_encode(['success' => true]));
            $this->response->statusCode(200);
        }else {
            $this->response->statusCode(403);
        }

        $this->response->send();
        $this->response->stop();
    }
    public function deleteLogo()
    {
        $this->response->type('json');

        $deleteDir = new \Cake\Filesystem\Folder(WWW_ROOT.'img/logos/');
        if ($deleteDir->delete()) {
            $this->response->body(json_encode(['success' => true]));
            $this->response->statusCode(200);
        }else {
            $this->response->statusCode(403);
        }

        $this->response->send();
        $this->response->stop();
    }

    public function deleteLogo2()
    {
        $this->response->type('json');

        $deleteDir = new \Cake\Filesystem\Folder(WWW_ROOT.'img/logos/');
        if ($deleteDir->delete()) {
            $this->response->body(json_encode(['success' => true]));
            $this->response->statusCode(200);
        }else {
            $this->response->statusCode(403);
        }

        $this->response->send();
        $this->response->stop();
    }

    public function clearCache(){
        $this->autoRender = false;
        $clear = \Cake\Cache\Cache::clearAll(false);
        echo json_encode(['success'=>$clear]);
        die();
    }
}
