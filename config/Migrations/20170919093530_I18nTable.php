<?php
use Migrations\AbstractMigration;

class I18nTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
	public function up()
	{
		$this->query("CREATE TABLE `i18n` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(11) NOT NULL,
  `languages_id` int(11) DEFAULT NULL,
  `field` varchar(255) NOT NULL,
  `content` text,
  `locale` char(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  KEY `languages_id` (`languages_id`),
  KEY `I18N_FIELD` (`model`,`foreign_key`,`field`),
  CONSTRAINT `i18n_ibfk_1` FOREIGN KEY (`locale`) REFERENCES `languages` (`code`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
	}
}
