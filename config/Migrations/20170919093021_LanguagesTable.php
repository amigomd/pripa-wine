<?php
use Migrations\AbstractMigration;

class LanguagesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
	public function up()
	{
		$this->query("CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `locale` varchar(10) DEFAULT NULL,
  `code` varchar(10) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `site` tinyint(1) NOT NULL DEFAULT '0',
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO `languages` (`id`, `locale`, `code`, `title`, `site`, `admin`, `status`)
VALUES
	(1, 'ro_RO', 'rom', 'Rom', 1, 1, 1),
	(2, 'ru_RU', 'rus', 'Rus', 1, 0, 1);
");
	}
}
