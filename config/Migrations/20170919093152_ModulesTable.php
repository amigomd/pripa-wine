<?php
use Migrations\AbstractMigration;

class ModulesTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
	public function up()
	{
		$this->query("CREATE TABLE `modules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sluggable` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
INSERT INTO `modules` (`id`, `name`, `sluggable`)
VALUES
	(1, 'Users', 0),
	(2, 'Translations', 0),
	(3, 'Pages', 1),
	(4, 'Menus', 0),
	(5, 'Settings', 0);
");
	}
}
