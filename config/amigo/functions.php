<?php
if (!function_exists('conf')) {
    function conf($value){
        return \Cake\Core\Configure::read('Settings.'.$value);
    }
}

if (!function_exists('dev_conf')) {
    function dev_conf($value){
        return \Cake\Core\Configure::read($value);
    }
}

if (!function_exists('lang')) {
    function lang($value = false){
        return $value === false ? \Cake\Core\Configure::read('Languages') : \Cake\Core\Configure::read('Languages.'.$value);
    }
}

if (!function_exists('__dm')) {
    function __dm($domain, $msg, ...$args)
    {
        $translate = Cake\I18n\I18n::translator($domain)->getPackage()->getMessages();
        $message = __d($domain, $msg, ...$args);
        if(!isset($translate[$msg])){
            foreach (\Cake\Core\Configure::read('Languages') as $language) {
                $model = Cake\ORM\TableRegistry::get('Translations')->find()->where(['domain'=>$domain,'msgid'=>$msg,'language_id'=>$language['id']])->first();
                if(!$model){
                    $translation = Cake\ORM\TableRegistry::get('Translations')->newEntity();
                    $translation->language_id = $language->id;
                    $translation->domain = $domain;
                    $translation->msgid = $msg;
                    $translation->content = $msg;
                    Cake\ORM\TableRegistry::get('Translations')->save($translation);
                }
            }
        }
        return $message;
    }
}

if (!function_exists('__da')) {
    function __da($msg, ...$args)
    {
        $domain = 'admin';
        $translate = Cake\I18n\I18n::translator($domain)->getPackage()->getMessages();
        $message = __d($domain, $msg, ...$args);
        if(!isset($translate[$msg])){
            foreach (\Cake\Core\Configure::read('Languages') as $language) {
                $model = Cake\ORM\TableRegistry::get('Translations')->find()->where(['domain'=>$domain,'msgid'=>$msg,'language_id'=>$language['id']])->first();
                if(!$model){
                    $translation = Cake\ORM\TableRegistry::get('Translations')->newEntity();
                    $translation->language_id = $language->id;
                    $translation->domain = $domain;
                    $translation->msgid = $msg;
                    $translation->content = $msg;
                    Cake\ORM\TableRegistry::get('Translations')->save($translation);
                }
            }
        }
        return $message;
    }
}

if (!function_exists('__ds')) {
    function __ds($msg, ...$args)
    {
        $lang_id = defined('LANG') ? dev_conf('LanguagesData')[LANG]->id : null;
        if (is_null($lang_id)) {
            $lang_id = 1;
        }
        $domain = 'site';
        $translate = Cake\I18n\I18n::translator($domain)->getPackage()->getMessages();
        $message = __d($domain, $msg, ...$args);
        if(!isset($translate[$msg])){
            foreach (\Cake\Core\Configure::read('Languages') as $language) {
                $model = Cake\ORM\TableRegistry::get('Translations')->find()->where(['domain'=>$domain,'msgid'=>$msg,'language_id'=>$language['id']])->first();
                if(!$model){
                    $translation = Cake\ORM\TableRegistry::get('Translations')->newEntity();
                    $translation->language_id = $language->id;
                    $translation->domain = $domain;
                    $translation->msgid = $msg;
                    $translation->content = $msg;
                    Cake\ORM\TableRegistry::get('Translations')->save($translation);
                }
            }
        }
        return $message;
    }
}

if (!function_exists('nested_recursive_menu')) {
    function nested_recursive_menu($parent,$item,$first = false){
        $html = '';

        $element = $first ? $parent : $parent->children;
        if($element){
            $html .= $first ? '<ol class="sortable">' : '<ol>';
            foreach ($element as $child){
                $html .=  '<li id="ch-'.$child->id.'" '.(!$child->published ? 'style="background-color: #ffe8e5"' : '').' class="'.(!$child->published ? "menu-unpublished" : "").'">';
                $html .= '<div>';
                if($child->type == 2){
                    $html .= '<a href="/admin/menus/edit/' . $child->id . '" class="nested-title">'.($child->title == '' ? '(---)' : $child->title).'</a>';
                }else{
                    $html .= '<a href="/admin/menus/edit/' . $child->id . '" class="nested-title">'.($child->page ? $child->page->title  : ($child->title ? $child->title  : '(---)')).'</a>';
                }
                $html .= '<span class="table-actions">';
                $html .= $item->element('index/actions', [
                    'item' => $child->id,
                    'actions' => ['edit' => true, 'delete'  => true, 'publish' => true],
                ] );
                $html .= '</span>';
                $html .= '</div>';
                $html .= nested_recursive_menu($child,$item);
                $html .=  '</li>';
            }
            $html .= '</ol>';
        }
        return $html;
    }
}

//
if (!function_exists('nested_recursive_menu_footer')) {
    function nested_recursive_menu_footer($parent,$item,$first = false){
        $html = '';
        $element = $first ? $parent : $parent->children;
        if($element){
            foreach ($element as $child){
//				$title = $child->title ? $child->title : ($child->page ? $child->page->title : '');
                $title = $child->page && $child->page->title ? $child->page->title : $child->title;
                if($child->type == 1 && $child->page){
                    if($child->page->module) {
                        $ex = explode('/', $child->page->module);
                        $href = '';
                        if (!empty($ex)) {
                            $url = [];
                            $url['controller'] = $ex[0];
                            $url['language'] = LANG;
                            $url['action'] = 'index';
                            if (count($ex) == 2) {
                                $url['action'] = $ex[1];
                                if (isset($child->page->slug[LANG])) {
                                    $url['slug'] = $child->page->slug[LANG];
                                }
                            }
                            $href = $item->Url->build($url);
                        }
                    }else{
                        $url = [];
                        $url['controller'] = 'Pages';
                        $url['action'] = 'view';
                        $url['language'] = LANG;
                        $url['slug'] = $child->page->slug[LANG];
                        $href = $item->Url->build($url);
                    }
                    $html .= '<a href="'.$href.'">'.$title.'</a>';
                }elseif($child->type == 1){
                    $html .= '<a href="'.$child->external_link.'" target="_blank">'.$title.'</a>';
                }else{
                    $html .= '<a href="/" target="_blank">'.$title.'</a>';
                }
                $html .= nested_recursive_menu_footer($child,$item);
                $html .=  '</li>';

            }
        }
        return $html;
    }
}

//
if (!function_exists('nested_recursive_menu_home')) {
    function nested_recursive_menu_home($parent,$item,$first = false){
        $html = '';
        $active_menu_ids = isset($item->viewVars['active_menu_ids']) ? $item->viewVars['active_menu_ids'] : [];
        //$active_menu_ids = [14,15,16,17,18,19];
        $element = $first ? $parent : $parent->children;
        if($element){

            $html .= !$first ? ($parent->category_id == 0 ?'<ul class="header__main-menu">' : '<ul>') : ($element->first()['category_id'] == 0 ?'<ul class="header__main-menu">' : '<ul>');
            foreach ($element as $child){

                $html .=  '<li id="ch-'.$child->id.'" '.(!$child->published ? 'class ="grey" title="Comming soon"' : '').' class="'.(!$child->published ? "menu-unpublished" : "").'">';

                // var_dump($child);
                if($child->type == 1 && $child->page){
                    if($child->page->module) {
                        $ex = explode('/', $child->page->module);
                        $href = '';
                        if (!empty($ex)) {
                            $url = [];
                            $url['controller'] = $ex[0];
                            $url['language'] = LANG == 'rom' ? null : LANG;
                            $url['action'] = 'index';
                            if (count($ex) == 2) {
                                $url['action'] = $ex[1];
                                if (isset($child->page->slug[LANG])) {
                                    $url['slug'] = $child->page->slug[LANG];
                                }
                            }
                            $href = $item->Url->build($url);
                        }
                    }else{
                        $url = [];
                        $url['controller'] = 'Pages';
                        $url['action'] = 'view';
                        $url['language'] = LANG == 'rom' ? null : LANG;
                        $url['slug'] = isset($child->page->slug[LANG]) ? $child->page->slug[LANG] : '';
                        $href = $item->Url->build($url);
                    }

                    $class = '';
                    if (ACTION_NAME != 'index' && ACTION_NAME != 'view') {
                        if (strtolower($url['controller']) == CONTROLLER_NAME && $url['action'] == ACTION_NAME) {
                            $class = ' class="current"';
                        }
                    } else {
                        if (count($ex) == 2) {
                            if (strtolower($url['controller']) == CONTROLLER_NAME && $url['action'] == ACTION_NAME) {
                                $class = ' class="current"';
                            }
                        } else {
                            if (CONTROLLER_NAME == 'portfolios' && strtolower($url['controller']) == CONTROLLER_NAME && (ACTION_NAME == 'index' || ACTION_NAME == 'view')) {
                                $class = ' class="current"';
                            } elseif (strtolower($url['controller']) == CONTROLLER_NAME) {
                                $class = ' class="current"';
                            }
                        }
                    }

                    $current_a_class = in_array($child->page_id,$active_menu_ids) ? 'class="current"' : ' ' ;
                   //$html .=  '<li id="ch-'.$child->id.'" '.(!$child->published ? 'class ="grey"' : '').' class="'.(!$child->published ? "menu-unpublished" : "").'">';
                 // $html .= (!$child->published) ? $child->getTitle() : '<a href="'.$href.'" '.$class.'>'.$child->getTitle().'</a>';
                      $html .= '<a href="'.$href.'" '.$class.'>'.$child->getTitle().'</a>';
                }elseif($child->type == 2){
                    //$html .= '<a href="'.$child->external_link.'" target="_blank">'.$child->getTitle().'</a>';
                }else{
                  $html .= $child->getTitle();
                }
                $html .= nested_recursive_menu_home($child,$item);
                $html .=  '</li>';
            }
            $html .= '</ul>';





        }
        return $html;
    }
}


if (!function_exists('nested_recursive_menu_hom_ft')) {
    function nested_recursive_menu_hom_ft($parent,$item,$first = false){
        $html = '';
        $active_menu_ids = isset($item->viewVars['active_menu_ids']) ? $item->viewVars['active_menu_ids'] : [];
        //$active_menu_ids = [14,15,16,17,18,19];
        $element = $first ? $parent : $parent->children;
        if($element){

            $html .= !$first ? ($parent->category_id == 0 ?'<ul class="footer-menu">' : '<ul>') : ($element->first()['category_id'] == 0 ?'<ul class="footer-menu">' : '<ul>');
            foreach ($element as $child){

                $html .=  '<li id="ch-'.$child->id.'" '.(!$child->published ? 'class ="grey" title="Comming soon"' : '').' class="'.(!$child->published ? "menu-unpublished" : "").'">';

                // var_dump($child);
                if($child->type == 1 && $child->page){
                    if($child->page->module) {
                        $ex = explode('/', $child->page->module);
                        $href = '';
                        if (!empty($ex)) {
                            $url = [];
                            $url['controller'] = $ex[0];
                            $url['language'] = LANG == 'rom' ? null : LANG;
                            $url['action'] = 'index';
                            if (count($ex) == 2) {
                                $url['action'] = $ex[1];
                                if (isset($child->page->slug[LANG])) {
                                    $url['slug'] = $child->page->slug[LANG];
                                }
                            }
                            $href = $item->Url->build($url);
                        }
                    }else{
                        $url = [];
                        $url['controller'] = 'Pages';
                        $url['action'] = 'view';
                        $url['language'] = LANG == 'rom' ? null : LANG;
                        $url['slug'] = isset($child->page->slug[LANG]) ? $child->page->slug[LANG] : '';
                        $href = $item->Url->build($url);
                    }

                    $class = '';
                    if (ACTION_NAME != 'index' && ACTION_NAME != 'view') {
                        if (strtolower($url['controller']) == CONTROLLER_NAME && $url['action'] == ACTION_NAME) {
                            $class = ' class="current"';
                        }
                    } else {
                        if (count($ex) == 2) {
                            if (strtolower($url['controller']) == CONTROLLER_NAME && $url['action'] == ACTION_NAME) {
                                $class = ' class="current"';
                            }
                        } else {
                            if (CONTROLLER_NAME == 'portfolios' && strtolower($url['controller']) == CONTROLLER_NAME && (ACTION_NAME == 'index' || ACTION_NAME == 'view')) {
                                $class = ' class="current"';
                            } elseif (strtolower($url['controller']) == CONTROLLER_NAME) {
                                $class = ' class="current"';
                            }
                        }
                    }

                    $current_a_class = in_array($child->page_id,$active_menu_ids) ? 'class="current"' : ' ' ;
                    //$html .=  '<li id="ch-'.$child->id.'" '.(!$child->published ? 'class ="grey"' : '').' class="'.(!$child->published ? "menu-unpublished" : "").'">';
                    // $html .= (!$child->published) ? $child->getTitle() : '<a href="'.$href.'" '.$class.'>'.$child->getTitle().'</a>';
                    $html .= '<a href="'.$href.'" '.$class.'>'.$child->getTitle().'</a>';
                }elseif($child->type == 2){
                    //$html .= '<a href="'.$child->external_link.'" target="_blank">'.$child->getTitle().'</a>';
                }else{
                    $html .= $child->getTitle();
                }
                $html .= nested_recursive_menu_home($child,$item);
                $html .=  '</li>';
            }
            $html .= '</ul>';





        }
        return $html;
    }
}

if (!function_exists('nested_recursive_menu_home_sitemap')) {
    function nested_recursive_menu_home_sitemap($parent,$item,$first = false){
        $ARR = [];
        $element = $first ? $parent : $parent->children;
        if($element){
            foreach ($element as $child){
                if($child->type == 1 && $child->page){
                    foreach (dev_conf('Languages') as $language) {
                        if(isset($child->page->slug[$language->code]) && $child->page->slug[$language->code]) {
                            if ($child->page->module) {
                                $ex = explode('/', $child->page->module);
                                $href = '';
                                if (!empty($ex)) {
                                    $url = [];
                                    $url['controller'] = $ex[0];
                                    $url['language'] = $language->code == 'en' ? null : $language->code;
                                    $url['action'] = 'index';
                                    if (count($ex) == 2) {
                                        $url['action'] = $ex[1];
                                        if (isset($child->page->slug[$language->code])) {
                                            $url['slug'] = $child->page->slug[$language->code];
                                        }
                                    }
                                    $href = $item->Url->build($url,true);
                                }
                            } else {
                                $url = [];
                                $url['controller'] = 'Pages';
                                $url['action'] = 'view';
                                $url['language'] = $language->code == 'en' ? null : $language->code;
                                $url['slug'] = isset($child->page->slug[$language->code]) ? $child->page->slug[$language->code] : '';
                                $href = $item->Url->build($url,true);
                            }
                            $ARR[] = ['url' => $href,'modified' => $child->page->modified ? strtotime($child->page->modified) : strtotime($child->page->created)];
                        }
                    }
                }
                $ARR[] = nested_recursive_menu_home($child,$item);
            }
        }
        return $ARR;
    }
}

if (!function_exists('summarize')) {
    function summarize($str, $limit)
    {
        $str = htmlspecialchars_decode(html_entity_decode(strip_tags($str)));
        $str = trim(preg_replace(array('/(?:\s|&nbsp;)+/u', '/\s+/u'), ' ', $str));
        $bigger = mb_strlen($str, 'UTF-8') > $limit;
        $str = mb_strimwidth($str, 0, $limit, ($bigger ? '...' : ''), 'UTF-8');

        return str_replace('....', '...', str_replace(' ...', '...', $str));
    }
}

if (!function_exists('escape_meta')) {
    function escape_meta($str)
    {
        return trim(str_replace(array('"', "'"), '', $str));
    }
}

if (!function_exists('seoify')) {
    function seoify($url = false)
    {
        if (!$url) {
            $url = $_SERVER['REQUEST_URI'];
        }
        if (substr($url, 0, 1) == '/') {
            $url = substr($url, 1);
        }
        $parts = explode('?', $url);

        return $parts[0];
    }
}


function replaceEnterWithP($content)
{
    $arr = explode(PHP_EOL, $content);
    $transformed_content = '';
    foreach ($arr as $key => $value) {
        $value = preg_replace('/\s+/', '', $value);
        if($value !=''){
            $transformed_content .= '<p>'.$value.'</p>';
        }
    }
    return $transformed_content;
}

function replacePWithEnter($content)
{
    $string = str_replace(['<p>','</p>'],['',PHP_EOL],$content);
    return $string;
}

function addToUItalic($content)
{
    $string = str_replace(['<u>','</u>'],['<u style="font-size: 12px;font-family: georgiaIab;"><i>','</i></u>'],$content);
    return $string;
}

function replacePWithBr($content)
{
    $string = str_replace(['<p>','</p>'],['<br>',''],$content);
    return $string;
}

function replaceEnterWithDiv($content)
{
    $arr = explode(PHP_EOL, $content);
    $string = '';
    if(isset($arr[0]) && trim($arr[0])){
        $string .= '<div class="title-white">'.$arr[0].'</div>';
    }
    if(isset($arr[1]) && trim($arr[1])){
        $string .= '<div class="title-brown">'.$arr[1].'</div>';
    }
    return $string;
}

function replaceEnterWithBr($content)
{
    $arr = explode(PHP_EOL, $content);
    $string = '';
    if(!count($arr)){
        return $string;
    }
    foreach ($arr as $k => $item){
        $string .= trim($item);
        if($k+1 != count($arr)){
            $string .= '<br>';
        }
    }
    return $string;
}

function replaceEnterWithCustomDiv($content)
{
    $arr = explode(PHP_EOL, $content);
    $string = '';
    if(!count($arr)){
        return $string;
    }
    foreach ($arr as $k => $item){
        $string .= "<div class=\"line\">
            <div class=\"inner-line\">$item</div>
        </div>";
    }
    return $string;
}

function replaceEnterWithLi($content)
{
    $arr = explode(PHP_EOL, $content);
    $string = '';
    if(!count($arr)){
        return $string;
    }
    foreach ($arr as $item){
        $string .= '<li>'.$item.'</li>';
    }
    return $string;
}

function datetime2MonthName($datetime, $language = 'rom') {
    // Eng
    if ($language == 'eng') {
        return strftime('%B', strtotime($datetime));
    }

    // Other languages
    $datetime = explode(' ', $datetime);
    $date = explode('-', $datetime[0]);
    $month = '';
    switch ($date[1]) {
        case '01': $month = $language == 'rom' ? 'Ianuarie' : 'Января';
            break;
        case '02': $month = $language == 'rom' ? 'Februarie' : 'Февраля';
            break;
        case '03': $month = $language == 'rom' ? 'Martie' : 'Марта';
            break;
        case '04': $month = $language == 'rom' ? 'Aprilie' : 'Апреля';
            break;
        case '05': $month = $language == 'rom' ? 'Mai' : 'Мая';
            break;
        case '06': $month = $language == 'rom' ? 'Iunie' : 'Июня';
            break;
        case '07': $month = $language == 'rom' ? 'Iulie' : 'Июля';
            break;
        case '08': $month = $language == 'rom' ? 'August' : 'Августа';
            break;
        case '09': $month = $language == 'rom' ? 'Septembrie' : 'Сентября';
            break;
        case '10': $month = $language == 'rom' ? 'Octombrie' : 'Октября';
            break;
        case '11': $month = $language == 'rom' ? 'Noiembrie' : 'Ноября';
            break;
        case '12': $month = $language == 'rom' ? 'Decembrie' : 'Декабря';
            break;
    }

    return $month;
}

function toNormalDate($datetime, $language = 'rom',$short=true) {
    if (empty($datetime)) {
        return 'empty';
    }
    if ($language == 'eng' || empty($language)) {
        return mb_strtolower(strftime('%d %B %Y', strtotime($datetime)));
    }

    $month = datetime2MonthName($datetime, $language);

    $datetime = explode(' ', $datetime);
    $date = explode('-', $datetime[0]);
    $fullMonth = mb_strtolower($month, 'UTF-8');
    if($short){
        $_month = mb_substr($fullMonth,0,3);
    }else{
        $_month = $fullMonth;
    }
    return (int) $date[2] . ' ' . $_month . ' ' . $date[0];
}

/**
 * Transliterates a string.
 */
function getTranslit($str) {
    // Replace Pattern
    $symbols = array(
        // Slavic
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g',
        'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh',
        'з' => 'z', 'и' => 'i', 'й' => 'y', 'к' => 'k',
        'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o',
        'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
        'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'ts',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'shch', 'ъ' => 'j',
        'ы' => 'y', 'ь' => 'i', 'э' => 'e', 'ю' => 'yu',
        'я' => 'ya',
    );

    // Return Transliterated String
    return str_replace(array_keys($symbols), $symbols, mb_strtolower($str));
}

/**
 * Get slug field for current model
 * @param string|App\Model\Entity\* $model
 * @return array
 */
function SlugListField($model = MODEL_NAME){
    $fields = [];
    $checkInSettings = dev_conf('Config.'.$model.'.slug_fields');
    if($checkInSettings){
        $fields = $checkInSettings;
    }else {
        $fields[] = 'title';
    }
    return $fields;
}

function reduceContent($string, $length = false, $id = '')
{
    if ($length)
    {
        $str =  strip_tags($string);
        $str = substr($str, 0, $length);
        $pos = strrpos($str, ' ');
        if(mb_strlen($string) > $length){
            return substr($str, 0, $pos).'...';
        }else{
            return $str;
        }
    }

    return $string;
}

function getListFromContain($obj, $name)
{
    $ar = [];
    foreach ($obj as $item){
        if($item[$name]){
            $ar[] = $item[$name];
        }
    }
    return $ar;
}

function getInternalSvgIcon($path = 'svg')
{
    $dir = WWW_ROOT.'common/'.$path;

    $icons = [];
    foreach (scandir($dir) as $item){
        if($item == '.' || $item == '..') continue;
        $icons[$item] = $item;
    }
    return $icons;
}

function SvgFromFolder($src = '')
{
    if(is_file($src)){
        return file_get_contents($src);
    }
    return '';
}

function replaceNoFollow($str)
{
    return str_replace('class="nofollow"','rel="nofollow"',$str);
}

function darken_color($rgb, $darker=2) {

    $hash = (strpos($rgb, '#') !== false) ? '#' : '';
    $rgb = (strlen($rgb) == 7) ? str_replace('#', '', $rgb) : ((strlen($rgb) == 6) ? $rgb : false);
    if(strlen($rgb) != 6) return $hash.'000000';
    $darker = ($darker > 1) ? $darker : 1;

    list($R16,$G16,$B16) = str_split($rgb,2);

    $R = sprintf("%02X", floor(hexdec($R16)/$darker));
    $G = sprintf("%02X", floor(hexdec($G16)/$darker));
    $B = sprintf("%02X", floor(hexdec($B16)/$darker));

    return $hash.$R.$G.$B;
}

function getAvailLang($langs = [],$domain = 'site'){
    $arr = [];
    foreach ($langs as $lang){
        if($lang->{$domain}){
            $arr[] = $lang;
        }
    }
    return $arr;
}

function replaceAllSymbolsBetweenTag($string){
    $content_processed = preg_replace_callback(
        '#\<u\>(.+?)\<\/u\>#s',
        create_function(
            '$matches',
            'return replaceBySymbols($matches[1]);'
        ),
        $string
    );

    return $content_processed;
}

function replaceBySymbols($str = ''){
    $del = explode(' ',$str);
    $ns = '';
    foreach ($del as $value){
        $ns .= '_';
        $leng = mb_strlen($value);
        for ($i=0; $i < $leng; $i++) {
            $ns .= '_';
        }
    }
    return $ns;
}
