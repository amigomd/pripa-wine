<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */


Router::defaultRouteClass(DashedRoute::class);

// for prefix admin
Router::prefix('admin', function ($routes) {
    // All routes here will be prefixed with `/admin`
    // And have the prefix => admin route element added.
    $routes->connect('/', ['controller' => 'Menus','action' => 'index' ]);
    $routes->connect('/:controller/edit/:id', ['action' => 'edit'], ['id' => '\d+', 'pass' => ['id']]);

    $routes->fallbacks(DashedRoute::class);
});

Router::scope('/', function (RouteBuilder $routes) {
    $routes->connect('/', ['controller' => 'pages', 'action' => 'index']);
    $routes->connect('/admin/users/login', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/admin/users/logout', ['controller' => 'Users', 'action' => 'logout']);
    $routes->connect('/admin/users/recovery', ['controller' => 'Users', 'action' => 'recovery']);
    $routes->connect('/users/reset-password', ['controller' => 'Users', 'action' => 'resetPassword']);

    $routes->addExtensions(['pdf']);
    /**
     * Set patterns.
     */
	$languages = dev_conf('Languages');
    if (count($languages) > 1) {
        $nl = [];
        foreach ($languages as $l){
            if(!$l->site) continue;
            if($l->code == 'rom' || $l->code == 'rom') continue;
            $nl[] = $l->code;
        }
        if(count($nl)){
            $langPattern = array('language' => '(' . implode('|', $nl) . ')');
        }else{
            $langPattern = array('language' => '(only-rom-language-available)');
        }
    } else {
        $langPattern = array('language' => '(only-rom-language-available)');
    }

    $withoutLangPattern = array('slug' => '[a-z0-9\-]+', 'pass' => array('slug'));

    $fullPatern = $langPattern + array('slug' => '[a-z0-9\-]+', 'pass' => array('slug'));

    /**
     * Connect Modules pages.
     */
    $pages = Cake\ORM\TableRegistry::get('Pages');
    $modules = $pages->find()->where(['Pages.module !=' => '','Pages.module IS NOT' => 'NULL'])->toArray();
    $languageList = lang();
    krsort($languageList);
    foreach ($modules as $c => $m) {
        foreach ($languageList as $id => $lang) {
            if($lang->site == 0) continue;
            if (strpos($m['module'], '/') === false) {
                if(isset($m['slug'][$lang->code])) {

                    $routes->connect('/' . $m['slug'][$lang->code] . '/:slug', array('controller' => $m['module'], 'action' => 'index'), $langPattern);

                    $routes->connect('/:language/' . $m['slug'][$lang->code] . '/:slug/*', array('controller' => $m['module'], 'action' => 'view'), $fullPatern);

                }
            } else {
                $url = explode('/', $m['module']);
                if(isset($m['slug'][$lang->code])){
                    if($lang->code == 'rom'){
                        $routes->connect('/:slug', array('controller' => $url[0], 'action' => $url[1]), $langPattern + array('slug' => $m['slug'][$lang->code]) + array('pass' => array('slug')));
                    }else{
                        $routes->connect('/:language/:slug', array('controller' => $url[0], 'action' => $url[1]), $langPattern + array('slug' => $m['slug'][$lang->code]) + array('pass' => array('slug')));
                    }
                }
            }
        }
    }

    $routes->connect('/:language/contact', array('controller' => 'Pages', 'action' => 'contact'), $langPattern);
    $routes->connect('/:language/awards', array('controller' => 'Pages', 'action' => 'awards'), $langPattern);

    $routes->connect('/contact', array('controller' => 'Pages', 'action' => 'contact'));

    $routes->connect('/awards', array('controller' => 'Pages', 'action' => 'awards'));
    $routes->connect('/wines', array('controller' => 'Pages', 'action' => 'wines'));
    $routes->connect('/winery', array('controller' => 'Pages', 'action' => 'winery'));

    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/:language', array('controller' => 'Pages', 'action' => 'display'), $langPattern);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */


    $routes->connect('/:language/:slug', array('controller' => 'Pages', 'action' => 'view'), $langPattern+array('slug' => '[a-z0-9\-]+') + array('pass' => array('slug')));
    $routes->connect('/:slug', array('controller' => 'Pages', 'action' => 'view'), $langPattern+array('slug' => '[a-z0-9\-]+') + array('pass' => array('slug')));



    $routes->connect('/:language/*', array('controller' => 'Pages', 'action' => 'display'), $langPattern);
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'display']);

    // sitemap
    $routes->connect('/sitemap.xml', ['controller' => 'Pages', 'action' => 'sitemap']);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
