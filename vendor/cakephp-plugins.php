<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'Duplicatable' => $baseDir . '/vendor/riesenia/cakephp-duplicatable/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'Settings' => $baseDir . '/plugins/Settings/',
        'Translations' => $baseDir . '/plugins/Translations/',
        'Users' => $baseDir . '/plugins/Users/'
    ]
];